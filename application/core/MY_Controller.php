<?php

class MY_Controller extends CI_Controller {

    public $data = array();

    function __construct() {
        parent::__construct();
        $this->data['errors'] = array();
    }

    public function _strtotime($str) {
        return strtotime($str);
    }

    public function upload($uploadFeild, $config) {

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($uploadFeild)) {
            return array('error' => $this->upload->display_errors());
        } else {
            return array('upload_data' => $this->upload->data());
        }
    }

}
