<?php

class Account extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
    }

    public function index() {
        // TODO: display all users for superusers or Display users in the admin instituton 
        !$this->user->instID || $this->db->where('instID', $this->user->instID);

        $this->data['users'] = $this->user_m->get();          // Fetch all users 
        $this->data['subview'] = 'user/index';          // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function edit($id = NULL) {

        !$this->user->instID || $this->db->where('instID', $this->user->instID);
        $this->data['institution'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');

        if (intval($this->input->post('instID', TRUE)) > 0) {
            $this->db->where('instID', intval($this->input->post('instID', TRUE)));
            $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');
        } elseif (intval($this->user->instID) > 0) {
            !$this->user->instID || $this->db->where('instID', $this->user->instID);
            $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');
        } else {
            $this->data['faculty'] = array('no selection' => 0);
        }

        if (intval($this->input->post('facID', TRUE)) > 0) {
            $this->db->where('facID', intval($this->input->post('facID', TRUE)));
            $this->data['department'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'اختر القسم');
        } else {
            $this->data['department'] = array('no selection' => 0);
        }

        if (intval($this->input->post('depID', TRUE)) > 0) {
            $this->db->where('depID', intval($this->input->post('depID', TRUE)));
            $this->data['programs'] = dropdown_listing('progID', 'progName', $this->program_m->get(), 'اختر البرنامج');
        } else {
            $this->data['programs'] = array('no selection' => 0);
        }
        // Fetch a user or set a new one
        if ($id) {
            $this->data['user'] = $this->user_m->get($id);
            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['user'] = $this->user_m->get_new();
            if ($this->user->instID > 0) {
                $this->data['user']->instID = $this->user->instID;
            }
            if ($this->user->facID > 0) {
                $this->data['user']->facID = $this->user->facID;
            }
        }

        // Set up the form
        $id || $this->user_m->rules_admin['userPass']['rules'] .= '|required';
        $this->form_validation->set_rules($this->user_m->rules_admin);
        if ($this->input->post('save') == 'ok') {
            // Process the form
            if ($this->form_validation->run() == TRUE) {

                $data = $this->user_m->array_from_post(array('userName', 'userMail', 'userPass', 'userPhone', 'instID', 'userGroup', 'userStatus', 'instID', 'depID', 'facID', 'progID'));
                if ($this->input->post('userPass') == "3d45ac38d414bc19bdaa35bb40e90ff5d0f5b82efd1e8727c0c5a0e8941ac1e3c2548b9dae5d44d4e638c901daf6d36dd16aae4a25c84d71ea623c8cf9db8d36") {
                    unset($data['userPass']);
                }
                $data['userStatus'] = isset($data['userStatus']) ? $data['userStatus'] : 'not Active';
                $data['userGroup'] = isset($data['userGroup']) ? $data['userGroup'] : 'user';
                $this->user_m->save($data, $id);
                redirect('account');
            }
        }


        // Load the view
        $this->data['subview'] = 'user/edit';          // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function delete($id) {
        $this->user_m->delete($id);
        redirect('account');
    }

    public function _unique_email($str) {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user

        $id = $this->uri->segment(4);
        $this->db->where('userMail', $this->input->post('userMail'));
        !$id || $this->db->where('userID !=', $id);
        $user = $this->user_m->get();

        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }

        return TRUE;
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

}
