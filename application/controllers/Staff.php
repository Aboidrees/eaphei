<?php

class Staff extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('staff_m');
        $this->load->model('scientificParticipation_m');
        $this->load->model('staffTrainning_m');
    }

    public function index() {
        empty($this->user->instID) || $this->db->where('instID', intval($this->user->instID));
        empty($this->user->facID) || $this->db->where('facID', intval($this->user->facID));
        empty($this->user->depID) || $this->db->where('depID', intval($this->user->depID));
        $this->data['staff'] = $this->staff_m->get();

        // Load view
        $this->data['subview'] = 'staff/index';
        $this->load->view('main_page', $this->data);
    }

    private function getStaff($staffID = NULL) {
        if ($staffID) {
            $this->data['staff'] = $this->staff_m->get($staffID);
            count($this->data['staff']) || $this->data['errors'][] = 'User could not be found';
        } else {
            //$this->data['staff'] = $this->staff_m->get_new();
            redirect(404);
        }
    }

    public function basicInfo($staffID) {

        $this->getStaff($staffID);
        $this->form_validation->set_rules($this->staff_m->rules_personal);
        $staffName = $this->input->post('staffName', TRUE);
        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staff_m->array_from_post(array('staffBirthDate', 'staffNationality', 'staffGender', 'staffNationalID', 'staffPhone', 'staffMobile', 'staffEmail', 'staffPermanentAddress'));
            $data['staffName'] = implode('-', $staffName);
            $this->staff_m->save($data, intval($staffID));
            redirect("staff/basicInfo/$staffID");
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/basicInfo';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function qualifications($staffID = NULL) {

        $this->getStaff($staffID);
        $this->data['qualifications'] = array('دكتوراة الفلسفة' => 'دكتوراة الفلسفة', 'ماجستير العلوم' => 'ماجستير العلوم', 'الدبلوم العالي' => 'الدبلوم العالي', 'بكلاريوس العلوم' => 'بكلاريوس العلوم', 'الدبلوم التقني' => 'الدبلوم التقني');
        $this->form_validation->set_rules($this->staff_m->rules_qualification);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staff_m->array_from_post(array('staffHigherQualification', 'staffDegreeDate', 'staffGenrSpec', 'staffSpec', 'staffResearchTitle'));
            $this->staff_m->save($data, intval($staffID));
            redirect("staff/qualifications/$staffID");
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/qualifications';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function job($staffID = NULL) {

        $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'Select Institution');
        $this->user->instID ? $this->db->where('instID', $this->user->instID) : $this->input->post('instID') ? $this->db->where('instID', $this->input->post('instID', TRUE)) : '';
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'Select Faculty');
        //$this->user->instID ? $this->db->where('instID', $this->user->instID) : $this->input->post('instID') ? $this->db->where('instID', $this->input->post('instID', TRUE)) : '';
        $this->user->facID ? $this->db->where('facID', $this->user->facID) : $this->input->post('facID') ? $this->db->where('facID', $this->input->post('facID', TRUE)) : '';
        $this->data['departments'] = dropdown_listing('depID', 'depName', $this->department_m->get(), 'Select Department');
        $this->data['staffPosition'] = array('أستاذ كرسي' => 'أستاذ كرسي', 'أستاذ' => 'أستاذ', 'أستاذ مشارك' => 'أستاذ مشارك', 'أستاذ مساعد' => 'أستاذ مساعد', 'محاضر' => 'محاضر', 'مساعد تدريس' => 'مساعد تدريس', 'كبير مدرسين' => 'كبير مدرسين', 'مدرس' => 'مدرس', 'مساعد مدرس' => 'مساعد مدرس');
        $this->data['staffStatus'] = array('على رأس العمل' => 'على رأس العمل', 'دوام جزئي' => 'دوام جزئي', 'منتدب' => 'منتدب', 'معار' => 'معار', 'مبتعث' => 'مبتعث', 'متعاقد بالخارج' => 'متعاقد بالخارج', 'أجازة بدون مرتب' => 'أجازة بدون مرتب', 'معاش' => 'معاش', 'اخرى' => 'اخرى',);
        $this->form_validation->set_rules($this->staff_m->rules_job);
        $this->getStaff($staffID);
        if ($this->form_validation->run() == TRUE and intval($staffID) and $this->input->post('save')) {
            $data = $this->staff_m->array_from_post(array('instID', 'facID', 'depID', 'staffPosition', 'staffHireDate', 'staffCurrentAddress', 'staffStatus', 'staffStatusStartDate', 'staffStatusEndDate'));
            $this->staff_m->save($data, intval($staffID));
            redirect("staff/job/$staffID");
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/job';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function scientificParticipation($staffID) {
        $this->getStaff($staffID);
        $this->data['scientificParticipations'] = $this->scientificParticipation_m->get_by("staffID = $staffID");
        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/scientificParticipations';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function scientificParticipationEdit($staffID, $id = NULL) {
        $this->getStaff($staffID);
        if ($id) {
            $this->data['scientificParticipation'] = $this->scientificParticipation_m->get($id);
            count($this->data['scientificParticipation']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['scientificParticipation'] = $this->scientificParticipation_m->get_new();
        }

        $this->data['spType'] = array('PhD Proposal' => 'PhD Proposal', 'Master Thesis' => 'Master Thesis', 'Scientific Participation' => 'Scientific Participation', 'financed Research' => 'financed Research', 'Scientific Paper' => 'Scientific Paper', 'Conferance' => 'Conferance', 'Book' => 'Book');

        $this->form_validation->set_rules($this->scientificParticipation_m->rules);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->scientificParticipation_m->array_from_post(array('spTitle', 'spType', 'spPublisher', 'spDate', 'spLink', 'spDetails', 'staffID'));
            $data['staffID'] = $staffID;
            $this->scientificParticipation_m->save($data, $id);
            redirect("staff/scientificParticipation/$staffID");
        }
        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/scientificParticipationEdit';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function staffTrainning($staffID = NULL) {
        $this->getStaff($staffID);
        $this->data['staffTrainning'] = $this->staffTrainning_m->get_by('staffID =' . intval($staffID));

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/staffTrainning';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function staffTrainningEdit($id = NULL) {
        $this->getStaff($id);
        if ($id) {
            $this->data['staffTrainning'] = $this->staffTrainning_m->get($id);
            count($this->data['staffTrainning']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['staffTrainning'] = $this->staffTrainning_m->get_new();
        }

        $this->form_validation->set_rules($this->staffTrainning_m->rules);

        if ($this->form_validation->run() == TRUE and intval($staffID)) {
            $data = $this->staffTrainning_m->array_from_post(array('cCode', 'cTitle', 'cContactHours', 'cReletedModules', 'cObjectives', 'cPrerequisites', 'cOutcomes', 'cOutline', 'cLearninigMethod', 'cRecommSoftware', 'cRecommHardware', 'cRecommBooks', 'cAssesstment',));
            $data['staffID'] = $staffID;

            $this->staffTrainning_m->save($data, $data['cCode']);
            redirect('staff/staffTrainning');
        }

        $this->data['subview'] = 'staff/staff';     // Set SubView
        $this->data['subsubview'] = 'staff/staffTrainningEdit';     // Set SubView
        $this->load->view('main_page', $this->data);   // Load view
    }

    public function edit($id = NULL) {

        // Fetch a staff or set a new one
        if ($id) {
            $this->data['staff'] = $this->staff_m->get($id);
            count($this->data['staff']) || $this->data['errors'][] = 'staff could not be found';
        } else {
            $this->data['staff'] = $this->staff_m->get_new();
        }

        $this->form_validation->set_rules($this->staff_m->rules_personal);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->staff_m->array_from_post(array('staffName', 'staffBirthDate', 'staffPhone', 'staffMobile', 'staffEmail', 'staffPermanentAddress'));
            $this->user->instID ? $data['instID'] = intval($this->user->instID) : 0;
            $this->user->facID ? $data['facID'] = intval($this->user->facID) : 0;
            $this->user->depID ? $data['depID'] = intval($this->user->depID) : 0;
            $id = $this->staff_m->save($data, $id);
            redirect("staff/basicInfo/$id");
        }


        $this->data['subview'] = 'staff/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->staff_m->delete($id);
        redirect('staff');
    }

}
