<?php

class StaffTrainning extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('faculty_m');
    }

    public function index() {
        // Fetch all StaffTrainning
        $this->data['staffTrainning'] = $this->StaffTrainning_m->get();

        // Load view
        $this->data['subview'] = 'StaffTrainning/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        // Fetch a StaffTrainning or set a new one
        if ($id) {
            $this->data['staffTrainning'] = $this->StaffTrainning_m->get($id);
            count($this->data['staffTrainning']) || $this->data['errors'][] = 'StaffTrainning could not be found';
        } else {
            $this->data['staffTrainning'] = $this->StaffTrainning_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->StaffTrainning_m->rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->StaffTrainning_m->array_from_post(array('tCode', 'tTitle', 'tContactHours', 'tObjectives', 'tPrerequisites', 'tAttendence', 'tOutline', 'tLearninigMethod', 'tRecommResource', 'tAssesstment', 'tValidation',));
            $this->StaffTrainning_m->save($data, $data['cTitle'], TRUE);
            redirect('staffTrainning');
        }

        // Load the view
        $this->data['subview'] = 'StaffTrainning/edit';
        $this->load->view('main_page', $this->data);
    }
    

    public function delete($id) {
        $this->StaffTrainning_m->delete($id);
        redirect('staffTrainning');
    }

}
