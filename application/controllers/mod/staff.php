<?php

class Staff extends Mod_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('staff_m');
        $this->load->model('institution_m');
    }

    public function index() {

        $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'حدد المؤسسة');
        if (intval($this->input->post('instID'))) {
            $this->data['instID'] = intval($this->input->post('instID'));
        }
        !isset($this->data['instID']) || $this->db->where("faculty.instID = " . $this->data['instID']);

        $this->db->join('faculty', 'faculty.facID = staff.facID');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['staff'] = $this->staff_m->get();

        $this->data['subview'] = 'mod/staff/index';     // Set SubView
        $this->load->view('mod/main_page2', $this->data);  // Load view
    }

}
