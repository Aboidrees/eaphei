<?php

class Home extends Mod_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['subview'] = 'mod/home';     // Set SubView
        $this->load->view('mod/main_page', $this->data);  // Load view
    }

}
