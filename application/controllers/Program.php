<?php

class Program extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('program_m');
        $this->load->model('department_m');
        if ($this->user->progID) {
            redirect("evaluation/index/{$this->user->progID}");
            exit();
        }
    }

    public function index($depID = NULL) {
        // Fetch all program
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['department'] = $this->department_m->get($depID);
        $this->data['programs'] = $this->program_m->get_by("depID = $depID");
        // Load view
        $this->data['subview'] = 'program/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($depID = NULL, $id = NULL) {
        $this->db->join('faculty', 'faculty.facID = department.facID');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['department'] = $this->department_m->get($depID);

        // TODO : Confirm programs Types
        $this->data['progType'] = array('دكتوراة' => 'دكتوراة', 'ماجستير' => 'ماجستير', 'دبلوم عالي' => 'دبلوم عالي', 'بكلاريوس' => 'بكلاريوس', 'دبلوم تقني' => 'دبلوم تقني',);
        $this->data['depID'] = $depID;

        // Fetch a program or set a new one
        if ($id) {
            $this->data['program'] = $this->program_m->get($id);
            count($this->data['program']) || $this->data['errors'][] = 'program could not be found';
        } else {
            $this->data['program'] = $this->program_m->get_new();
        }

        // Set up the form
        $rules = $this->program_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->program_m->array_from_post(array('progName', 'progStartDate', 'progLastReviewDate', 'progType', 'depID', 'progPref',));
            $this->program_m->save($data, $id);
            redirect("program/index/$depID");
        }

        // Load the view
        $this->data['subview'] = 'program/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($depID = NULL, $id) {
        $this->program_m->delete($id);
        redirect('program/index/' . $depID);
    }

}
