<?php

class Report extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('institution_m');
        $this->load->model('faculty_m');
        $this->load->model('department_m');
        $this->load->model('program_m');
        $this->load->model('evaluation_m');
        $this->load->model('staff_m');
//        $this->load->model('evaluation_m');
        
    }

    public function index() {
        $this->data['instList'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');

        $this->data['instID'] = intval($this->user->instID) > 0 ? intval($this->user->instID) : intval($this->input->post('instID', TRUE)) > 0 ? intval($this->input->post('instID', TRUE)) : 0;
        if (intval($this->data['instID']) > 0) {
            $this->data['facList'] = dropdown_listing('facID', 'facName', $this->faculty_m->get_by('instID=' . $this->data['instID']), 'اختر الكلية');
        }

        $this->data['facID'] = intval($this->user->facID) > 0 ? intval($this->user->facID) : intval($this->input->post('facID', TRUE)) > 0 ? intval($this->input->post('facID', TRUE)) : 0;
        if (intval($this->data['facID']) > 0) {
            $this->data['depList'] = dropdown_listing('depID', 'depName', $this->department_m->get_by('facID=' . $this->data['facID']), 'اختر القسم');
        }

        $this->data['depID'] = intval($this->user->depID) > 0 ? intval($this->user->depID) : intval($this->input->post('depID', TRUE)) > 0 ? intval($this->input->post('depID', TRUE)) : 0;
        if (intval($this->data['depID']) > 0) {
            $this->data['progList'] = dropdown_listing('progID', 'progName', $this->program_m->get_by('depID=' . $this->data['depID']), 'اختر القسم');
        }

        $this->data['progID'] = intval($this->user->progID) > 0 ? intval($this->user->progID) : intval($this->input->post('depID', TRUE)) > 0 ? intval($this->input->post('progID', TRUE)) : 0;
        if (intval($this->data['progID']) > 0) {
            $this->data['evalList'] = dropdown_listing('evalID', 'evalDate', $this->evaluation_m->get_by('progID=' . $this->data['progID']), 'اختر التقويم');
        }


        if (intval($this->input->post('evalID', TRUE)) > 0) {

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'cra');
            $this->data['cra'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'prwa');
            $this->data['prwa'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ha');
            $this->data['ha'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'sa');
            $this->data['sa'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'esa');
            $this->data['esa'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'espa');
            $this->data['espa'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'S');
            $this->data['S'] = $this->db->from('department_evaluation_view')->get()->result()[2];

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'L3');
            $this->data['L3'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'L4');
            $this->data['L4'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'L5');
            $this->data['L5'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'L6');
            $this->data['L6'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'L7');
            $this->data['L7'] = $this->db->from('department_evaluation_view')->get()->row();




            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar41');
            $this->data['ar41'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar42');
            $this->data['ar42'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar43');
            $this->data['ar43'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar44');
            $this->data['ar44'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar45');
            $this->data['ar45'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar46');
            $this->data['ar46'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->db->where('evalID', $this->input->post('evalID', TRUE));
            $this->db->where('varName', 'ar47');
            $this->data['ar47'] = $this->db->from('department_evaluation_view')->get()->row();

            $this->data['ar4'] = $this->data['ar41']->degValue + $this->data['ar42']->degValue + $this->data['ar43']->degValue + $this->data['ar44']->degValue + $this->data['ar45']->degValue + $this->data['ar46']->degValue + $this->data['ar47']->degValue;

            //esa, espa ,sa,ha,prwa,cra
        }

        $this->evaluation();

        $this->data['subview'] = 'report/index';     // Set SubView
        $this->load->view('main_page', $this->data);  // Load view
    }

    private function evaluation() {

        $this->db->select('institution.*, COUNT(faculty.instID) AS facNum');
        $this->db->join('faculty', 'faculty.instID = institution.instID', 'LEFT');
        $this->db->group_by('institution.instID');
        $this->data['institutions'] = $this->institution_m->get();
    }

    private function zeros($x = 0, $y = 0) {
        $array = array();
        for ($i = 0; $i < $x; $i++) {
            for ($j = 0; $j < $y; $j++) {
                $array[$i][$j] = 0;
            }
        }
        //dump($array);
        return $array;
    }
    
    function staff() {
        $this->data['staff_array']['staffByPosition'] = $this->staffByPosition();
        $this->data['staff_array']['staffByQualification'] = $this->staffByQualification();
        $this->data['staff_array']['leak'] = $this->leak();
        $this->data['staff_array']['foreigners'] = $this->foreigners();
        $this->data['staff_array']['duty'] = $this->duty();
        
        // Load view
        $this->data['subview'] = 'report/staff';
        $this->load->view('main_page', $this->data);        
    }

    private function staffByPosition() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();

        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }
        return $staff_array;
    }

    private function staffByQualification() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffHigherQualification');
        $temp = $this->zeros(1, 10);
        $staff_array = array();
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'PHD' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'MSC' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'Higher Diploma' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffHigherQualification == 'BSC' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffHigherQualification == 'BSC' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
        }
        return $staff_array;
    }

    private function leak() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis('', 'staffStatus');
        $temp = $this->zeros(1, 10);
        $staff_array = array();
        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffStatus == 'Mandate' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Mandate' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'loaning' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'loaning' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'leave without pay' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'leave without pay' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffStatus == 'Absence' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Absence' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Separate' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'Separate' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'retired' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffStatus == 'retired' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }
        } //'Available','Scholarship','Mandate','loaning','leave without pay','Absence','Separate','retired'
        return $staff_array;
    }

    private function foreigners() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);
        $faculty_staff = $this->staff_m->getStatistcis("staff.staffNationality != 'سوداني'", 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }
        return $staff_array;
    }

    private function duty() {
        !$this->user->facID || $this->db->where('facID', $this->user->facID);

        $faculty_staff = $this->staff_m->getStatistcis("staff.staffStatus = 'دوام جزئي'", 'staffPosition');
        $temp = $this->zeros(1, 10);
        $staff_array = array();


        for ($i = 0; $i < count($faculty_staff); $i++) {
            if (!isset($staff_array[$faculty_staff[$i]->facName])) {
                $staff_array[$faculty_staff[$i]->facName] = $temp[0];
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][0] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][1] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][2] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مشارك' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][3] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][4] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'أستاذ مساعد' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][5] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][6] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'محاضر' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][7] = $faculty_staff[$i]->staffNumber;
            }

            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'ذكر') {
                $staff_array[$faculty_staff[$i]->facName][8] = $faculty_staff[$i]->staffNumber;
            }
            if ($faculty_staff[$i]->staffPosition == 'مساعد تدريس' and $faculty_staff[$i]->staffGender == 'أنثى') {
                $staff_array[$faculty_staff[$i]->facName][9] = $faculty_staff[$i]->staffNumber;
            }
        }
        return $staff_array;
    }
}
