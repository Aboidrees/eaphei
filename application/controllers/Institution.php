<?php

class Institution extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('user_m');
        if ($this->user->instID) {
            redirect("faculty/index/{$this->user->instID}");
            exit();
        }
    }

    public function index() {

        !$this->input->post('instName', TRUE) || $this->db->like('instName', $this->input->post('instName', TRUE));
        $this->data['institutions'] = $this->institution_m->get();   // Fetch all institution
        $this->data['subview'] = 'institution/index';         // Set SubView
        $this->load->view('main_page', $this->data);          // Load view
    }

    public function edit($id = NULL) {

        // Fetch a institution or set a new one
        if ($id) {
            $this->data['institution'] = $this->institution_m->get($id);
            count($this->data['institution']) || $this->data['errors'][] = 'institution could not be found';
        } else {
            $this->data['institution'] = $this->institution_m->get_new();
        }
        // Set up the form
        $this->form_validation->set_rules($this->institution_m->rules);
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->institution_m->array_from_post(array('instName', 'instEmail', 'instPhone', 'instFax', 'instFoundationDate', 'instAddress',));
            $config = array('upload_path' => './public/uploads/images/logos/', 'allowed_types' => 'gif|jpg|png', 'max_size' => 100, 'max_width' => 160, 'max_height' => 160);
            $info = $this->upload('instLogo', $config);
            if (!isset($info["error"]) && isset($info['upload_data']) and $info['upload_data']['is_image'] === TRUE) {
                $data['instLogo'] = $info['upload_data']['file_name'];
            }
            $this->institution_m->save($data, $id);
            redirect('institution');
        }

        $this->data['subview'] = 'institution/edit';  // Set SubView
        $this->load->view('main_page', $this->data);  // Load the view
    }

    public function faculty($instID = NULL) {

        if (!$instID) {
            redirect(404);
        } elseif ($this->user->facID) {
            redirect("institution/department/{$this->user->facID}");
        } else {
            $this->db->where('parentID', $instID);
            $this->data['faculties'] = $this->institution_m->get();   // Fetch all institution
            $this->data['facParent'] = $this->institution_m->get($instID);   // Fetch parent
            $this->data['subview'] = 'institution/faculty';         // Set SubView
            $this->load->view('main_page', $this->data);          // Load view
        }
    }

    public function department($facID = NULL) {
        if (!$facID) {
            redirect(404);
        } else {
            $this->db->where('parentID', $facID);
            $this->data['departments'] = $this->institution_m->get();   // Fetch all institution

            $this->data['faculty'] = $this->institution_m->get($facID);   // Fetch parent
            $this->data['institution'] = $this->institution_m->get($this->data['faculty']->parentID);   // Fetch parent parent

            $this->data['subview'] = 'institution/department';         // Set SubView
            $this->load->view('main_page', $this->data);          // Load view
        }
    }

    private function pre_edit($type_parentID) {
        dump(explode('-', $type_parentID));
        $type = explode('-', $type_parentID)[0];
        @$parentID = explode('-', $type_parentID)[1];

        if ($type == 'inst' && (!isset($parentID) || !$parentID || intval($parentID) == 0)) {
            $this->data['parentID'] = 0;
        } else if ($type == 'fac') {
            if ($this->user->instID) {
                $parentID = $this->user->instID;
            }
            $this->data['parentID'] = $parentID;
        } else if ($type == 'dep') {
            if ($this->user->facID) {
                $parentID = $this->user->facID;
            }
            $this->data['parentID'] = $parentID;
        } else {
            redirect(404);
        }
        $this->data['type'] = $type;
    }

    public function edit_inst($id = NULL) {

// Fetch a institution or set a new one
        if ($id) {
            $this->data['institution'] = $this->institution_m->get(intval($id));
            count($this->data['institution']) || $this->data['errors'][] = 'institution could not be found';
        } else {
            $this->data['institution'] = $this->institution_m->get_new();
        }
// Set up the form
        $this->form_validation->set_rules($this->institution_m->rules);

// Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->institution_m->array_from_post(array('name', 'email', 'phone', 'fax', 'foundationDate', 'address', 'logo', 'parentID', 'type'));
            $this->institution_m->save($data, $id);
            redirect('institution');
        }

        $this->data['subview'] = 'institution/inst_edit';  // Set SubView
        $this->load->view('main_page', $this->data);  // Load the view
    }

    public function edit_fac($instID = NULL, $id = NULL) {
        if (!$instID) {
            redirect(404);
        }

        $this->data['institution'] = $this->institution_m->get(intval($instID));

// Fetch a institution or set a new one
        if ($id) {
            $this->data['faculty'] = $this->institution_m->get(intval($id));
            count($this->data['faculty']) || $this->data['errors'][] = 'institution could not be found';
        } else {
            $this->data['faculty'] = $this->institution_m->get_new();
        }
// Set up the form
        $this->form_validation->set_rules($this->institution_m->rules);

// Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->institution_m->array_from_post(array('name', 'email', 'phone', 'fax', 'foundationDate', 'address', 'logo', 'parentID', 'type'));
            $this->institution_m->save($data, $id);
            redirect("institution/faculty/$instID");
        }

        $this->data['subview'] = 'institution/fac_edit';  // Set SubView
        $this->load->view('main_page', $this->data);  // Load the view
    }

    public function edit_dep($facID = NULL, $id = NULL) {
        if (!$facID) {
            redirect(404);
        }

        $this->data['faculty'] = $this->institution_m->get(intval($facID));   // Fetch parent
        $this->data['institution'] = $this->institution_m->get($this->data['faculty']->parentID);   // Fetch parent parent
// Fetch a institution or set a new one
        if ($id) {
            $this->data['department'] = $this->institution_m->get(intval($id));
            count($this->data['department']) || $this->data['errors'][] = 'institution could not be found';
        } else {
            $this->data['department'] = $this->institution_m->get_new();
        }
// Set up the form
        $this->form_validation->set_rules($this->institution_m->rules);

// Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->institution_m->array_from_post(array('name', 'email', 'phone', 'fax', 'foundationDate', 'address', 'logo', 'parentID', 'type'));
            $this->institution_m->save($data, $id);
            redirect("institution/department/$facID");
        }

        $this->data['subview'] = 'institution/dep_edit';  // Set SubView
        $this->load->view('main_page', $this->data);  // Load the view
    }

    public function delete($id) {
        $this->institution_m->delete($id);
        redirect('institution');
    }

}
