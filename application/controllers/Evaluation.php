<?php

class Evaluation extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('program_m');
        $this->load->model('program_m');
        $this->load->model('evalelement_m');
        $this->load->model('evaluation_m');
        $this->load->model('evalform_m');
    }

    public function index($progID = NULL) {

        $this->db->join('((institution), (faculty), (department))', 'institution.instID = faculty.instID and department.depID = program.depID and faculty.facID = department.facID');
        $this->data['program'] = $this->program_m->get($progID);

        $this->data['evaluations'] = $this->evaluation_m->get_by("progID =$progID");

        // Load view
        $this->data['subview'] = 'evaluation/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($progID = NULL, $id = NULL) {
        !intval($this->user->instID) || $this->db->where("instID", $this->user->instID);
        !intval($this->user->facID) || $this->db->where("program.facID", $this->user->facID);
        $this->data['program'] = dropdown_listing('progID', 'progName', $this->program_m->get());

        $this->data['evalforms'] = dropdown_listing("efID", "efName", $this->evalform_m->get(), "اختر استمارة التقويم");


        // Fetch a evaluation or set a new one
        if ($id) {
            $this->data['evaluation'] = $this->evaluation_m->get($id);
            count($this->data['evaluation']) || $this->data['errors'][] = 'evaluation could not be found';
        } else {
            $this->data['evaluation'] = $this->evaluation_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->evaluation_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->evaluation_m->array_from_post(array('evalDate', 'efID', 'dataGetheringDate', 'progID',));
            $this->evaluation_m->save($data, $id);
            redirect('evaluation/index/' . $progID);
        }

        // Load the view
        $this->data['subview'] = 'evaluation/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($progID = NULL, $id) {
        $this->evaluation_m->delete($id);
        redirect('evaluation/index/' . $progID);
    }

    public function _get_element_and_values($evalID) {
        $array = array();
        $evalelements = $this->evalelement_m->get();
        foreach ($evalelements as $index => $evalelement) {
            $this->db->group_by('elementID, evalID, varName');
            $degreeValue[$index] = $this->evalelement_m->getValues("evalID = $evalID and elementID = $evalelement->elementID", TRUE);
            $evalelement->evalID = $evalID;
            $evalelement->degValue = $degreeValue[$index];
            $array[$index] = $evalelement;
            $array['elementIDs'][] = $evalelement->elementID;
        }
        return $array;
    }

    public function elementValues($evalID, $efID) {
        $this->db->join('((institution), (faculty), (department), (program))', 'institution.instID = faculty.instID and department.depID = program.depID and faculty.facID = department.facID and program.progID = evaluation.progID');
        $this->data['evaluation'] = $this->evaluation_m->get($evalID);

        $this->data['evalID'] = $evalID;
        $this->data['efID'] = $efID;

        $this->db->where("elementParent = 0 and efID = $efID");
        $this->data['evalelements'] = $this->_get_element_and_values($evalID);

        $this->db->where('efID', $efID);
        $this->db->where_in('elementParent', $this->data['evalelements']['elementIDs']);
        $this->data['sub_evalelements'] = $this->_get_element_and_values($evalID);

        $this->db->where('efID', $efID);
        $this->db->where_in('elementParent', $this->data['sub_evalelements']['elementIDs']);
        $this->data['sub_sub_evalelements'] = $this->_get_element_and_values($evalID);

        unset($this->data['evalelements']['elementIDs'], $this->data['sub_evalelements']['elementIDs'], $this->data['sub_sub_evalelements']['elementIDs']);

        $this->form_validation->set_rules($this->_elementFieldsValidation($this->input->post()));

        if ($this->form_validation->run() == TRUE) {
            foreach ($this->input->post() as $element => $value) {
                $element = array_combine(array('elementID', 'varName'), explode('_', $element));
                $manual_ID_Input = empty($this->evalelement_m->getValues(array('elementID' => $element['elementID'], 'evalID' => $evalID, 'varName' => $element['varName']))) ? TRUE : FALSE;
                $manual_ID_Input || $this->db->where(array('evalID' => $evalID, 'varName' => $element['varName']));
                $this->evalelement_m->saveValues(array('elementID' => $element['elementID'], 'evalID' => $evalID, 'varName' => $element['varName'], 'degValue' => $value), $element['elementID'], $manual_ID_Input);
            }
            redirect("evaluation/elementValues/$evalID/$efID");
        }
        $this->data['subview'] = 'evaluation/elementValues';
        $this->load->view('main_page', $this->data);
    }

    public function pointer($evalID) {
        $this->data['evalID'] = $evalID;

        $this->db->join('degree', 'degree.elementID = evalelement.elementID');
        $this->data['pointers'] = $this->evalelement_m->get_by("evalID = $evalID and elementParent = (SELECT elementID FROM evalelement where elementSymbol = 'T')");

        $this->db->join('varvalue', 'varvalue.varID = variable.varID');
        $this->data['student'] = $this->variable_m->get_by("evalID = $evalID and varSymbol = 'S'", TRUE);

        $this->data['subview'] = 'evaluation/pointer';
        $this->load->view('main_page', $this->data);
    }

    function _elementFieldsValidation($vars) {
        $validationArray = array();
        foreach (array_keys($vars) as $value) {
            $validationArray[$value] = array('field' => $value, 'label' => $value, 'rules' => 'intval');
        }
        return $validationArray;
    }

    public function evaluat($evalID, $efID) {
        $this->db->join('((institution), (faculty), (department), (program))', 'institution.instID = faculty.instID and department.depID = program.depID and faculty.facID = department.facID and program.progID = evaluation.progID');
        $this->data['evaluation'] = $this->evaluation_m->get($evalID);

        $new_sub_sub_elements = array();
        $new_sub_elements = array();

        // geting level 0 evaluation elements 
        $elements = $this->evalelement_m->getEvelElements(array('elementParent' => '0', 'evalID' => $evalID, 'efID' => $efID));
        foreach ($elements as $element) {
            // geting level 1 evaluation elements 
            $sub_elements = $this->evalelement_m->getEvelElements(array('elementParent' => $element->elementID, 'evalID' => $evalID, 'efID' => $efID));
            foreach ($sub_elements as $sub_element) {
                // getting level 2 evaluation elements

                $sub_sub_elements = $this->evalelement_m->getEvelElements(array('elementParent' => $sub_element->elementID, 'evalID' => $evalID, 'efID' => $efID));
                $sub_sub_element_result[$sub_element->elementID] = $this->evalelement_m->equationProcessing($sub_sub_elements, $sub_element->elementSymbol);
                if (isset($sub_sub_element_result[$sub_element->elementID]['arrays'])) {
                    $x = $sub_sub_element_result[$sub_element->elementID]['arrays'];
                    $new_sub_sub_elements = array_merge($new_sub_sub_elements, $x);
                }
            }
            $sub_element_result[$element->elementID] = $this->evalelement_m->equationProcessing($sub_elements, $element->elementSymbol, $sub_sub_element_result);
            if (isset($sub_element_result[$element->elementID]['arrays'])) {
                $x = $sub_element_result[$element->elementID]['arrays'];
                $new_sub_elements = array_merge($new_sub_elements, $x);
            }
        }
        $element_result = $this->evalelement_m->equationProcessing($elements, $element->elementSymbol, $sub_element_result);
        $new_elements = $element_result['arrays'];
        $this->data ['new_subsubelements'] = $new_sub_sub_elements;
        $this->data ['new_subelements'] = $new_sub_elements;
        $this->data ['new_elements'] = $new_elements;
        $this->data ['evalID'] = $evalID;

        //Load the view
        $this->data['subview'] = 'evaluation/evaluat';
        $this->load->view('main_page', $this->data);
    }

}
