<?php

class Assistancestaff extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('assistancestaff_m');
    }

    public function index() {
        $this->data['assistancestaffs'] = $this->assistancestaff_m->get();
        // Load view
        $this->data['subview'] = 'assistancestaff/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($id = NULL) {

        if ($id) {
            $this->data['assistancestaff'] = $this->assistancestaff_m->get($id);
            count($this->data['assistancestaff']) || $this->data['errors'][] = 'assistance staff could not be found';
        } else {
            $this->data['assistancestaff'] = $this->assistancestaff_m->get_new();
        }

        // Set up the form
        $rules = $this->assistancestaff_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->assistancestaff_m->array_from_post(array('aStaffJopTitle'));
            $this->assistancestaff_m->save($data, $id);
            redirect("assistancestaff");
        }

        // Load the view
        $this->data['subview'] = 'assistancestaff/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->assistancestaff_m->delete($id);
        redirect('assistancestaff');
    }

}