<?php

class Department extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('institution_m');
        $this->load->model('department_m');
        $this->load->model('faculty_m');

        if ($this->user->depID) {
            redirect("program/index/{$this->user->depID}");
            exit();
        }
    }

    public function index($facID = NULL) {
        if ($this->user->facID > 0 && $this->user->facID !== $facID) {
            redirect("department/index/{$this->user->facID}");
            exit();
        }
        $this->data['facID'] = (intval($this->user->facID) > 0) ? intval($this->user->facID) : intval($facID);

        // Fetch all department
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['faculty'] = $this->faculty_m->get($this->data['facID']);
        !$this->input->post('depName',TRUE) || $this->db->like('depName',$this->input->post('depName',TRUE));
        $this->data['departments'] = $this->department_m->get_by("facID = {$this->data['facID']}");

        // Load view
        $this->data['subview'] = 'department/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($facID = NULL, $id = NULL) {

        $this->data['facID'] = $facID;
        $this->data['institution'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['faculty'] = $this->faculty_m->get($this->data['facID']);
        
        // Fetch a department or set a new one
        if ($id) {
            $this->data['department'] = $this->department_m->get($id);
            count($this->data['department']) || $this->data['errors'][] = 'department could not be found';
        } else {
            $this->data['department'] = $this->department_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->department_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->department_m->array_from_post(array('depName', 'depEmail', 'depPhone', 'depFax', 'depFoundationDate', 'depAddress', 'facID'));
            $this->department_m->save($data, $id);
            redirect('department/index/' . $data['facID']);
        }

        // Load the view
        $this->data['subview'] = 'department/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($id) {
        $this->department_m->delete($id);
        redirect('department');
    }

}
