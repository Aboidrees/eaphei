<?php

class Evalform extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('evalform_m');
    }

    public function index() {
        $this->data['evalforms'] = $this->evalform_m->get();   // Fetch all evalform
        $this->data['subview'] = 'evalform/index';         // Set SubView
        $this->load->view('main_page', $this->data);          // Load view
    }

    public function edit($id = NULL) {

        $this->data['efSector'] = array('Engineering' => 'Engineering', 'Medical' => 'Medical', 'Agricultural' => 'Agricultural', 'Economical' => 'Economical', 'Industrial' => 'Industrial');

        // Fetch a evalform or set a new one
        if ($id) {
            $this->data['evalform'] = $this->evalform_m->get($id);
            count($this->data['evalform']) || $this->data['errors'][] = 'evalform could not be found';
        } else {
            $this->data['evalform'] = $this->evalform_m->get_new();
        }
        // Set up the form
        $this->form_validation->set_rules($this->evalform_m->rules);
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->evalform_m->array_from_post(array('efName', 'efSector', 'efDesc',));
            $this->evalform_m->save($data, $id);
            redirect('evalform');
        }

        $this->data['subview'] = 'evalform/edit';  // Set SubView
        $this->load->view('main_page', $this->data);  // Load the view
    }

    public function delete($id) {
        $this->evalform_m->delete($id);
        redirect('evalform');
    }

}
