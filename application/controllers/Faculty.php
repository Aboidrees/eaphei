<?php

class Faculty extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('faculty_m');
        $this->load->model('institution_m');


        if ($this->user->facID) {
            redirect("department/index/{$this->user->facID}");
            exit();
        }
    }

    public function index($instID = NULL) {

        if ($this->user->instID > 0 && $this->user->instID !== $instID) {
            redirect("faculty/index/{$this->user->instID}");
            exit();
        }
        $this->data['instID'] = (intval($this->user->instID) > 0) ? intval($this->user->instID) : intval($instID);
        // Fetch all faculty
        
        $this->data['institution'] = $this->institution_m->get($instID);
        !$this->input->post('facName',TRUE) || $this->db->like('facName',$this->input->post('facName',TRUE));
        $this->data['faculties'] = $this->faculty_m->get_by("instID = {$this->data['instID']}");

        // Load view
        $this->data['subview'] = 'faculty/index';
        $this->load->view('main_page', $this->data);
    }

    public function edit($instID = NULL, $id = NULL) {
        !($this->user->instID > 0 && $this->user->instID !== $instID) || redirect("faculty/index/{$this->user->instID}");
        $this->data['instID'] = (intval($this->user->instID) > 0) ? intval($this->user->instID) : intval($instID);
        $this->data['institution'] = $this->institution_m->get($instID);
        
        !$this->user->instID || $this->db->where('instID', $this->user->instID);
        $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');
        // Fetch a faculty or set a new one
        if ($id) {
            $this->data['faculty'] = $this->faculty_m->get($id);
            count($this->data['faculty']) || $this->data['errors'][] = 'faculty could not be found';
        } else {
            $this->data['faculty'] = $this->faculty_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->faculty_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->faculty_m->array_from_post(array('facName', 'facEmail', 'facPhone', 'facFax', 'facFoundationDate', 'facAddress', 'instID'));
            $this->faculty_m->save($data, $id);
            redirect('faculty/index/' . $data['instID']);
        }

        // Load the view
        $this->data['subview'] = 'faculty/edit';
        $this->load->view('main_page', $this->data);
    }

    public function delete($instID, $id) {
        if ($this->user->instID > 0 && $this->user->instID !== $instID) {
            redirect(404);
        }
        $this->faculty_m->delete($id);
        redirect('faculty');
    }

}
