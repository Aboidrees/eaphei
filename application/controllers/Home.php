<?php

class Home extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['subview'] = 'home';     // Set SubView
        $this->load->view('main_page', $this->data);  // Load view
    }

}
