<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->data = array();
    }

    public function index() {
        redirect('user/login');
    }

    public function login() {

        // Redirect a user if he's already logged in
        !$this->user_m->adminLogin() || redirect('home');
        !$this->user_m->userLogin() || redirect('mod/evaluation');

        // Set form
        $this->form_validation->set_rules($this->user_m->rules);

        // Process form
        if ($this->form_validation->run() == TRUE and $this->user_m->login() == TRUE) {
            !$this->user_m->adminLogin() || redirect('home');
            !$this->user_m->userLogin() || redirect('mod/evaluation');
        }
        // Load view
        $this->load->view('login', $this->data);
    }

    public function logout() {
        $this->user_m->logout();
        redirect('user/login');
    }

}
