<?php

class Group extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('group_m');
    }

    public function index() {
        $this->data['group'] = $this->group_m->get();

        $this->data['subview'] = 'admin/group/index';     // Set SubView
        $this->load->view('admin/main_page', $this->data);  // Load view
    }

    public function edit($id = NULL) {

        $this->data['group'] = dropdown_listing('groupID', 'groupName', $this->group_m->get());


        // Fetch a group or set a new one
        if ($id) {
            $this->data['group'] = $this->group_m->get($id);
            count($this->data['group']) || $this->data['errors'][] = 'group could not be found';
        } else {
            $this->data['group'] = $this->group_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->group_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $this->group_m->save($this->group_m->array_from_post(array('groupName')), $id);
            redirect('admin/group');
        }

        // Load the view
        $this->data['subview'] = 'admin/group/edit';
        $this->load->view('admin/main_page', $this->data);
    }

    public function delete($id) {
        $this->group_m->delete($id);
        redirect('admin/group');
    }

}
