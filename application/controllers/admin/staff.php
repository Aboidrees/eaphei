<?php

class Staff extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('staff_m');
        $this->load->model('faculty_m');
    }

    public function index() {
        if ($this->user->instID == 0) {
            $this->data['institution'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');
        }
        if (intval($this->input->post('instID'))) {
            $this->data['instID'] = intval($this->input->post('instID'));
        }
        if (isset($this->data['instID'])) {
            $this->db->where("instID", intval($this->data['instID']));
        } else if ($this->user->instID) {
            $this->db->where("instID", $this->user->instID);
        }

        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');

        // Fetch all staff
        !isset($this->data['instID']) || $this->db->where("faculty.instID = " . $this->data['instID']);
        $this->db->join('faculty', 'faculty.facID = staff.facID');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['staff'] = $this->staff_m->get();
        // Load view
        $this->data['subview'] = 'admin/staff/index';
        $this->load->view('admin/main_page', $this->data);
    }

    public function edit($id = NULL) {
        if ($this->user->instID == 0) {
            $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');
        }
        if (intval($this->input->post('instID'))) {
            $this->data['instID'] = intval($this->input->post('instID'));
        }
        if (isset($this->data['instID'])) {
            $this->db->where("instID", intval($this->data['instID']));
        } else if ($this->user->instID) {
            $this->db->where("instID", $this->user->instID);
        }

        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');
        $this->data['staffPosition'] = array('استاذ' => 'استاذ', 'استاذ مشارك' => 'استاذ مشارك', 'استاذ مساعد' => 'استاذ مساعد', 'محاضر' => 'محاضر', 'مساعد تدريس' => 'مساعد تدريس',);
        $this->data['staffStatus'] = array('على راس العمل' => 'على راس العمل', 'منتدب' => 'منتدب', 'معار' => 'معار', 'مبعوث' => 'مبعوث', 'متعاقد' => 'متعاقد', 'اجازة' => 'اجازة', 'معاش' => 'معاش','أخرى' => 'أخرى',);

        // Fetch a staff or set a new one
        if ($id) {
            $this->db->join('faculty', 'faculty.facID = staff.facID');
            $this->data['staff'] = $this->staff_m->get($id);
            count($this->data['staff']) || $this->data['errors'][] = 'staff could not be found';
        } else {
            $this->data['staff'] = $this->staff_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->staff_m->rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->staff_m->array_from_post(array('facID', 'staffName', 'staffPosition', 'staffHireDate', 'staffBirthDate', 'staffStatus', 'staffGenrSpec', 'staffSpec', 'staffHigherQualification', 'staffResearchTitle', 'staffDegreeDate', 'staffStatusStartDate', 'staffStatusEndDate',));
            $this->staff_m->save($data, $id);
            redirect('admin/staff');
        }

        // Load the view
        $this->data['subview'] = 'admin/staff/edit';
        $this->load->view('admin/main_page', $this->data);
    }

    public function delete($id) {
        $this->staff_m->delete($id);
        redirect('admin/staff');
    }

}
