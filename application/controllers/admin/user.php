<?php

class User extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('faculty_m');
        $this->load->model('program_m');
        $this->load->model('group_m');
    }

    public function index() {
//        $this->db->join('group', 'group.groupID = userGroup');
        // TODO: display all users for superusers or Display users in the admin instituton 

        $this->data['users'] = $this->user_m->get();          // Fetch all users 
        $this->data['subview'] = 'admin/user/index';          // Set SubView
        $this->load->view('admin/main_page', $this->data);   // Load view
    }

    public function edit($id = NULL) {
        // Fetch a user or set a new one
        if ($id) {
            $this->data['user'] = $this->user_m->get($id);
            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['user'] = $this->user_m->get_new();
        }
        $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(),'اختر المؤسسة');

        !$this->input->post('instID') || $this->db->where('instID', $this->input->post('instID', TRUE));
        $this->data['faculties'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(),'اختر الكلية');

        !$this->input->post('facID') || $this->db->where('facID', $this->input->post('facID', TRUE));
        $this->data['programs'] = dropdown_listing('progID', 'progName', $this->program_m->get(),'اختر البرنامج');

        // Set up the form
        $id || $this->user_m->rules_admin['userPass']['rules'] .= '|required';
        $this->form_validation->set_rules($this->user_m->rules_admin);
        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            echo 'ok';
            $data = $this->user_m->array_from_post(array('userName', 'userMail', 'userPass', 'userPhone', 'instID', 'userGroup', 'userStatus', 'facID', 'progID'));
            dump($data);
            if (empty($this->input->post('userPass'))) {
                unset($data['userPass']);
            }

            if ($this->form_validation->run() == TRUE) {
                $this->user_m->save($data, $id);
                redirect('admin/user');
            }
        }
        // Load the view
        $this->data['subview'] = 'admin/user/edit';          // Set SubView
        $this->load->view('admin/main_page', $this->data);   // Load view
    }

    public function delete($id) {
        $this->user_m->delete($id);
        redirect('manbord/user');
    }

    public function _unique_email($str) {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user

        $id = $this->uri->segment(4);
        $this->db->where('userMail', $this->input->post('userMail'));
        !$id || $this->db->where('userID !=', $id);
        $user = $this->user_m->get();

        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }

        return TRUE;
    }

    public function _hash($str) {
        return $this->user_m->hash($str);
    }

}
