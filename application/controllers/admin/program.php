<?php

class Program extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('program_m');
        $this->load->model('faculty_m');
    }

    public function index() {

        !$this->user->instID || $this->db->where("instID = {$this->user->instID}");
        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');

        $this->form_validation->set_rules(array('facID' => array('field' => 'facID', 'label' => 'Faculty', 'rules' => 'intval',)));
        if ($this->form_validation->run() == TRUE && $this->input->post('facID') !== 0) {
            $this->db->where(array('program.facID' => $this->faculty_m->array_from_post(array('facID'))['facID']));
        }

        // Fetch all program
        !$this->user->instID || $this->db->where("instID = {$this->user->instID}");
        $this->db->join('faculty', 'faculty.facID = program.facID');
        $this->data['program'] = $this->program_m->get();
        // Load view
        $this->data['subview'] = 'admin/program/index';
        $this->load->view('admin/main_page', $this->data);
    }

    public function edit($id = NULL) {
        if ($this->user->instID == 0) {
            $this->data['institutions'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');
        }

        if (intval($this->input->post('instID'))) {
            $this->data['instID'] = intval($this->input->post('instID'));
        }
        if (isset($this->data['instID'])) {
            $this->db->where("instID", intval($this->data['instID']));
        } else if ($this->user->instID) {
            $this->db->where("instID", $this->user->instID);
        }

        $this->data['faculty'] = dropdown_listing('facID', 'facName', $this->faculty_m->get(), 'اختر الكلية');

        // TODO : Confirm programs Types
        $this->data['progType'] = array(
            'دكتوراة' => 'دكتوراة',
            'ماجستير' => 'ماجستير',
            'دبلوم عالي' => 'دبلوم عالي',
            'بكلاريوس' => 'بكلاريوس',
            'دبلوم تقني' => 'دبلوم تقني',
        );



        // Fetch a program or set a new one
        if ($id) {
            $this->data['program'] = $this->program_m->get($id);
            count($this->data['program']) || $this->data['errors'][] = 'program could not be found';
        } else {
            $this->data['program'] = $this->program_m->get_new();
        }

        // Set up the form
        $rules = $this->program_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->program_m->array_from_post(array('progName', 'progStartDate', 'progLastReviewDate', 'progType', 'facID',));
            $this->program_m->save($data, $id);
            redirect('admin/program');
        }

        // Load the view
        $this->data['subview'] = 'admin/program/edit';
        $this->load->view('admin/main_page', $this->data);
    }

    public function delete($id) {
        $this->program_m->delete($id);
        redirect('admin/program');
    }

}
