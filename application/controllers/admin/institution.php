<?php

class Institution extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('faculty_m');
        $this->load->model('program_m');
        $this->load->model('user_m');
    }

    public function index() {
        
        
        
        $user_institution = $this->user_m->get($this->session->userdata('userID'))->instID;
        
        if(intval($user_institution) !== 0){
            $this->db->where('instID',$user_institution);
        }
        
        
        if($this->input->post('instName') !== null){
            $this->db->like ('instName',$this->input->post('instName'));
        }
        $this->data['user_institution'] = $user_institution;
        $this->data['institution'] = $this->institution_m->get();   // Fetch all institution
        
        $this->data['subview'] = 'admin/institution/index';         // Set SubView
        $this->load->view('admin/main_page', $this->data);          // Load view
    }

    public function edit($id = NULL) {
        // Fetch a institution or set a new one
        if ($id) {
            $this->data['institution'] = $this->institution_m->get($id);
            count($this->data['institution']) || $this->data['errors'][] = 'institution could not be found';
        } else {
            $this->data['institution'] = $this->institution_m->get_new();
        }
        // Set up the form
        $this->form_validation->set_rules($this->institution_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $this->institution_m->save($this->institution_m->array_from_post(array('instName', 'instMail', 'instPhone', 'instFax', 'instFoundationDate', 'instAddress')), $id);
            redirect('admin/institution');
        }

        $this->data['subview'] = 'admin/institution/edit';  // Set SubView
        $this->load->view('admin/main_page', $this->data);  // Load the view
    }

    public function delete($id) {
        $this->institution_m->delete($id);
        redirect('admin/institution');
    }

}
