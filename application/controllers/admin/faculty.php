<?php

class Faculty extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('faculty_m');
        $this->load->model('institution_m');
    }

    public function index() {
        $this->data['institution'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر مؤسسة');


        $this->form_validation->set_rules(array('instID' => array('field' => 'instID', 'label' => 'Institution', 'rules' => 'intval',)));

        if ($this->form_validation->run() == TRUE and $this->input->post('instID')) {
            $data = $this->institution_m->array_from_post(array('instID'));
            $this->db->where('institution.instID', $data['instID']);
        }

        if (intval($this->user->instID) > 0) {
            $this->db->where('institution.instID', intval($this->user->instID));
        }

        // Fetch all faculty
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->data['faculty'] = $this->faculty_m->get();

        // Load view
        $this->data['subview'] = 'admin/faculty/index';
        $this->load->view('admin/main_page', $this->data);
    }

    public function edit($id = NULL) {

        $this->data['institution'] = dropdown_listing('instID', 'instName', $this->institution_m->get(), 'اختر المؤسسة');


        // Fetch a faculty or set a new one
        if ($id) {
            $this->data['faculty'] = $this->faculty_m->get($id);
            count($this->data['faculty']) || $this->data['errors'][] = 'faculty could not be found';
        } else {
            $this->data['faculty'] = $this->faculty_m->get_new();
        }

        // Set up the form
        $rules = $this->faculty_m->rules;
        $this->form_validation->set_rules($rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->faculty_m->array_from_post(array('facName', 'facMail', 'facPhone', 'facFax', 'facFoundationDate', 'facAddress', 'instID'));
            $this->faculty_m->save($data, $id);
            redirect('admin/faculty');
        }

        // Load the view
        $this->data['subview'] = 'admin/faculty/edit';
        $this->load->view('admin/main_page', $this->data);
    }

    public function delete($id) {
        $this->faculty_m->delete($id);
        redirect('admin/faculty');
    }

}
