<?php

class Home extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['subview'] = 'admin/home';     // Set SubView
        $this->load->view('admin/main_page', $this->data);  // Load view
    }

}
