<?php

class Evalelement extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('evalelement_m');
    }

    public function index() {
        $this->data['evalelement'] = $this->evalelement_m->get();   // Fetch all evalelement
        $this->data['subview'] = 'admin/evalelement/index';         // Set SubView
        $this->load->view('admin/main_page', $this->data);          // Load view
    }

    public function edit($id = NULL) {
        $this->data['elementParent'] = dropdown_listing('elementID', 'elementTitle2', $this->evalelement_m->get());

        // Fetch a evalelement or set a new one
        if ($id) {
            $this->data['evalelement'] = $this->evalelement_m->get($id);
            count($this->data['evalelement']) || $this->data['errors'][] = 'evalelement could not be found';
        } else {
            $this->data['evalelement'] = $this->evalelement_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->evalelement_m->rules);

        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->evalelement_m->array_from_post(array('elementTitle1','elementTitle2', 'elementParent', 'elementEquation', 'elementSymbol', 'elementWeight', 'elementTypicalDgree','editValue'));
            $this->evalelement_m->save($data, $id);
            redirect("admin/evalelement/index/$evalID");
        }

        $this->data['subview'] = 'admin/evalelement/edit';      // Set SubView
        $this->load->view('admin/main_page', $this->data);      // Load the view
    }

    public function delete($id) {
        $this->evalelement_m->delete($id);
        redirect('admin/evalelement');
    }

    public function evaluat($evalID) {
        $new_sub_elements = array();
        $new_sub_sub_elements = array();
        // geting level 0 evaluation elements 
        $elements = $this->evalelement_m->get_by_join(array('elementParent' => '0', 'evalID' => $evalID));

        foreach ($elements as $element) {

            // geting level 1 evaluation elements 
            $sub_elements = $this->evalelement_m->get_by_join(array('elementParent' => $element->elementID, 'evalID' => $evalID));
            foreach ($sub_elements as $sub_element) {
                // getting level 2 evaluation elements
                $sub_sub_elements = $this->evalelement_m->get_by_join(array('elementParent' => $element->elementID, 'evalID' => $evalID));
                $sub_sub_element_result[$sub_element->elementID] = $this->evalelement_m->equationProcessing($sub_sub_elements, $sub_element->elementSymbol);
                //dump($sub_sub_element_result[$sub_element->elementID]);

                if (isset($sub_sub_element_result[$sub_element->elementID]['arrays'])) {
                    $x = $sub_sub_element_result[$sub_element->elementID]['arrays'];
                    $new_sub_sub_elements = array_merge($new_sub_sub_elements, $x);
                }
                
            }

//            $sub_element_result[$element->elementID] = $this->evalelement_m->equationProcessing($sub_elements, $element->elementSymbol, $sub_sub_element_result);
//            if (isset($sub_element_result[$element->elementID]['arrays'])) {
//                $x = $sub_element_result[$element->elementID]['arrays'];
//                $new_sub_elements = array_merge($new_sub_elements, $x);
//            }
        }
//        $element_result = $this->evalelement_m->equationProcessing($elements, $element->elementSymbol, $sub_element_result);
//        $new_elements = $element_result['arrays'];
//        $this->data ['new_subsubelements'] = $new_sub_sub_elements;
//        $this->data ['new_subelements'] = $new_sub_elements;
//        $this->data ['new_elements'] = $new_elements;
//        $this->data ['evalID'] = $evalID;
        // Load the view
//        $this->data['subview'] = 'admin/evalelement/evaluat';
//        $this->load->view('admin/main_page', $this->data);
    }

}
