<?php

class Course extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('institution_m');
        $this->load->model('course_m');
        $this->load->model('faculty_m');
    }

    public function index() {
        // Fetch all course
        $this->data['course'] = $this->course_m->get();

        // Load view
        $this->data['subview'] = 'admin/course/index';
        $this->load->view('admin/main_page', $this->data);
    }

    public function edit($id = NULL) {

        // Fetch a course or set a new one
        if ($id) {
            $this->data['course'] = $this->course_m->get($id);
            count($this->data['course']) || $this->data['errors'][] = 'course could not be found';
        } else {
            $this->data['course'] = $this->course_m->get_new();
        }

        // Set up the form
        $this->form_validation->set_rules($this->course_m->rules);

        // Process the form
        if (count($this->input->post()) > 1 && $this->form_validation->run() == TRUE) {
            $data = $this->course_m->array_from_post(array('cCode', 'cTitle', 'cContactHours', 'cReletedModules', 'cObjectives', 'cPrerequisites', 'cOutcomes', 'cOutline', 'cLearninigMethod', 'cRecommSoftware', 'cRecommHardware', 'cRecommBooks', 'cAssesstment',));
            $this->course_m->save($data, $data['cTitle'], TRUE);
            redirect('admin/course');
        }

        // Load the view
        $this->data['subview'] = 'admin/course/edit';
        $this->load->view('admin/main_page', $this->data);
    }

    public function delete($id) {
        $this->course_m->delete($id);
        redirect('admin/course');
    }

}
