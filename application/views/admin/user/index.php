<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">المستخدمين</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">المستخدمين</h4>
        <p class="category">مستخدمين نظام التقويم </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div>
            <?php echo anchor("admin/user/edit", '<i class="ti-plus"></i> إضافة مستخدم', 'class="btn btn-primary"'); ?>
        </div>
        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>المستخدم</th>
                    <th>المجموعة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($users)): ?>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?php echo anchor("admin/user/edit/$user->userID", $user->userName); ?></td>
                            <td><?php echo $user->userGroup; ?></td>
                            <td><?php echo btn_edit("admin/user/edit/$user->userID") ?></td>
                            <td><?php echo btn_delete("admin/user/delete/$user->userID") ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td class="text-center" colspan="5">ﻻ يوجد متغيرات</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>