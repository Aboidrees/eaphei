<ol class="breadcrumb">
    <li><?php echo anchor('', 'الرئيسية'); ?></li>
    <li><?php echo anchor("admin/user/index", 'المستخدمين'); ?></li>
    <li class = "active"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></li>
</ol>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></h3>
                <hr>

    </div>
    <div class="panel-body">
        <?php echo validation_errors('<div class="warning">', '</div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php ?>
        <?php echo form_open(); ?>
        <table class="table table-bordered" style="width: 80%; margin: auto;">
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('المؤسسة'); ?></th>
                <td><?php echo form_dropdown('instID', $institutions, set_value('instID', !empty($this->input->post('instID', TRUE)) ? $this->input->post('instID', TRUE) : $user->instID ), 'class="form-control" onchange="form.submit()"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('الكلية'); ?></th>
                <td><?php echo form_dropdown('facID', !empty($faculties) ? $faculties : array(), set_value('facID', !empty($this->input->post('facID', TRUE)) ? $this->input->post('facID', TRUE) : $user->facID ), 'class="form-control" onchange="form.submit()"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('البرنامج'); ?></th>
                <td><?php echo form_dropdown('progID', !empty($programs) ? $programs : array(), set_value('progID', !empty($this->input->post('progID', TRUE)) ? $this->input->post('progID', TRUE) : $user->progID ), 'class="form-control"  onchange="form.submit()"'); ?></td>
            </tr>
        </table>
        <?php
        echo form_close();
        echo form_open();
        ?>
        <table class="table table-bordered" style="width: 80%; margin: auto;">
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('اسم المستخدم'); ?></th>
                <td><?php echo form_input('userName', set_value('userName', $user->userName), 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('البريد الالكتروني'); ?></th>
                <td><?php echo form_input('userMail', set_value('userMail', $user->userMail), 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('كلمة المرور'); ?></th>
                <td><?php echo form_password('userPass', '', 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('تأكيد كلمة المرور'); ?></th>
                <td><?php echo form_password('userPass_confirm', '', 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('تلفون'); ?></th>
                <td><?php echo form_input('userPhone', set_value('userPhone', $user->userPhone), 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('المجموعة'); ?></th>
                <td><?php echo form_dropdown('userGroup', array('admin'=>'admin','user'=>'user'), set_value('userGroup', $user->userGroup), 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"><?php echo form_label('الحالة'); ?></th>
                <td>

                    <label class="btn btn-success">فعال
                        <?php echo form_radio('userStatus', set_value('userStatus', $user->userStatus ? $user->userStatus : 'active'), $user->userStatus == 'active'); ?>
                    </label>
                    <label class="btn btn-danger">غير فعال 
                        <?php echo form_radio('userStatus', set_value('userStatus', $user->userStatus ? $user->userStatus : 'not active'), $user->userStatus == 'not active'); ?>
                    </label>
                </td>
            </tr>
            <tr>
                <th class="info text-primary" width="25%"></th>
                <td>
                    <?php echo form_hidden('instID', !empty($this->input->post('instID', TRUE)) ? intval($this->input->post('instID', TRUE)) : intval($user->instID) ); ?>
                    <?php echo form_hidden('facID', !empty($this->input->post('facID', TRUE)) ? intval($this->input->post('facID', TRUE)) : intval($user->facID) ); ?>
                    <?php echo form_hidden('progID', !empty($this->input->post('progID', TRUE)) ? intval($this->input->post('progID', TRUE)) : intval($user->progID) ); ?>
                    <?php echo form_submit('add', 'أضف', 'class="btn btn-primary"'); ?>
                </td>
            </tr>
        </table>
        <?php echo form_close(); ?>
    </div>
</div>

