<ol class="breadcrumb">
    <li><?php echo anchor('', 'الرئيسية'); ?></li>
    <li><?php echo anchor("admin/user/index", 'المستخدمين'); ?></li>
    <li class = "active"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></h4>
        <p class="category">مستخدمين نظام التقويم </p>
        <hr>
    </div>
    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_dropdown('instID', $institutions, set_value('instID', !empty($this->input->post('instID', TRUE)) ? $this->input->post('instID', TRUE) : $user->instID ), 'class="form-control" onchange="form.submit()" id="instID"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_dropdown('facID', !empty($faculties) ? $faculties : array(), set_value('facID', !empty($this->input->post('progID', TRUE)) ? $this->input->post('progID', TRUE) : $user->facID ), 'class="form-control" id="facID" onchange="form.submit()"'); ?>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <?php echo form_dropdown('progID', !empty($programs) ? $programs : array(), set_value('progID', !empty($this->input->post('progID', TRUE)) ? $this->input->post('progID', TRUE) : $user->progID ), 'class="form-control"  id="progID" onchange="form.submit()"'); ?>
                </div>
            </div>
        </div>
        <?php
        echo form_close();
        echo form_open();
        ?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_input('userName', set_value('userName', $user->userName), 'placeholder="اسم المستخدم" class="form-control" id="userName"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_input('userMail', set_value('userMail', $user->userMail), 'placeholder="البريد الالكتروني" class="form-control" id="userMail"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_input('userPhone', set_value('userPhone', $user->userPhone), 'placeholder="رقم الهاتف" class="form-control" id="userPhone"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_password('userPass', '', 'placeholder="كلمة المرور" class="form-control" id="userPass"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_password('userPass_confirm', '', 'placeholder="تأكيد كلمة المرور" class="form-control" id="userPass_confirm"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-md-5">
                <div class="form-group">
                    <?php echo form_label('المجموعة'); ?>
                    <label class="btn btn-success">Administrator
                        <?php echo form_radio('userGroup', set_value('userGroup', $user->userGroup ? $user->userGroup : 'admin'), $user->userGroup == 'admin'); ?>
                    </label>
                    <label class="btn btn-danger">User 
                        <?php echo form_radio('userGroup', set_value('userGroup', $user->userGroup ? $user->userGroup : 'user'), $user->userGroup == 'user'); ?>
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php echo form_label('الحالة'); ?>
                    <label class="btn btn-success">فعال
                        <?php echo form_radio('userStatus', set_value('userStatus', $user->userStatus ? $user->userStatus : 'active'), $user->userStatus == 'active'); ?>
                    </label>
                    <label class="btn btn-danger">غير فعال 
                        <?php echo form_radio('userStatus', set_value('userStatus', $user->userStatus ? $user->userStatus : 'not active'), $user->userStatus == 'not active'); ?>
                    </label>
                </div>
            </div>
        </div>
        <?php echo form_hidden('instID', !empty($this->input->post('instID', TRUE)) ? intval($this->input->post('instID', TRUE)) : intval($user->instID) ); ?>
        <?php echo form_hidden('facID', !empty($this->input->post('facID', TRUE)) ? intval($this->input->post('facID', TRUE)) : intval($user->facID) ); ?>
        <?php echo form_hidden('progID', !empty($this->input->post('progID', TRUE)) ? intval($this->input->post('progID', TRUE)) : intval($user->progID) ); ?>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>

        <?php echo form_close(); ?>
    </div>
</div>

