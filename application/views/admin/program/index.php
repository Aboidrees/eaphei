<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">البرامج</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">البرامج</h4>
        <p class="category">برامج الكليات التابعة لمؤسسات التعليم العالي </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div>
            <?php echo form_open(); ?> 
            <?php echo form_dropdown('facID', $faculty, set_value('facID'), 'class="form-control" onChange="form.submit()"'); ?> 
            <?php echo form_close(); ?>
            <br>
        </div>
        <div>
            <?php echo anchor('admin/program/edit', '<i class="ti-plus"></i> إضافة برنامج', 'class="btn btn-primary"'); ?>
        </div>

        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>اسم البرنامج</th>
                    <th>تاريخ البدء</th>
                    <th>تاريخ اخر مراجعة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($program)) {
                    foreach ($program as $program) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/program/edit/$program->progID", $program->progName); ?></td>
                            <td><?php echo @date('Y-m-d', $program->progStartDate); ?></td>
                            <td><?php echo @date('Y-m-d', $program->progLastReviewDate); ?></td>
                            <td><?php echo btn_edit("admin/program/edit/$program->progID") ?></td>
                            <td><?php echo btn_delete("admin/program/delete/$program->progID") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">ﻻ يوجد برامج</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>