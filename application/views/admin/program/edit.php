<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('program', 'البرامج'); ?></li>
    <li class="active"><?php echo empty($program->progID) ? 'إضافة برنامج' : "تعديل  $program->progName"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($faculty->facID) ? 'إضافة برنامج' : "تعديل  $program->progName"; ?></h4>
        <p class="category">برامج الكليات التابعة لمؤسسات التعليم العالي</p>
        <hr>
    </div>

    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php if (isset($institutions)): ?>
            <?php echo form_open(); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_dropdown('instID', $institutions, set_value('instID'), 'onchange="form.submit()" class="form-control" id="instID"'); ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        <?php endif; ?>
        <?php echo form_open(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('الكلية', 'facID'); ?>
                    <?php echo form_dropdown('facID', $faculty, set_value('facID', $program->facID), 'class="form-control" id="facID"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('اسم البرنامج', 'progName'); ?>
                    <?php echo form_input('progName', set_value('progName', $program->progName), 'placeholder="بكلاريوس العلوم في ....." class="form-control" id="progName"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('نوع البرنامج', 'progType'); ?>
                    <?php echo form_dropdown('progType', $progType, set_value('progType', $program->progType), 'class="form-control" id="progType"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('تاريخ بداء تنفيذ البرنامج', 'progStartDate'); ?>
                    <?php echo form_date('progStartDate', set_value('progStartDate', date('Y-m-d', $program->progStartDate)), 'class="form-control right" id="progStartDate"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('تاريخ آخر مراجعة وتحديث', 'progLastReviewDate'); ?>
                    <?php echo form_date('progLastReviewDate', set_value('progLastReviewDate', date('Y-m-d', $program->progLastReviewDate)), 'class="form-control right" id="progLastReviewDate"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>
        <?php echo form_close(); ?> </div>
</div>
<script>
    $(function () {
        $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
    });
</script> 
