<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor("admin/evalelement/index", 'عناصر التقويم'); ?></li>
    <li class="active"><?php echo empty($evalelement->elementID) ? 'إضافة العناصر' : "تعديل  $evalelement->elementTitle2"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($evalelement->elementID) ? 'إضافة عنصر' : "تعديل  $evalelement->elementTitle2"; ?></h4>
        <p class="category">عناصر تقويم البرامج الدراسية للمؤسسات التابعة للتعليم العالي</p>
        <hr>
    </div>

    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العنوان في استمارة جمع البيانات', 'elementTitle1'); ?>
                    <?php echo form_input('elementTitle1', set_value('elementTitle1', $evalelement->elementTitle1), 'placeholder="عنوان عنصر التقويم..." class="form-control" id="elementTitle1"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العنوان في استمارة التقويم', 'elementTitle2'); ?>
                    <?php echo form_input('elementTitle2', set_value('elementTitle2', $evalelement->elementTitle2), 'placeholder="عنوان عنصر التقويم..." class="form-control" id="elementTitle2"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العنصر الاصل', 'elementParent'); ?>
                    <?php echo form_dropdown('elementParent', $elementParent, set_value('elementParent', $evalelement->elementParent), 'class="form-control" id="elementParent"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <?php echo form_label('رمز عنصر التقويم', 'elementSymbol'); ?>
                    <?php echo form_input('elementSymbol', set_value('elementSymbol', $evalelement->elementSymbol), 'placeholder="رمز عنصر التقويم" class="form-control" id="elementSymbol"'); ?>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <?php echo form_label('وزن عنصر التقويم', 'elementWeight'); ?>
                    <?php echo form_input('elementWeight', set_value('elementWeight', $evalelement->elementWeight), 'placeholder="وزن عنصر التقويم" class="form-control" id="elementWeight"'); ?>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <?php echo form_label('درجة الكلية الانموذج', 'elementTypicalDgree'); ?>
                    <?php echo form_input('elementTypicalDgree', set_value('elementTypicalDgree', $evalelement->elementTypicalDgree), 'placeholder="درجة الكلية الانموذج" class="form-control" id="elementTypicalDgree"'); ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('معادلة عنصر التقويم', 'elementSymbol'); ?>
                    <?php echo form_input('elementEquation', set_value('elementEquation', $evalelement->elementEquation), 'placeholder="معادلة عنصر التقويم" class="form-control" dir="ltr"  id="elementEquation"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="btn btn-success">فعالة
                        <?php echo form_radio('editValue', set_value('editValue', 'yes'), $evalelement->editValue == 'yes'); ?>
                    </label>
                    <label class="btn btn-danger">غير فعالة
                        <?php echo form_radio('editValue', set_value('editValue', 'no'), $evalelement->editValue == 'no'); ?>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br><br>


        <?php echo form_close(); ?>
    </div>
</div>

