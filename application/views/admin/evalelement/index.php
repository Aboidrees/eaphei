<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">عناصر التقويم</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">عناصر التقويم</h4>
        <p class="category">عناصر تقويم البرامج الدراسية للمؤسسات التابعة للتعليم العالي </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div>
            <?php echo anchor("admin/evalelement/edit", '<i class="ti-plus"></i> إضافة عنصر', 'class="btn btn-primary"'); ?>
        </div>

        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>العنصر</th>
                    <th>رمز العنصر</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($evalelement)) {
                    foreach ($evalelement as $evalelement) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/evalelement/edit/$evalelement->elementID", $evalelement->elementTitle2); ?></td>
                            <td><?php echo $evalelement->elementSymbol; ?></td>
                            <td><?php echo btn_edit("admin/evalelement/edit/$evalelement->elementID") ?></td>
                            <td><?php echo btn_delete("admin/evalelement/delete/$evalelement->elementID") ?></td>
                        </tr>

                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">ﻻ يوجد عناصر</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>