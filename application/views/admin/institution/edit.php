<ol class="breadcrumb">
    <li><?php echo anchor('admin/', 'الرئيسية'); ?></li>
    <li><?php echo anchor('admin/institution', 'المؤسسات'); ?></li>
    <li class="active"><?php echo empty($institution->instID) ? 'إضافة برنامج' : "تعديل  $institution->instName"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($institution->instID) ? 'إضافة مؤسسة' : "تعديل  $institution->instName"; ?></h4>
        <p class="category">  مؤسسات التعليم العالي (الجامعات والكليات الحكومية والاهلية)</p>
        <hr>
    </div>
    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>

        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('المؤسسة', 'instName'); ?>
                    <?php echo form_input('instName', set_value('instName', $institution->instName), 'class="form-control" id="instName"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('تاريخ التأسيس', 'instFoundationDate'); ?>
                    <?php echo form_date('instFoundationDate', set_value('instFoundationDate', date('Y-m-d', $institution->instFoundationDate)), 'class="form-control right datepicker" id="instFoundationDate"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'instPhone'); ?>
                            <?php echo form_input('instPhone', set_value('instPhone', $institution->instPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="instPhone"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('فاكس', 'instFax'); ?>
                            <?php echo form_input('instFax', set_value('instFax', $institution->instFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="instFax"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'instMail'); ?>
                            <?php echo form_input('instMail', set_value('instMail', $institution->instMail), 'placeholder="inst@inst.ext" class="form-control" id="instMail"'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العنوان', 'instAddress'); ?>
                    <?php echo form_textarea('instAddress', set_value('instAddress', $institution->instAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="instAddress"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>
        <?php echo form_close(); ?>
    </div>
</div>