<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">المؤسسات</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">المؤسسات</h4>
        <p class="category">مؤسسات التعليم العالي (الجامعات والكليات الحكومية والاهلية)</p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div>
            <?php echo form_open(); ?> 
            <?php echo form_input('instName', '', 'placeholder="ابحث عن مؤسسة ...." class="form-control"'); ?> 
            <?php echo form_close(); ?> 
            <br />
        </div>
        <div>    
            <?php echo anchor('admin/institution/edit', '<i class="ti-plus"></i> إضافة مؤسسة', 'class="btn btn-primary"'); ?>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>اسم المؤسسة</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($institution)) {
                    foreach ($institution as $institution) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/institution/edit/$institution->instID", $institution->instName); ?></td>
                            <td><?php echo btn_edit("admin/institution/edit/$institution->instID") ?></td>
                            <td><?php echo btn_delete("admin/institution/delete/$institution->instID") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="3">ﻻ يوجد مؤسسات</td>
                    </tr>
                    <?php
                }
                ?>

            </tbody>
        </table>
    </div>
</div>