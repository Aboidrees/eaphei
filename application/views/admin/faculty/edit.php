<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('admin/faculty', 'الكليات'); ?></li>
    <li class = "active"><?php echo empty($faculty->facID) ? 'إضافة كلية' : "تعديل  $faculty->facName"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($faculty->facID) ? 'إضافة كلية' : "تعديل  $faculty->facName"; ?></h4>
        <p class="category">الكليات التابعة لمؤسسات التعليم العالي</p>
        <hr>
    </div>
    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>

        <?php if (intval($this->user->instID) == 0): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_label('المؤسسة', 'instID'); ?>
                        <?php echo form_dropdown('instID', $institution, set_value('instID', $faculty->instID), 'class="form-control" id="instID" '); ?>
                    </div>
                </div>
            </div>
            <?php
        else:
            echo form_hidden('instID', intval($this->user->instID));
        endif;
        ?>

        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('الكلية', 'facName'); ?>
                    <?php echo form_input('facName', set_value('facName', $faculty->facName), 'placeholder="كلية الهندسة، كلية القانون ... الخ" class="form-control" id="facName"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('تاريخ التأسيس', 'facFoundationDate'); ?>
                    <?php echo form_date('facFoundationDate', set_value('facFoundationDate', date('Y-m-d', $faculty->facFoundationDate)), 'class="form-control right datepicker" id="facFoundationDate"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'facPhone'); ?>
                            <?php echo form_input('facPhone', set_value('facPhone', $faculty->facPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="facPhone"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('فاكس', 'facFax'); ?>
                            <?php echo form_input('facFax', set_value('facFax', $faculty->facFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="facFax"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'facMail'); ?>
                            <?php echo form_input('facMail', set_value('facMail', $faculty->facMail), 'placeholder="fac@inst.ext" class="form-control" id="facMail"'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العنوان', 'facAddress'); ?>
                    <?php echo form_textarea('facAddress', set_value('facAddress', $faculty->facAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="facAddress"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>
        <?php echo form_close(); ?>
    </div>
</div>
<script>
    $(function () {
        $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
    });
</script>