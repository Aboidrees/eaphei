<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">الكليات</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">الكليات</h4>
        <p class="category">الكليات التابعة لمؤسسات التعليم العالي </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <?php if (intval($this->user->instID) == 0): ?>
            <div>
                <?php echo form_open(); ?> 
                <?php echo form_dropdown('instID', $institution, set_value('instID'), 'class="form-control" onchange="form.submit()"'); ?>
                <?php echo form_close(); ?> 
                <br />
            </div>
        <?php endif; ?>

        <div>    
            <?php echo anchor('admin/faculty/edit', '<i class="ti-plus"></i> إضافة كلية', 'class="btn btn-primary"'); ?>
        </div>

        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>اسم الكلية</th>
                    <th>المؤسسة</th>
                    <th>الهاتف / الفاكس</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($faculty)) {
                    foreach ($faculty as $faculty) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/faculty/edit/$faculty->facID", $faculty->facName); ?></td>
                            <td><?php echo $faculty->instName; ?></td>
                            <td><?php echo 'هاتف ' . $faculty->facPhone . ' / فاكس ' . $faculty->facFax; ?></td>
                            <td><?php echo btn_edit("admin/faculty/edit/$faculty->facID") ?></td>
                            <td><?php echo btn_delete("admin/faculty/delete/$faculty->facID") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="4">ﻻ يوجد كليات</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>