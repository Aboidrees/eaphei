<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">المجموعات</li>
</ol>
<section>

    <?php echo anchor('admin/group/edit', '<i class="ti-plus"></i> إضافة مجموعة', 'class="btn btn-primary"'); ?>
    <table class="table table-striped" >
        <thead>
            <tr>
                <th>اسم المجموعة</th>
                <th>تعديل</th>
                <th>حذف</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($group)) {
                foreach ($group as $group) {
                    ?>
                    <tr>
                        <td><?php echo anchor("admin/group/edit/$group->groupID", $group->groupName); ?></td>
                        <td><?php echo btn_edit("admin/group/edit/$group->groupID") ?></td>
                        <td><?php echo btn_delete("admin/group/delete/$group->groupID") ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td class="text-center" colspan="4">ﻻ يوجد مجموعات</td>
                </tr>
                <?php
            }
            ?>
        </tbody>

    </table>
</section>