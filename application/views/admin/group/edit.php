<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('admin/group', 'المجموعات'); ?></li>
    <li class = "active"><?php echo empty($group->groupID) ? 'إضافة مجموعة' : "تعديل  $group->groupName"; ?></li>
</ol>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo empty($group->groupID) ? 'إضافة مجموعة' : "تعديل  $group->groupName"; ?></h3>
    </div>
    <div class="panel-body">
        <?php echo validation_errors('<div class="warning">', '</div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>
        <table class="table">
            <tr>
                <td>اسم المجموعة</td>
                <td>
                    <?php echo form_input('groupName', set_value('groupName', $group->groupName), 'class="form-control"'); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php echo form_submit('', 'Save', 'class="btn btn-primary"'); ?>
                </td>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
