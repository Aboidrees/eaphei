<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('course', 'الدورات'); ?></li>
    <li class="active"><?php echo empty($course->cCode) ? 'إضافة دورة' : "تعديل  $course->cTitle"; ?></li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($faculty->facID) ? 'إضافة دورة' : "تعديل  $course->cTitle"; ?></h4>
        <p class="category">الدورات التدريبية</p>
        <hr>
    </div>

    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('كود الدورة', 'cCode'); ?>
                    <?php echo form_input('cCode', set_value('cCode', $course->cCode), 'class="form-control" id="cCode"'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('ساعات الدورة', 'cContactHours'); ?>
                    <?php echo form_input('cContactHours', set_value('cContactHours', $course->cContactHours), 'class="form-control" id="cContactHours"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('عنوان الدورة', 'cTitle'); ?>
                    <?php echo form_input('cTitle', set_value('cTitle', $course->cTitle), 'class="form-control" id="cTitle"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('المستهدفين', 'cAttendence'); ?>
                    <?php echo form_input('cAttendence', set_value('cAttendence', $course->cAttendence), 'class="form-control" id="cAttendence"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('الدورات المتعلقة بالدورة', 'cReletedModules'); ?>
                    <?php echo form_textarea('cReletedModules', set_value('cReletedModules', $course->cReletedModules), 'class="form-control" id="cReletedModules"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('اهداف الدورة', 'cObjectives'); ?>
                    <?php echo form_textarea('cObjectives', set_value('cObjectives', $course->cObjectives), 'class="form-control" id="cObjectives"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('متطلبات الدورة', 'cPrerequisites'); ?>
                    <?php echo form_textarea('cPrerequisites', set_value('cPrerequisites', $course->cReletedModules), 'class="form-control" id="cPrerequisites"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('مخرجات الدورة', 'cOutcomes'); ?>
                    <?php echo form_textarea('cOutcomes', set_value('cOutcomes', $course->cOutcomes), 'class="form-control" id="cOutcomes"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('محتويات الدورة', 'cOutline'); ?>
                    <?php echo form_textarea('cOutline', set_value('cOutline', $course->cOutline), 'class="form-control" id="cOutline"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('طريقة التدريس', 'cLearninigMethod'); ?>
                    <?php echo form_textarea('cLearninigMethod', set_value('cLearninigMethod', $course->cLearninigMethod), 'class="form-control" id="cLearninigMethod"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('البرمجيات المقترحة', 'cRecommSoftware'); ?>
                    <?php echo form_textarea('cRecommSoftware', set_value('cRecommSoftware', $course->cRecommSoftware), 'class="form-control" id="cRecommSoftware"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('العتاد المقترح', 'cRecommHardware'); ?>
                    <?php echo form_textarea('cRecommHardware', set_value('cRecommHardware', $course->cRecommHardware), 'class="form-control" id="cRecommHardware"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('الكتب المقترحة', 'cRecommBooks'); ?>
                    <?php echo form_textarea('cRecommBooks', set_value('cRecommBooks', $course->cRecommBooks), 'class="form-control" id="cRecommBooks"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('التقييم والمتابعة', 'cAssesstment'); ?>
                    <?php echo form_textarea('cAssesstment', set_value('cAssesstment', $course->cAssesstment), 'class="form-control" id="cAssesstment"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>
        <?php echo form_close(); ?> </div>
</div>

