<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">الدورات</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">الدورات</h4>
        <p class="category">برامج الكليات التابعة لمؤسسات التعليم العالي </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div>
            <?php echo anchor('admin/course/edit', '<i class="ti-plus"></i> إضافة دورة', 'class="btn btn-primary"'); ?>
        </div>

        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>اسم الدورة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($course)) {
                    foreach ($course as $course) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/course/edit/$course->cCode", $course->cTitle); ?></td>
                            <td><?php echo btn_edit("admin/course/edit/$course->cCode") ?></td>
                            <td><?php echo btn_delete("admin/course/delete/$course->cCode") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">ﻻ يوجد دورات</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>