<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('admin/staff', 'هيئة تدريس'); ?></li>
    <li class="active"><?php echo empty($staff->staffID) ? 'إضافة عضو هيئة تدريس' : "تعديل  $staff->staffName"; ?></li>
</ol>

<div class="card">
    <div class="header">
        <h4 class="title"><?php echo empty($faculty->facID) ? 'إضافة عضو هيئة تدريس' : "تعديل  $faculty->facName"; ?></h4>
        <p class="category">هيئة التدريس في مؤسسات التعليم العالي </p>
        <hr>
    </div>
    <div class="content">
        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button><span data-notify="icon" class="ti-bell"></span><span data-notify="message">', '</span></div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php if (isset($institutions)): ?>
            <?php echo form_open(); ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <?php echo form_dropdown('instID', $institutions, set_value('instID'), 'onchange="form.submit()" class="form-control" id="instID"'); ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        <?php endif; ?>

        <?php echo form_open(); ?>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <?php echo form_dropdown('facID', $faculty, set_value('facID', $staff->facID), 'class="form-control" id="facID"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <?php echo form_label('الاسم', 'staffName'); ?>
                    <?php echo form_input('staffName', set_value('staffName', $staff->staffName), 'placeholder="اسم عضو هيئة التدريس ....." class="form-control" id="staffName"'); ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('تاريخ الميلاد', 'staffBirthDate'); ?>
                    <?php echo form_date('staffBirthDate', set_value('staffBirthDate', date('Y-m-d', $staff->staffBirthDate)), 'class="form-control" min="10" max="100" id="staffBirthDate"'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('الدرجة الوظيفية', 'staffPosition'); ?>
                    <?php echo form_dropdown('staffPosition', $staffPosition, set_value('staffPosition', $staff->staffPosition), 'class="form-control" id="staffPosition"'); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('تاريخ التعيين', 'staffHireDate'); ?>
                    <?php echo form_date('staffHireDate', set_value('staffHireDate', date('Y-m-d', $staff->staffHireDate)), 'class="form-control" id="staffHireDate"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('التخصص العام', 'staffGenrSpec'); ?>
                    <?php echo form_input('staffGenrSpec', set_value('staffGenrSpec', $staff->staffGenrSpec), 'class="form-control" id="staffGenrSpec"'); ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('التخصص الدقيق', 'staffSpec'); ?>
                    <?php echo form_input('staffSpec', set_value('staffSpec', $staff->staffSpec), 'class="form-control" id="staffSpec"'); ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('أعلى مؤهل أكاديمي', 'staffHigherQualification'); ?>
                    <?php echo form_input('staffHigherQualification', set_value('staffHigherQualification', $staff->staffHigherQualification), 'class="form-control" id="staffHigherQualification"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php echo form_label('عنوان البحث لأعلى مؤهل اكاديمي', 'staffResearchTitle'); ?>
                    <?php echo form_input('staffResearchTitle', set_value('staffResearchTitle', $staff->staffResearchTitle), 'class="form-control" id="staffResearchTitle"'); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('تاريخ الدرجة', 'staffDegreeDate'); ?>
                    <?php echo form_date('staffDegreeDate', set_value('staffDegreeDate', date('Y-m-d', $staff->staffDegreeDate)), 'class="form-control" id="staffDegreeDate"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('الحالة', 'staffStatus'); ?>
                    <?php echo form_dropdown('staffStatus', $staffStatus, set_value('staffStatus', $staff->staffStatus), 'class="form-control" id="staffStatus"'); ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('تاريخ البداية', 'staffStatusStartDate'); ?>
                    <?php echo form_date('staffStatusStartDate', set_value('staffStatusStartDate', date('Y-m-d', $staff->staffStatusStartDate)), 'class="form-control" id="staffStatusStartDate"'); ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <?php echo form_label('تاريخ النهاية', 'staffStatusEndDate'); ?>
                    <?php echo form_date('staffStatusEndDate', set_value('staffStatusEndDate', date('Y-m-d', $staff->staffStatusEndDate)), 'class="form-control" id="staffStatusEndDate"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-info btn-fill btn-wd">حفظ</button>
            </div>
        </div>
        <br>        <br>        <br>
        <?php echo form_close(); ?>
    </div>
</div>
