<ol class="breadcrumb">
    <li><?php echo anchor('admin/home', 'الرئيسية'); ?></li>
    <li class = "active">هيئة التدريس</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">هيئة التدريس</h4>
        <p class="category">هيئة التدريس في مؤسسات التعليم العالي </p>
        <hr>
    </div>
    <div class="content table-responsive">
        <div calss="row">
            <?php echo form_open(); ?> 
            <?php if (isset($institution)): ?>
                <div class="col-lg-6">
                    <?php echo form_dropdown('instID', $institution, set_value('instID'), 'class="form-control" onChange="form.submit()"'); ?> 
                </div>
            <?php endif; ?>
            <div class="col-lg-6">
                <?php echo form_dropdown('facID', $faculty, set_value('facID'), 'class="form-control" onChange="form.submit()"'); ?> 
            </div>
            <?php echo form_close(); ?>
            <br>
        </div>
        <br>
        <div>
            <?php echo anchor('admin/staff/edit', '<i class="ti-plus"></i> إضافة عضو هيئة تدريس', 'class="btn btn-primary"'); ?>
        </div>

        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>الاسم</th>
                    <th>المؤسسة</th>
                    <th>تاريخ التعيين</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($staff)) {
                    foreach ($staff as $staff) {
                        ?>
                        <tr>
                            <td><?php echo anchor("admin/staff/edit/$staff->staffID", $staff->staffName); ?></td>
                            <td><?php echo $staff->instName; ?></td>
                            <td><?php echo date('Y-m-d', $staff->staffHireDate); ?></td>
                            <td><?php echo btn_delete("admin/staff/delete/$staff->staffID") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">ﻻ يوجد اعضاء هيئة تدريس</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>