<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor("assistancestaff", 'Specification'); ?></li>
            <li class="active"><?php echo empty($assistancestaff->aStaffID) ? 'Add Specification' : "Edit  $assistancestaff->aStaffJopTitleEN"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($assistancestaff->aStaffID) ? 'Add Specification' : "Edit  $assistancestaff->aStaffJopTitleEN"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="form-group row">
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                        <?php echo form_label('Specification Name', 'aStaffJopTitle'); ?>
                    </div>
                    <div class="col-lg-5 col-md-9 col-sm-8 col-xs-12">
                        <?php echo form_input('aStaffJopTitle', set_value('aStaffJopTitle', $assistancestaff->aStaffJopTitle), 'class="form-control" id="aStaffJopTitle"'); ?>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="submit" class="btn btn-success">Specification Save</button>
                </div>
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</section>

