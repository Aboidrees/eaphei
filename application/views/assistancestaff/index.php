<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li class="active">Assistance staff</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("assistancestaff/edit", '<i class="fa fa-plus"></i> Add Assistance staff', 'class="btn btn-success"'); ?>
        <hr style="margin: 2px 0">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Assistance staffs </h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('instName', '', 'placeholder="Search Programs ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
                <hr>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Assistance staff name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($assistancestaffs)) {
                            $i = 1;
                            foreach ($assistancestaffs as $assistancestaff) {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $assistancestaff->aStaffJopTitle; ?></td>
                                    <td><?php echo btn_edit("assistancestaff/edit/$assistancestaff->aStaffID/$assistancestaff->aStaffID"); ?></td>
                                    <td><?php echo btn_delete("assistancestaff/delete/$assistancestaff->aStaffID/$assistancestaff->aStaffID"); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">No Assistance staff</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
