<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?>
            <li><?php echo anchor('evalform', 'استمارات التقويم'); ?></li>
            <li class="active"><?php echo empty($evalform->efID) ? 'إضافة استمارة تقويم' : "تعديل  $evalform->efName"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($evalform->efID) ? 'إضافة استمارة تقويم' : "تعديل  $evalform->efName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo form_label('اسم الاستمارة', 'efName'); ?>
                            <?php echo form_input('efName', set_value('efName', $evalform->efName), 'class="form-control" id="efName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('القطاع', 'efSector'); ?>
                            <?php echo form_dropdown('efSector', $efSector, set_value('efSector', $evalform->efSector), 'class="form-control" id="efSector"'); ?>
                        </div>                       
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo form_label('وصف', 'efDesc'); ?>
                            <?php echo form_textarea('efDesc', set_value('efDesc', $evalform->efDesc), 'class="form-control" id="efDesc"'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

