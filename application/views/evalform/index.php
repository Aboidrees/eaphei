<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li class="active">استمارات التقويم</li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content container">
    <?php echo anchor("evalform/edit", '<i class="fa fa-plus fa-larg"></i> اضافة استمارة', 'class="btn btn-primary"'); ?>
    <hr style="margin: 2px 0">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">استمارات التقويم</h3>
            <div class="box-tools">
                <?php form_open(); ?>
                <div class="input-group input-group-sm" style="width: 200px;">
                    <?php echo form_input('efName', '', 'placeholder="ابحث عن استمارة ...." class="form-control pull-right"'); ?> 
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <?php echo form_close(); ?> 
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-1"></th>
                        <th>اسم الاستمارة</th>
                        <th>القطاع</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($evalforms)) {
                        foreach ($evalforms as $evalform) {
                            ?>
                            <tr>
                                <td class="text-center"><!--<?php echo btn_edit("evalform/edit/$evalform->efID"); ?> &nbsp; --><?php echo btn_delete("evalform/delete/$evalform->efID"); ?></td>
                                <td><?php echo anchor("evalelement/index/$evalform->efID", $evalform->efName); ?></td>
                                <td><?php echo $evalform->efSector; ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td class="text-center" colspan="5">ﻻ يوجد استمارات تقويم</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
