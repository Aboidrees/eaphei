<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li class="active">Qualification</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("qualification/edit", '<i class="fa fa-plus"></i> Add Qualification', 'class="btn btn-success"'); ?>
        <hr style="margin: 2px 0">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Qualifications </h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('instName', '', 'placeholder="Search Programs ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
                <hr>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Qualification name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($qualifications)) {
                            $i=1;
                            foreach ($qualifications as $qualification) {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $qualification->qName; ?></td>
                                    <td><?php echo btn_edit("qualification/edit/$qualification->qID/$qualification->qID"); ?></td>
                                    <td><?php echo btn_delete("qualification/delete/$qualification->qID/$qualification->qID"); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">No Qualification</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
