<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor("institution", "المؤسسات"); ?></li>
            <li class="active"><?php echo $institution->instName; ?></li>
        </ol>          
        <div class="row">
            <div class="col-sm-2">
                <?php echo anchor("faculty/edit/{$instID}", '<i class="fa fa-plus fa-larg"></i> اضافة كلية', 'class="btn btn-primary" style="width:100%"'); ?>
            </div>
            <div class="col-sm-10">
                <?php echo form_open(); ?> 
                <?php echo form_input('facName', set_value('facName', $this->input->post('facName',TRUE)), 'placeholder="ابحث عن كلية ...." class="form-control"'); ?> 
                <?php echo form_close(); ?> 
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php if (count($faculties)): ?>
            <div class="row">
                <?php foreach ($faculties as $faculty): ?>
                    <div class="col-sm-6 col-lg-4">
                        <?php $this->load->view("faculty/fac_module", $faculty); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>