<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor('faculty/index/'.$institution->instID, $institution->instName); ?></li>
            <li class="active"><?php echo empty($faculty->facID) ? 'إضافة كلية' : "تعديل  $faculty->facName"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($faculty->facID) ? 'إضافة كلية' : "تعديل  $faculty->facName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo form_label('الكلية', 'facName'); ?>
                            <?php echo form_input('facName', set_value('facName', $faculty->facName), 'class="form-control" id="facName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('تاريخ التأسيس', 'facFoundationDate'); ?>
                            <?php echo form_date('facFoundationDate', set_value('facFoundationDate',  $faculty->facFoundationDate), 'class="form-control" id="facFoundationDate"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'facEmail'); ?>
                            <?php echo form_input('facEmail', set_value('facEmail', $faculty->facEmail), 'placeholder="fac@fac.ext" class="form-control" id="facEmail"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('الشعار', 'facLogo'); ?>
                            <?php echo form_file('facLogo', set_value('facLogo', $faculty->facLogo), 'class="form-control" id="facLogo"'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'facPhone'); ?>
                            <?php echo form_input('facPhone', set_value('facPhone', $faculty->facPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="facPhone"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('فاكس', 'facFax'); ?>
                            <?php echo form_input('facFax', set_value('facFax', $faculty->facFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="facFax"'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('العنوان', 'facAddress'); ?>
                            <?php echo form_textarea('facAddress', set_value('facAddress', $faculty->facAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="facAddress"'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_dropdown("instID", $institutions, $faculty->instID || $instID, 'class="form-control"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

