<!-- Widget: user widget style 1 -->
<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-primary">
        <div class="widget-user-image">
            <img class="img-circle" src="public/assets/dist/img/moheLogo.png" alt="شعار الكلية">
        </div>
        <!-- /.widget-user-image -->
        <h4 class="widget-user-username"><?php echo anchor("department/index/{$facID}", $facName); ?></h4>
        <h5 class="widget-user-desc"><?php echo anchor("department/index/{$facID}", $facAddress); ?></h5>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li>
                <div class="btn-group">
                    <?php echo btn_edit("faculty/edit/{$instID}/{$facID}", 'class="btn btn-default btn-lg text-green"', '') ?>
                    <?php echo btn_delete("faculty/delete/{$instID}/{$facID}", 'class="btn btn-default btn-lg text-red"', '') ?>
                </div>
            </li>
            <li><a href="#">البرامج  <span class="pull-left badge bg-purple-gradient">000</span></a></li>
            <li><a href="#"> الاساتذة <span class="pull-left badge bg-purple-gradient">000</span></a></li>
            <li><a href="#">الطلاب  <span class="pull-left badge bg-purple-gradient">000</span></a></li>
        </ul>
    </div>
</div>
<!-- /.widget-user -->