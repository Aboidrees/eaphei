<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?>
            <li><?php echo anchor("institutions", 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$faculty->instID}", 'الكليات'); ?></li>
            <li><?php echo anchor("department/index/{$faculty->facID}", $faculty->facName); ?></li>
            <li class="active"><?php echo empty($department->depID) ? 'إضافة قسم' : "Edit  $department->depName"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($department->depID) ? 'إضافة قسم' : "تعديل  $department->depName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo form_label('اسم القسم', 'depName'); ?>
                            <?php echo form_input('depName', set_value('depName', $department->depName), 'class="form-control" id="depName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('تاريخ التأسيس', 'depFoundationDate'); ?>
                            <?php echo form_date('depFoundationDate', set_value('depFoundationDate', $department->depFoundationDate), 'class="form-control right datepicker" id="depFoundationDate"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'depEmail'); ?>
                            <?php echo form_input('depEmail', set_value('depEmail', $department->depEmail), 'placeholder="dep@dep.ext" class="form-control" id="depEmail"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('الشعار', 'depLogo'); ?>
                            <?php echo form_file('depLogo', set_value('depLogo', $department->depLogo), 'class="form-control" id="depLogo"'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'depPhone'); ?>
                            <?php echo form_input('depPhone', set_value('depPhone', $department->depPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="depPhone"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('فاكس', 'depFax'); ?>
                            <?php echo form_input('depFax', set_value('depFax', $department->depFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="depFax"'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('العنوان', 'depAddress'); ?>
                            <?php echo form_textarea('depAddress', set_value('depAddress', $department->depAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="depAddress"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <?php echo form_hidden('facID', $facID); ?>
            <button type="submit" class="btn btn-primary">حفظ بيانات القسم</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

