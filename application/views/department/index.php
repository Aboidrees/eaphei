<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor("institution", "المؤسسات"); ?></li>
            <li><?php echo anchor("faculty/index/{$faculty->instID}", $faculty->instName); ?></li>
            <li class="active"><?php echo $faculty->facName; ?></li>

        </ol>          
        <div class="row">
            <div class="col-sm-2">
                <?php echo anchor("department/edit/$facID", '<i class="fa fa-plus fa-larg"></i> اضافة قسم', 'class="btn btn-primary" style="width:100%"'); ?>
            </div>
            <div class="col-sm-10">
                <?php echo form_open(); ?> 
                <?php echo form_input('depName', set_value('depName', $this->input->post('depName', TRUE)), 'placeholder="ابحث عن قسم ...." class="form-control"'); ?> 
                <?php echo form_close(); ?> 
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php if (count($departments)): ?>
            <div class="row">
                <?php foreach ($departments as $department): ?>
                    <div class="col-sm-6 col-lg-4">
                        <?php $this->load->view("department/dep_module", $department); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>



