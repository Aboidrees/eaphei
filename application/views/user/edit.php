<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('account', 'المستخدمين '); ?></li>
            <li class="active"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <?php echo form_open('', 'role="form"'); ?>
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($user->userID) ? 'إضافة مستخدم' : "تعديل  $user->userName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 border-left">
                        <div class="form-group">
                            <?php echo form_label("المؤسسة", "instID"); ?>
                            <?php echo form_dropdown('instID', $institution, set_value('instID', !empty($this->input->post('instID', TRUE)) ? $this->input->post('instID', TRUE) : $user->instID ), 'class="form-control" onchange="form.submit()" id="instID"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("الكلية", "facID"); ?>
                            <?php echo form_dropdown('facID', $faculty, set_value('facID', !empty($this->input->post('facID', TRUE)) ? $this->input->post('facID', TRUE) : $user->facID ), 'class="form-control" id="facID" onchange="form.submit()"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("القسم", "depID"); ?>
                            <?php echo form_dropdown('depID', $department, set_value('depID', !empty($this->input->post('depID', TRUE)) ? $this->input->post('depID', TRUE) : $user->depID ), 'class="form-control" id="depID" onchange="form.submit()"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("البرنامج", "progID"); ?>
                            <?php echo form_dropdown('progID', $programs, set_value('progID', !empty($this->input->post('progID', TRUE)) ? $this->input->post('progID', TRUE) : $user->progID ), 'class="form-control"  id="progID" onchange="form.submit()"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 border-left">
                        <div class="form-group">
                            <?php echo form_label("اسم المستخدم", "userName"); ?>
                            <?php echo form_input('userName', set_value('userName', $user->userName), 'placeholder="اسم المستخدم" class="form-control" id="userName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("البريد الالكتروني", "userMail"); ?>
                            <?php echo form_input('userMail', set_value('userMail', $user->userMail), 'placeholder="البريد الالكتروني" class="form-control" id="userMail"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("كلمة المرور", "userPass"); ?>
                            <?php echo form_password('userPass', '', 'placeholder="كلمة المرور" class="form-control" id="userPass"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label("تأكيد كلمة المرور", "userPass_confirm"); ?>
                            <?php echo form_password('userPass_confirm', '', 'placeholder="تأكيد كلمة المرور" class="form-control" id="userPass_confirm"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo form_label("هاتف", "userPhone"); ?>
                            <?php echo form_input('userPhone', set_value('userPhone', $user->userPhone), 'placeholder="رقم الهاتف" class="form-control" id="userPhone"'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('مدير في النظام', 'userGroup', array('style' => "width:25%")); ?>
                            <?php echo form_checkbox('userGroup', 'admin ', $user->userGroup == 'admin', 'id="userGroup" class="flat-red" style = "position: absolute; opacity: 0;"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('يمكنه الدخول', 'userStatus', array('style' => "width:25%")); ?>
                            <?php echo form_checkbox('userStatus', 'active ', $user->userStatus == 'active', 'id="userStatus" class="flat-red" style = "position: absolute; opacity: 0;"'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" value="ok" class="btn btn-primary" name="save">Save</button>
            <?php echo validation_errors('<div class = "alert alert-danger alert-with-icon" data-notify = "container"><button type = "button" aria-hidden = "true" class = "close">×</button> <span data-notify = "icon" class = "fa fa-bell"></span><span data-notify = "message"> ', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>