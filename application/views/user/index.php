<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li class="active">المستخدمين</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <?php echo anchor("account/edit", '<i class="fa fa-plus"></i> إضافة مستخدم', 'class="btn btn-primary"'); ?>
                <br>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">المستخدمين</h3>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <?php echo form_input('username', '', 'placeholder="ابحث عن مستخدم ...." class="form-control pull-right"'); ?> 
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <?php echo form_close(); ?> 
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>المستخدم</th>
                                    <th>المجموعة</th>
                                    <th>حالة الحساب</th>
                                    <th>حذف</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($users)): ?>
                                    <?php foreach ($users as $user): ?>
                                        <tr>
                                            <td><?php echo anchor("account/edit/$user->userID", $user->userName); ?></td>
                                            <td><?php echo $user->userGroup; ?></td>
                                            <td><?php echo $user->userStatus == 'active'? '<span class="btn btn-success"></span>':'<span class="btn btn-danger"></span>'; ?></td>
                                            <td><?php echo btn_delete("account/delete/$user->userID") ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr><td class="text-center" colspan="5">ﻻ يوجد ستخدمين</td></tr>
                                <?php endif; ?>
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </div>
</section>
