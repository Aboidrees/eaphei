<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li class = "active">الدورات</li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor('staffTrainning/edit', '<i class="fa fa-plus"></i> إضافة دورة', 'class="btn btn-primary"'); ?>
        <hr style="margin: 2px 0">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">الدورات التدريبية</h3>
                <div class="box-tools">
                    <?php echo form_open(); ?>
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('staffTrainning', '', 'placeholder="ابحث عن دورة ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>حذف</th>
                            <th>الرمز</th>
                            <th>اسم الدورة</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($staffTrainning)) {
                            foreach ($staffTrainning as $staffTrainning) {
                                ?>
                                <tr>
                                    <td><?php echo btn_delete("staffTrainning/delete/$staffTrainning->tCode") ?></td>
                                    <td><?php echo $staffTrainning->tCode; ?></td>
                                    <td><?php echo anchor("staffTrainning/edit/$staffTrainning->tCode", $staffTrainning->tTitle); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="3">ﻻ يوجد دورات</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
