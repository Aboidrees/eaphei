<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li><?php echo anchor('staffTrainning', 'الدورات'); ?></li>
            <li class="active"><?php echo empty($staffTrainning->tCode) ? 'إضافة دورة' : "تعديل  $staffTrainning->cTitle"; ?></li>
        </ol>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="title"><?php echo empty($staffTrainning->tCode) ? 'إضافة دورة' : "تعديل  $staffTrainning->cTitle"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('كود الدورة', 'tCode'); ?>
                            <?php echo form_input('tCode', set_value('tCode', $staffTrainning->tCode), 'class="form-control" id="tCode"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('عنوان الدورة', 'tTitle'); ?>
                            <?php echo form_input('tTitle', set_value('tTitle', $staffTrainning->tTitle), 'class="form-control" id="tTitle"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('عدد الساعات', 'tContactHours'); ?>
                            <?php echo form_input('tContactHours', set_value('tContactHours', $staffTrainning->tContactHours), 'class="form-control" id="tContactHours"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('اهداف الدورة', 'tObjectives'); ?>
                            <?php echo form_textarea('tObjectives', set_value('tObjectives', $staffTrainning->tObjectives), 'class="form-control" id="tObjectives"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('متطلبات', 'tPrerequisites'); ?>
                            <?php echo form_textarea('tPrerequisites', set_value('tPrerequisites', $staffTrainning->tPrerequisites), 'class="form-control" id="tPrerequisites"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('المستهدفين', 'tAttendence'); ?>
                            <?php echo form_input('tAttendence', set_value('tAttendence', $staffTrainning->tAttendence), 'class="form-control" id="tAttendence"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('المحتويات', 'tOutline'); ?>
                            <?php echo form_textarea('tOutline', set_value('tOutline', $staffTrainning->tOutline), 'class="form-control" id="tOutline"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('طريقة التدريس', 'tLearninigMethod'); ?>
                            <?php echo form_input('tLearninigMethod', set_value('tLearninigMethod', $staffTrainning->tLearninigMethod), 'class="form-control" id="tLearninigMethod"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('البرمجيات والاجهزة والكتب المقترحة', 'tRecommResource'); ?>
                            <?php echo form_input('tRecommResource', set_value('tRecommResource', $staffTrainning->tRecommResource), 'class="form-control" id="tRecommResource"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('طريقة تقويم المتدرين', 'tAssesstment'); ?>
                            <?php echo form_input('tAssesstment', set_value('tAssesstment', $staffTrainning->tAssesstment), 'class="form-control" id="tAssesstment"'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <?php echo form_label('فاعلية مقرر الدورة', 'tValidation'); ?>
                            <?php echo form_input('tValidation', set_value('tValidation', $staffTrainning->tValidation), 'class="form-control" id="tValidation"'); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>