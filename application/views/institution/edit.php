<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li class="active"><?php echo empty($institution->instID) ? 'إضافة مؤسسة' : "تعديل  $institution->instName"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($institution->instID) ? 'إضافة مؤسسة' : "تعديل  $institution->instName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo form_hidden("parentID", 0); ?>
                        <?php echo form_hidden("type", "inst"); ?>
                        <div class="form-group">
                            <?php echo form_label('المؤسسة', 'instName'); ?>
                            <?php echo form_input('instName', set_value('instName', $institution->instName), 'class="form-control" id="instName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('تاريخ التأسيس', 'instFoundationDate'); ?>
                            <?php echo form_date('instFoundationDate', set_value('instFoundationDate', $institution->instFoundationDate), 'class="form-control" id="instFoundationDate"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'instEmail'); ?>
                            <?php echo form_input('instEmail', set_value('instEmail', $institution->instEmail), 'placeholder="inst@inst.ext" class="form-control" id="instEmail"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('الشعار', 'instLogo'); ?>
                            <?php echo form_file('instLogo', set_value('instLogo'), 'class="form-control" id="instLogo"'); ?>
                            <img class="img-bordered img-bordered-sm img-thumbnail" src="<?php echo base_url("/public/uploads/images/logos/$institution->instLogo"); ?>" alt="no Logo" />

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'instPhone'); ?>
                            <?php echo form_input('instPhone', set_value('instPhone', $institution->instPhone), 'placeholder="00249XXXXXXXXX" class="form-control" id="instPhone"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('فاكس', 'instFax'); ?>
                            <?php echo form_input('instFax', set_value('instFax', $institution->instFax), 'placeholder="00249XXXXXXXXX" class="form-control" id="instFax"'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('العنوان', 'instAddress'); ?>
                            <?php echo form_textarea('instAddress', set_value('instAddress', $institution->instAddress), 'placeholder="السودان ، مدني، حي  الاندلس - ص.ب. 20" class="form-control" id="instAddress"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

