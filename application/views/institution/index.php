<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li class="active">المؤسسات</li>
        </ol>          
        <div class="row">
            <div class="col-sm-2">
                <?php echo anchor("institution/edit", '<i class="fa fa-plus fa-larg"></i> اضافة مؤسسة', 'class="btn btn-primary" style="width:100%"'); ?>
            </div>
            <div class="col-sm-10">
                <?php echo form_open(); ?> 
                <?php echo form_input('instName', set_value('instName', $this->input->post('instName',TRUE)), 'placeholder="ابحث عن مؤسسة ...." class="form-control"'); ?> 
                <?php echo form_close(); ?> 
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php if (count($institutions)): ?>
            <div class="row">
                <?php foreach ($institutions as $institution): ?>
                    <div class="col-sm-6 col-lg-4">
                        <?php $this->load->view("institution/inst_module", $institution); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>



