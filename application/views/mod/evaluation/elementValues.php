<ol class="breadcrumb">
    <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('mod/evaluation', 'التقويمات'); ?></li>
    <li class = "active">تعبئة قيم التقويم</li>
</ol>
<section>
    <nav>
        <ul class="pager info">
            <li class="next"><?php echo anchor("mod/evaluation/evaluat/$evalID", 'التالي<span aria-hidden="true">&Gg;</span>', 'class="btn btn-success"'); ?></li>
            <li class="previous"><?php echo anchor("mod/evaluation", '<span aria-hidden="true">&Ll;</span> السابق', 'class="btn btn-success"'); ?></li>
        </ul>
    </nav>
    <?php echo form_open(); ?>
    <?php $i = 0; ?>
    <?php foreach ($evalelements as $evalelement): ?>
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><?php echo ++$i . '- ' . $evalelement->elementTitle1; ?></div>
            <?php echo form_hidden($evalelement->elementID . '_' . $evalelement->elementSymbol, 0); ?>

            <table class="table table-bordered panel panel-primary">
                <?php $j = 0; ?>
                <?php foreach ($sub_evalelements as $sub_evalelement): ?>
                    <?php if ($sub_evalelement->elementParent == $evalelement->elementID): ?>
                        <?php if (searchForSub($sub_sub_evalelements, $sub_evalelement->elementID) == NULL): ?>
                            <tr>
                                <td width="5%" class="text-center"><?php echo ++$j; ?></td>
                                <td><?php echo $sub_evalelement->elementTitle1; ?></td>
                                <td width='50%' dir="ltr">  
                                    <?php
                                    if ($sub_evalelement->editValue == 'yes') {
                                        if (preg_match_all('[((\[)(\w+)(\]))]', $sub_evalelement->elementEquation, $matchs)) {
                                            foreach ($matchs[3] as $match) {
                                                echo form_label($match);
                                                echo form_input($sub_evalelement->elementID . '_' . $match, setInputElementValue($sub_evalelement->degValue, $match), ' class="form-control text-center"');
                                            }
                                        } else {
                                            echo form_label($sub_evalelement->elementSymbol);
                                            echo form_input($sub_evalelement->elementID . '_' . $sub_evalelement->elementSymbol, setInputElementValue($sub_evalelement->degValue, $sub_evalelement->elementSymbol), ' class="form-control text-center"');
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td colspan="4">
                                    <table class="table table-bordered panel panel-primary">
                                        <caption class="text-right bg-info"><?php echo $i . ' - ' . ++$j . '- ' . $sub_evalelement->elementTitle1; ?></caption>
                                        <?php echo form_hidden($sub_evalelement->elementID . '_' . $sub_evalelement->elementSymbol, 0); ?>
                                        <?php foreach ($sub_sub_evalelements as $sub_sub_evalelement): ?>
                                            <?php if ($sub_sub_evalelement->elementParent == $sub_evalelement->elementID): ?>
                                                <tr>
                                                    <td> <?php echo $sub_sub_evalelement->elementTitle1; ?></td>
                                                    <td width='50%' dir="ltr">   
                                                        <?php
                                                        if ($sub_sub_evalelement->editValue == 'yes') {
                                                            if (preg_match_all('[((\[)(\w+)(\]))]', $sub_sub_evalelement->elementEquation, $matchs)) {
                                                                foreach ($matchs[3] as $match) {
                                                                    echo form_label($match);
                                                                    echo form_input($sub_sub_evalelement->elementID . '_' . $match, setInputElementValue($sub_sub_evalelement->degValue, $match), ' class="form-control text-center"');
                                                                }
                                                            } else {
                                                                echo form_label($sub_sub_evalelement->elementSymbol);
                                                                echo form_input($sub_sub_evalelement->elementID . '_' . $sub_sub_evalelement->elementSymbol, setInputElementValue($sub_sub_evalelement->degValue, $sub_sub_evalelement->elementSymbol), ' class="form-control text-center"');
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </table>
        </div>
        <hr />
    <?php endforeach; ?>
    <div class="text-center"><?php echo form_submit('', 'حفظ قيم عناصر التقويم', 'class="btn btn-primary"') ?></div>
    <?php echo form_close(); ?>
</section>