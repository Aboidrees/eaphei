<ol class="breadcrumb">
    <li><?php echo anchor('', 'الرئيسية'); ?></li>
    <li><?php echo anchor('evaluation', 'التقويمات'); ?></li>
    <li class = "active">البدء</li>
</ol>
<section>
    <table class="table table-bordered">
        <tr>
            <th class="info text-primary" width="25%">اسم المؤسسة</th>
            <td><?php echo $general->instName; ?></td>
        </tr>
        <tr>
            <th class="info text-primary">عنوان المؤسسة</th>
            <td>
                <strong>البريد الالكتروني: </strong><?php echo $general->facMail; ?><br />
                <strong>تلفون: </strong><?php echo $general->facPhone; ?>
                <strong>فاكس: </strong><?php echo $general->facFax; ?><br />
            </td>
        </tr>
        <tr>
            <th class="info text-primary">تاريخ انشاء الكلية</th>
            <td><?php echo date('Y-m-d', $general->facFoundationDate); ?></td>
        </tr>
        <tr>
            <th class="info text-primary">عدد البرامج</th>
            <td>
                <?php foreach ($progType as $key => $progType) { ?>
                    <strong><?php echo $key; ?>: </strong><?php echo $progType; ?>&nbsp;&nbsp;&nbsp;
                    <?php
                }
                ?>
            </td>
        </tr>
        <tr>
            <th class="info text-primary">عدد الطلاب الكلي </th>
            <td>
                
            </td>
        </tr>
    </table>

    <table class="table table-bordered">
        <tr>
            <th class="info text-primary" width="25%">البرنامج المراد تقويمه</th>
            <td><?php echo $general->progName; ?></td>
        </tr>
        <tr>
            <th class="info text-primary">تاريخ بدء تنفيذ البرامج</th>
            <td><?php echo date('Y/m/d', $general->progStartDate); ?></td>
        </tr>
        <tr>
            <th class="info text-primary">تاريخ آخر مراجعة وتحديث</th>
            <td><?php echo date('Y/m/d', $general->facFoundationDate); ?></td>
        </tr>
        <tr>
            <th class="info text-primary">عدد الطلاب في كل صف</th>
            <td>

                <?php echo form_open('evaluation/addStudentsNumbers'); ?>
                <?php echo form_hidden('progID', $general->progID, TRUE); ?>
                <?php echo form_hidden('evalID', $general->evalID, TRUE); ?>
                <?php echo form_label('عدد الطلاب:'); ?>
                <?php echo form_input('numStudent', 0, 'size="1"'); ?>
                <?php echo $general->yearOrSem == 'yes' ? form_label('رقم الصف:') : form_label('رقم الفصل:'); ?>
                <?php echo $general->yearOrSem == 'yes' ? form_dropdown('semester', array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6), 'size="1"') : form_dropdown('semester', array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12), 'size="1"'); ?>
                <?php echo form_submit('', 'أضف', 'class="btn btn-primary"'); ?>
                <?php echo form_close(); ?>  

                <!--(array_sum(vectorMultiply($x, $y)))-->
                <table class="table table-bordered">
                    <tr>
                        <?php
                        $i = 0;
                        foreach ($stdnumbers as $stdnumber) {
                            ?>
                            <td>
                                <strong><?php echo $stdnumber->semester; ?></strong>: <?php echo $stdnumber->numStudent; ?>
                                <?php echo anchor("evaluation/removeStudentsNumbers/{$stdnumber->progID}/{$stdnumber->numStudent}/{$stdnumber->semester}/{$stdnumber->evalID}", ' ', 'class="glyphicon glyphicon-remove"'); ?>
                            </td>
                            <?php
                            $i += intval($stdnumber->numStudent);
                        }
                        ?>
                        <td>المجموع: <?php echo $i; ?></td>
                    </tr>
                </table>

            </td>
        </tr>

    </table>
    <nav>
        <ul class="pager">
            <li class="disabled"><a><< السابق</a></li>
            <li><?php echo anchor("variable/index/$general->evalID", "التالي >>"); ?></li>
        </ul>
    </nav>

</section>