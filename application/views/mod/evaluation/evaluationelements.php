<ol class="breadcrumb">
    <li><?php echo anchor('', 'الرئيسية'); ?></li>
    <li><?php echo anchor('evaluation', 'التقويمات'); ?></li>
    <li class = "active">عناصر التقويم</li>
</ol>
<section>
    <?php echo form_open(); ?>
    <table class="table table-bordered" style="width: 70%; margin: auto;">
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('عنوان عنصر التقويم'); ?></td>
            <td><?php echo form_input('elementTitle2', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('العنصر الاصل'); ?></td>
            <td><?php echo form_dropdown('elementParent', array(), '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('رمز عنصر التقويم'); ?></td>
            <td><?php echo form_input('elementSymbol', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('معادلة عنصر التقويم'); ?></td>
            <td><?php echo form_input('elementEquation', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('وزن عنصر التقويم'); ?></td>
            <td><?php echo form_input('elementWeight', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('درجة الكلية الانموذج'); ?></td>
            <td><?php echo form_input('elementTypicalDgree', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('الدرجة المتحصل عليها'); ?></td>
            <td><?php echo form_input('elementDgree', '', 'class="form-control"'); ?></td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"><?php echo form_label('نتيجة تقويم العنصر'); ?></td>
            <td>
                <?php echo form_input('elementResult', '', 'class="form-control"'); ?>
                <?php echo form_hidden('evalID', $evalID); ?>
            </td>
        </tr>
        <tr>
            <th class="info text-primary" width="25%"></td>
            <td><?php echo form_submit('', 'إضافة', 'class="btn btn-primary"'); ?></td>
        </tr>
    </table>
    <?php echo form_close(); ?>


    <nav>
        <ul class="pager">
            <li><?php echo anchor("evaluation/start/1", "<< السابق"); ?></li>
            <li><?php echo anchor("evaluation/evaluationelements/1", "التالي >>"); ?></li>
        </ul>
    </nav>

</section>