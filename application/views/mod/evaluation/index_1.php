<ol class="breadcrumb">
    <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
    <li class = "active">التقويمات</li>
</ol>
<section>

    <?php echo anchor('mod/evaluation/edit', '<i class="ti-plus"></i> إضافة تقويم', 'class="btn btn-primary"'); ?>
    <table class="table table-striped" >
        <thead>
            <tr>
                <th>اسم التقويم</th>
                <th>تاريخ التقويم</th>
                <th>تاريخ جمع البيانات</th>
                <th>تعديل</th>
                <th>حذف</th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($evaluations)): ?>
                <?php foreach ($faculties as $faculty): ?>
                    <tr>
                        <td colspan="5" class="info">
                            <?php echo $faculty->facName . ' / ' . $faculty->progName; ?>
                        </td>
                    </tr>
                    <?php $i = 0; ?>
                    <?php foreach ($evaluations as $evaluation): ?>
                        <?php if ($faculty->progID == $evaluation->progID): ?>
                            <tr>
                                <td><?php echo anchor("mod/evaluation/setVars/$evaluation->evalID", "تقويم رقم  " . ++$i); ?></td>
                                <td><?php echo date('Y-m-d', $evaluation->evalDate); ?></td>
                                <td><?php echo date('Y-m-d', $evaluation->dataGetheringDate); ?></td>
                                <td><?php echo btn_edit("mod/evaluation/edit/$evaluation->evalID") ?></td>
                                <td><?php echo btn_delete("mod/evaluation/delete/$evaluation->evalID") ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td class="text-center" colspan="4">ﻻ يوجد كليات</td>
                </tr>
            <?php endif; ?>
        </tbody>

    </table>
</section>