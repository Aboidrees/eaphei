<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>

        <link href="<?php echo base_url('public/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/bootstrap-rtl.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/bootstrap-theme.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/datepicker.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/style.css'); ?>" rel="stylesheet" type="text/css" />

        <script src="<?php echo base_url('public/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/bootstrap-dropdown.js'); ?>"></script>
        <script src="<?php echo base_url('public/js/bootstrap-datepicker.js'); ?>"></script>


    </head>
    <body>

        <div class="container-fluid" id="container" >
            <div class="row" id="head">
                <h2 class="col-lg-11"><?php echo $meta_title; ?></h2>
                <h2 class="col-lg-1"><?php echo anchor('user/logout', '<i class="glyphicon glyphicon-off"></i>'); ?></h2>
            </div>
            <div id="body"><?php $this->load->view($subview); ?></div>
            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
        </div>
    </body>
</html>