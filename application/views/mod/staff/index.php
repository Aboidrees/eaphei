<ol class="breadcrumb">
    <li class = "active">هيئة التدريس</li>
</ol>
<div class="card">
    <div class="header">
        <h4 class="title">هيئة التدريس</h4>
        <p class="category">هيئة التدريس في مؤسسات التعليم العالي</p>
        <hr>
    </div>
    <div class="content table-responsive">
        <?php echo form_open('', 'class="row"'); ?> 
        <?php echo form_dropdown('instID', $institutions, set_value('instID'), 'class="form-control col-sm-4" onchange="form.submit()"'); ?>
        <?php echo form_close(); ?> 
        <br />


        <table class="table table-striped" >
            <thead>
                <tr>
                    <th>الاسم</th>
                    <th>المؤسسة</th>
                    <th>الكلية</th>
                    <th>تاريخ التعيين</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($staff)) {
                    foreach ($staff as $staff) {
                        ?>
                        <tr>
                            <td><?php echo $staff->staffName; ?></td>
                            <td><?php echo $staff->instName; ?></td>
                            <td><?php echo $staff->facName; ?></td>
                            <td><?php echo date("d-m-Y", $staff->staffHireDate); ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="4">ﻻ يوجد هيئة تدريس</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>