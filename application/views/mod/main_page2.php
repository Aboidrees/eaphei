<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <base href="<?php echo base_url(); ?>" >

        <link rel="apple-touch-icon" sizes="76x76" href="public/assets/img/menu_logo.png">
        <link rel="icon" type="image/png" sizes="96x96" href="public/assets/img/menu_logo.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Paper Dashboard by Creative Tim</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- datepicker CSS     -->
        <link href="public/assets/css/datepicker.css" rel="stylesheet" />

        <!-- Bootstrap core CSS     -->
        <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" />
        <!--<link href="public/assets/css/bootstrap-theme.min.css" rel="stylesheet" />-->
        <link href="public/assets/css/bootstrap-rtl.min.css" rel="stylesheet" />


        <!-- Animation library for notifications   -->
        <link href="public/assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Paper Dashboard core CSS    -->
        <link href="public/assets/css/paper-dashboard.css" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="public/assets/css/demo.css" rel="stylesheet" />


        <!--  Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="public/assets/css/themify-icons.css" rel="stylesheet">
    </head>
    <body>

        <div class="wrapper" style="width: 100%">
            <div class="main-panel" style="width: 100%">
                <nav class="navbar navbar-default">
                    <div class="container-fluid"  style="width: 80%">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar1"></span>
                                <span class="icon-bar bar2"></span>
                                <span class="icon-bar bar3"></span>
                            </button>
                            <a class="navbar-brand" href="#">Dashboard</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-panel"></i>
                                        <p>الحالة</p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">state1 </a></li>
                                        <li><a href="#">state2 </a></li>
                                        <li><a href="#">state3 </a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-bell"></i>
                                        <p>الاشعارات</p>
                                        <p class="notification">5</p>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">اشعار 1</a></li>
                                        <li><a href="#">اشعار 2</a></li>
                                        <li><a href="#">اشعار 3</a></li>
                                        <li><a href="#">اشعار 4</a></li>
                                        <li><a href="#">اشعار اخر</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-settings"></i>
                                        <p>الاعدادات</p>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid"  style="width: 80%">
                        <div class="row">
                            <div class="col-md-12">
                                <?php $this->load->view($subview); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    Page rendered in <strong>{elapsed_time}</strong>
                                    seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear());</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
                        </div>
                    </div>
                </footer>

            </div>
        </div>


    </body>

    <!--   Core JS Files   -->
    <script src="public/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="public/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="public/assets/js/bootstrap-datepicker.js"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <!--<script src="public/assets/js/bootstrap-checkbox-radio.js"></script>-->

    <!--  Charts Plugin -->
    <script src="public/assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="public/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="public/assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="public/assets/js/demo.js"></script>

<!--    <script type="text/javascript">
                                $(document).ready(function () {

                                    demo.initChartist();

                                    $.notify({
                                        icon: 'ti-gift',
                                        message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."

                                    }, {
                                        type: 'success',
                                        timer: 4000
                                    });

                                });
    </script>-->

</html>
