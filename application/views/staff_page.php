<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li><?php echo anchor('staff', 'هيئة تدريس'); ?></li>
            <li class="active"><?php echo empty($staff->staffID) ? 'إضافة عضو هيئة تدريس' : "تعديل  $staff->staffName"; ?></li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($staff->staffID) ? 'إضافة عضو هيئة تدريس' : "تعديل  $staff->staffName"; ?></h3>
            <div class="btn-group pull-left">
                <?php echo anchor("staff/basicInfo/$staff->staffID", 'Basic Informations', 'class="btn btn-primary"'); ?>
                <?php echo anchor("staff/qualifications/$staff->staffID", 'Qualifications', 'class="btn btn-primary" '); ?>
                <?php echo anchor("staff/job/$staff->staffID", 'Job Information', 'class="btn btn-primary"'); ?>
                <?php echo anchor("staff/books/$staff->staffID", 'Books', 'class="btn btn-primary"'); ?>
                <?php echo anchor("staff/publications/$staff->staffID", 'Publications', 'class="btn btn-primary"'); ?>
                <?php echo anchor("staff/staffTrainning/$staff->staffID", 'staffTrainning Courses', 'class="btn btn-primary"'); ?>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>

            <div class="row">
                <div class="col-md-6 border-left">
                    <div class="form-group">
                        <?php echo form_label('Name', 'staffName'); ?>
                        <?php echo form_input('staffName', set_value('staffName', $staff->staffName), 'placeholder="Staff Name ..." class="form-control" id="staffName"'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('Birth Date', 'staffBirthDate'); ?>
                        <?php echo form_date('staffBirthDate', date('Y-m-d', $staff->staffBirthDate), 'class="form-control" min="10" max="100" id="staffBirthDate"'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('E-Mail', 'staffEmail'); ?>
                        <?php echo form_input('staffEmail', set_value('staffEmail', $staff->staffEmail), 'class="form-control" min="10" max="100" id="staffEmail"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php echo form_label('Permanent Address', 'staffPermanentAddress'); ?>
                        <?php echo form_input('staffPermanentAddress', set_value('staffPermanentAddress', $staff->staffPermanentAddress), 'class="form-control" min="10" max="100" id="staffPermanentAddress"'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Telephone', 'staffPhone'); ?>
                        <?php echo form_input('staffPhone', set_value('staffPhone', $staff->staffPhone), 'class="form-control" min="10" max="100" id="staffPhone"'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('Mobile', 'staffMobile'); ?>
                        <?php echo form_input('staffMobile', set_value('staffMobile', $staff->staffMobile), 'class="form-control" min="10" max="100" id="staffMobile"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <hr>
                <div class="col-md-12">
                    <button type="submit" value="personal" name="save" class="btn btn-primary">حفظ</button>
                </div>
            </div>
            <!-- /.box-body -->
            <?php echo form_close(); ?>
        </div>
    </div>
</section>
<!-- Main content -->
