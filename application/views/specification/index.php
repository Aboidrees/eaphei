<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li class="active">Specification</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("specification/edit", '<i class="fa fa-plus"></i> Add Specification', 'class="btn btn-success"'); ?>
        <hr style="margin: 2px 0">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Specifications </h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('instName', '', 'placeholder="Search Programs ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
                <hr>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Specification name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($specifications)) {
                            $i = 1;
                            foreach ($specifications as $specification) {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $specification->sName; ?></td>
                                    <td><?php echo btn_edit("specification/edit/$specification->sID/$specification->sID"); ?></td>
                                    <td><?php echo btn_delete("specification/delete/$specification->sID/$specification->sID"); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">No Specification</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
