<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$department->instID}", $department->instName); ?></li>
            <li><?php echo anchor("department/index/{$department->facID}", $department->facName); ?></li>
            <li><?php echo anchor("program/index/{$department->depID}", $department->depName); ?></li>
            <li class="active"><?php echo empty($program->progID) ? 'إضافة برنامج' : "تعديل  $program->progName"; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo empty($program->progID) ? 'إضافة برنامج' : "تعديل  $program->progName"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo form_hidden("depID", $depID); ?>
                        <div class="form-group">
                            <?php echo form_label('اسم البرنامج', 'progName'); ?>
                            <?php echo form_input('progName', set_value('progName', $program->progName), 'placeholder="بكلاريوس العلوم في ....." class="form-control" id="progName"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('نوع البرنامج', 'progType'); ?>
                            <?php echo form_dropdown('progType', $progType, set_value('progType', $program->progType), 'class="form-control" id="progType"'); ?>

                        </div>
                        <div class="form-group">
                            <?php echo form_label('تاريخ بداء تنفيذ البرنامج', 'progStartDate'); ?>
                            <?php echo form_date('progStartDate', set_value('progStartDate', date('Y-m-d', $program->progStartDate)), 'class="form-control right" id="progStartDate"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('تاريخ آخر مراجعة وتحديث', 'progLastReviewDate'); ?>
                            <?php echo form_date('progLastReviewDate', set_value('progLastReviewDate', date('Y-m-d', $program->progLastReviewDate)), 'class="form-control right" id="progLastReviewDate"'); ?>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo form_label('نبذة عن البرنامج', 'progPref'); ?>
                            <?php echo form_textarea('progPref', set_value('progPref', $program->progPref), 'class="form-control" id="editor"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

