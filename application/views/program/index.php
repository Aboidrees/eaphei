<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$department->instID}", $department->instName); ?></li>
            <li><?php echo anchor("department/index/{$department->facID}", $department->facName); ?></li>
            <li class="active">برامج <?php echo $department->depName; ?></li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("program/edit/{$department->depID}", '<i class="fa fa-plus"></i> إضافة برنامج', 'class="btn btn-primary"'); ?>
        <br>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">برامج <?php echo $department->depName; ?></h3>
                <!--<div class="box-tools"></div>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="col-xs-1"></th>
                            <th>اسم البرنامج</th>
                            <th>تاريخ البدء</th>
                            <th>تاريخ اخر مراجعة</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($programs)) {
                            foreach ($programs as $program) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo btn_edit("program/edit/$department->depID/$program->progID"); ?> &nbsp; <?php echo btn_delete("program/delete/$department->depID/$program->progID"); ?></td>
                                    <td><?php echo anchor("evaluation/index/{$program->progID}", $program->progName); ?></td>
                                    <td><?php echo date('Y-m-d', $program->progStartDate); ?></td>
                                    <td><?php echo date('Y-m-d', $program->progLastReviewDate); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">ﻻ يوجد برامج</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
