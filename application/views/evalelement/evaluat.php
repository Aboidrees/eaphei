<ol class="breadcrumb">
    <li><?php echo anchor('', 'الرئيسية'); ?></li>
    <li><?php echo anchor('evaluation', 'التقويمات'); ?></li>
    <li><?php echo anchor("evalelement/index/$evalID", 'عناصر التقويم'); ?></li>
    <li class = "active">تقويم</li>
</ol>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title">نتيجة التقويم</h1>
    </div>
    <div class="panel-body">

        <!-- main elements -->

        <?php
        $i = 0;
        foreach ($new_elements as $new_element) {
            ?>
            <table class="table table-bordered panel panel-primary" style="font-family: tahoma">
                <caption class="text-right bg-info"><?php echo ++$i . '- ' . $new_element->elementTitle2; ?></caption>

                <!-- sub elements -->
                <?php
                $j = 0;
                foreach ($new_subelements as $new_subelement) {
                    
                    if ($new_subelement->elementParent == $new_element->elementID) {
                        if (searchForSub($new_subsubelements, $new_subelement->elementID) == NULL) {
                            ?>
                            <tr>
                                <td width="5%" class="text-center"><?php echo ++$j; ?></td>
                                <td> <?php echo $new_subelement->elementTitle2; ?></td>
                                <td><?php echo round($new_subelement->elementResult, 3); ?></td>
                            </tr>

                            <?php
                        } else {
                            ?>
                            <tr>
                                <td colspan="3">
                                    <table class="table table-bordered panel panel-primary" style="font-family: tahoma">
                                        <caption class="text-right bg-info"><?php echo $i . ' - ' . ++$j . '- ' . $new_subelement->elementTitle2; ?></caption>
                                        <?php
                                        $k = 0;
                                        foreach ($new_subsubelements as $new_subsubelement) {
                                            if ($new_subsubelement->elementParent == $new_subelement->elementID) {
                                                ?>
                                                <tr>
                                                    <td width="5%" class="text-center"><?php echo ++$k; ?></td>
                                                    <td><?php echo $new_subsubelement->elementTitle2; ?></td>
                                                    <td><?php echo round($new_subsubelement->elementResult, 3); ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>


                                        <tr style="background-color: #CCC">
                                            <td colspan="2">مجموع درجات <?php echo $new_subelement->elementTitle2; ?></td>
                                            <td width="10%"><?php echo $new_subelement->elementSymbol . '=' . round($new_subelement->elementResult, 3); ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                <!-- sub elements end -->



                <tr style="background-color: #CCC">
                    <td colspan="2">مجموع درجات <?php echo $new_element->elementTitle2; ?></td>
                    <td><?php echo round($new_element->elementResult, 3); ?></td>
                </tr>
            </table>
            <hr />
            <?php
        }
        ?>
    </div>
</div>