<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('evalform', 'استمارات التقويم '); ?></li>
            <li class = "active">عناصر التقويم - <?php echo $evalform->efName; ?> </li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1">
                <?php echo anchor("evalelement/edit/{$evalform->efID}", '<i class="ti-plus"></i> إضافة عنصر', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-11">
                <?php echo form_dropdown('efName', $evalforms, set_value("efName", $evalform->efID), 'class="form-control"'); ?>
            </div>
        </div>
        <br>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">عناصر التقويم</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('instName', '', 'placeholder="ابحث عن برنامج ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">

                <table class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>العنصر</th>
                            <th>رمز العنصر</th>
                            <th>تعديل</th>
                            <th>حذف</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($evalelements)) {
                            foreach ($evalelements as $evalelement) {
                                ?>
                                <tr>
                                    <td><?php echo anchor("evalelement/edit/{$evalform->efID}/$evalelement->elementID", $evalelement->elementTitle2); ?></td>
                                    <td><?php echo $evalelement->elementSymbol; ?></td>
                                    <td><?php echo btn_edit("evalelement/edit/{$evalform->efID}/$evalelement->elementID") ?></td>
                                    <td><?php echo btn_delete("evalelement/delete/{$evalform->efID}/$evalelement->elementID") ?></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table class="table table-striped table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th>العنصر</th>
                                                    <th>رمز العنصر</th>
                                                    <th>تعديل</th>
                                                    <th>حذف</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sub_evalelements = $this->evalelement_m->get_by("elementParent = {$evalelement->elementID}");
                                                if (count($sub_evalelements)) {
                                                    foreach ($sub_evalelements as $sub_evalelement) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo anchor("evalelement/edit/{$evalform->efID}/$sub_evalelement->elementID", $sub_evalelement->elementTitle2); ?></td>
                                                            <td><?php echo $sub_evalelement->elementSymbol; ?></td>
                                                            <td><?php echo btn_edit("evalelement/edit/{$evalform->efID}/$sub_evalelement->elementID") ?></td>
                                                            <td><?php echo btn_delete("evalelement/delete/{$evalform->efID}/$sub_evalelement->elementID") ?></td>
                                                        </tr>
                                                        <?php
                                                        $sub_sub_evalelements = $this->evalelement_m->get_by("elementParent = {$sub_evalelement->elementID}");
                                                        if (count($sub_sub_evalelements)) {
                                                            ?>
                                                            <tr>
                                                                <td colspan="4">

                                                                    <table class="table table-striped table-bordered" >
                                                                        <thead>
                                                                            <tr>
                                                                                <th>العنصر</th>
                                                                                <th>رمز العنصر</th>
                                                                                <th>تعديل</th>
                                                                                <th>حذف</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                            <?php
                                                                            foreach ($sub_sub_evalelements as $sub_sub_evalelement) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo anchor("evalelement/edit/{$evalform->efID}/$sub_sub_evalelement->elementID", $sub_sub_evalelement->elementTitle2); ?></td>
                                                                                    <td><?php echo $sub_sub_evalelement->elementSymbol; ?></td>
                                                                                    <td><?php echo btn_edit("evalelement/edit/{$evalform->efID}/$sub_sub_evalelement->elementID") ?></td>
                                                                                    <td><?php echo btn_delete("evalelement/delete/{$evalform->efID}/$sub_sub_evalelement->elementID") ?></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">ﻻ يوجد عناصر</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>