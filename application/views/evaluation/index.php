<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$program->instID}", $program->instName); ?></li>
            <li><?php echo anchor("department/index/{$program->facID}", $program->facName); ?></li>
            <li><?php echo anchor("program/index/{$program->depID}", $program->depName); ?></li>
            <li class="active"> <?php echo $program->progName; ?></li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("evaluation/edit/{$program->progID}", '<i class="fa fa-plus"></i> إضافة تقويم', 'class="btn btn-primary"'); ?>
        <br>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo $program->progName; ?></h3>
                <!--<div class="box-tools"></div>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="col-xs-1"></th>
                            <th>اسم التقويم</th>
                            <th>تاريخ التقويم</th>
                            <th>تاريخ جمع البيانات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($evaluations)): ?>

                            <?php $i = 0; ?>
                            <?php foreach ($evaluations as $evaluation): ?>
                                <tr>
                                    <td class="text-center"><?php echo btn_edit("evaluation/edit/$program->progID/$evaluation->evalID") ?> &nbsp; <?php echo btn_delete("evaluation/delete/$program->progID/$evaluation->evalID") ?></td>
                                    <td><?php echo anchor("evaluation/elementValues/$evaluation->evalID/$evaluation->efID", date('Y/m', $evaluation->evalDate)); ?></td>
                                    <td><?php echo date('Y-m-d', $evaluation->evalDate); ?></td>
                                    <td><?php echo date('Y-m-d', $evaluation->dataGetheringDate); ?></td>
                                </tr>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="5">ﻻ يوجد كليات</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
