<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
            <li><?php echo anchor('mod/evaluation', 'التقويم'); ?></li>
            <li class="active"><?php echo empty($evaluation->evalID) ? 'إضافة تقويم' : "تعديل التقويم"; ?></li>
        </ol>          
    </div>
</section>


<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="panel-title"><?php echo empty($evaluation->evalID) ? 'إضافة تقويم' : "تعديل التقويم"; ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('', 'role="form"'); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?php echo form_label('تاريخ إجراء التقويم', 'evalDate'); ?>
                            <?php echo form_input('evalDate', set_value('evalDate', date('Y-m-d', $evaluation->evalDate)), 'class="form-control right datepicker"'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('استمارة التقويم', 'efID'); ?>
                            <?php echo form_dropdown('efID',$evalforms, set_value('efID', $evaluation->efID), 'class="form-control"'); ?>
                        </div>                       
                        <div class="form-group">
                            <?php echo form_label('تاريخ جمع البيانات', 'dataGetheringDate'); ?>
                            <?php echo form_input('dataGetheringDate', set_value('dataGetheringDate', date('Y-m-d', $evaluation->dataGetheringDate)), 'class="form-control right datepicker"'); ?>
                        </div>                       
                        <div class="form-group">
                            <?php echo form_label('البرنامج', 'progID'); ?>
                            <?php echo form_dropdown('progID', $program, set_value('progID', $evaluation->progID), 'class="form-control"'); ?>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>