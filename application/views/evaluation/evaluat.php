<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$evaluation->instID}", $evaluation->instName); ?></li>
            <li><?php echo anchor("department/index/{$evaluation->facID}", $evaluation->facName); ?></li>
            <li><?php echo anchor("program/index/{$evaluation->depID}", $evaluation->depName); ?></li>
            <li><?php echo anchor("evaluation/index/{$evaluation->evalID}", $evaluation->progName); ?></li>
            <li class="active"> <?php echo date('Y/m', $evaluation->evalDate); ?></li>
        </ol>          
    </div>
</section>
<section class="content">
    <div class="container-fluid">

        <nav>
            <ul class="pager">
                <!--<li class="next"><?php echo anchor("evaluation/pointer/$evalID", 'التالي<span aria-hidden="true">&Gg;</span>'); ?></li>-->
                <li class="previous"><?php echo anchor("evaluation/elementValues/$evalID/{$evaluation->efID}", '<span aria-hidden="true">&Ll;</span> السابق'); ?></li>
            </ul>
        </nav>
        <div class="panel panel-danger">

            <div class="panel-heading">
                <h1 class="panel-title">نتيجة التقويم</h1>
            </div>
            <div class="panel-body">

                <!-- main elements -->

                <?php
                $i = 0;
                foreach ($new_elements as $new_element) {
                    ?>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h1 class="panel-title"><?php echo ++$i . '- ' . $new_element->elementTitle2; ?></h1>
                        </div>
                        <table class="table table-bordered table-striped" style="font-family: tahoma">
                            <!-- sub elements -->
                            <?php
                            $j = 0;
                            foreach ($new_subelements as $new_subelement) {

                                if ($new_subelement->elementParent == $new_element->elementID) {
                                    if (searchForSub($new_subsubelements, $new_subelement->elementID) == NULL) {
                                        ?>
                                        <tr>
                                            <td width="5%" class="text-center"><?php echo ++$j; ?></td>
                                            <td> <?php echo $new_subelement->elementTitle2; ?></td>
                                            <td width="10%"><?php echo round($new_subelement->degValue, 3); ?></td>
                                        </tr>

                                        <?php
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="3">
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <h1 class="panel-title"><?php echo $i . ' - ' . ++$j . '- ' . $new_subelement->elementTitle2; ?></h1>
                                                    </div>

                                                    <table class="table table-bordered table-striped" style="font-family: tahoma">
                                                        <?php
                                                        $k = 0;
                                                        foreach ($new_subsubelements as $new_subsubelement) {
                                                            if ($new_subsubelement->elementParent == $new_subelement->elementID) {
                                                                ?>
                                                                <tr>
                                                                    <td width="5%" class="text-center"><?php echo ++$k; ?></td>
                                                                    <td><?php echo $new_subsubelement->elementTitle2; ?></td>
                                                                    <td width="1%"><?php echo round($new_subsubelement->degValue, 3); ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>


                                                        <tr style="background-color: #CCC">
                                                            <td colspan="2">مجموع درجات <?php echo $new_subelement->elementTitle2; ?></td>
                                                            <td width="10%"><?php echo round($new_subelement->degValue, 3); ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <!-- sub elements end -->



                            <tr style="background-color: #CCC">
                                <td colspan="2">مجموع درجات <?php echo $new_element->elementTitle2; ?></td>
                                <td width="10%"><?php echo round($new_element->degValue, 3); ?></td>
                            </tr>
                        </table>
                    </div>
                    <hr />
                    <?php
                }
                ?>
                <?php $x = 0.0; ?>
                <?php $y = 'TOT = '; ?>
                <?php $z = count($new_elements); ?>
                <?php foreach ($new_elements as $new_element): ?>
                    <?php $x += $new_element->elementWeight * $new_element->degValue; ?>
                    <?php $y .= $new_element->elementWeight . ' * ' . $new_element->elementSymbol . ( --$z ? '&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;' : ''); ?>
                <?php endforeach; ?>
                <div class="text-center"><?php echo($y); ?></div>

                <table class="table table-bordered" align="center" width="40%">
                    <tr>
                        <td>مجموع درجات التقويم الكلي</td>
                        <td><?php echo round($x, 2); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>