<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li><?php echo anchor('institution', 'المؤسسات'); ?></li>
            <li><?php echo anchor("faculty/index/{$evaluation->instID}", $evaluation->instName); ?></li>
            <li><?php echo anchor("department/index/{$evaluation->facID}", $evaluation->facName); ?></li>
            <li><?php echo anchor("program/index/{$evaluation->depID}", $evaluation->depName); ?></li>
            <li><?php echo anchor("evaluation/index/{$evaluation->progID}", $evaluation->progName); ?></li>
            <li class="active"> <?php echo date('Y/m', $evaluation->evalDate); ?></li>
        </ol>          
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <nav>
            <ul class="pager info">
                <li class="next"><?php echo anchor("evaluation/evaluat/$evalID/$efID", 'التالي<span aria-hidden="true">&Gg;</span>'); ?></li>
                <li class="previous"><?php echo anchor("evaluation/index/{$evaluation->progID}/{$evaluation->efID}", '<span aria-hidden="true">&Ll;</span> السابق'); ?></li>
            </ul>
        </nav>
        <?php echo form_open(); ?>
        <?php $i = 0; ?>
        <?php foreach ($evalelements as $evalelement): ?>
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading"><?php echo ++$i . '- ' . $evalelement->elementTitle1; ?></div>
                <?php echo form_hidden($evalelement->elementID . '_' . $evalelement->elementSymbol, 0); ?>

                <table class="table table-striped table-bordered">
                    <?php $j = 0; ?>
                    <?php foreach ($sub_evalelements as $sub_evalelement): ?>
                        <?php if ($sub_evalelement->elementParent == $evalelement->elementID): ?>
                            <?php if (searchForSub($sub_sub_evalelements, $sub_evalelement->elementID) == NULL): ?>
                                <tr>
                                    <td width="5%" class="text-center"><?php echo ++$j . '-' .$i; ?></td>
                                    <td><?php echo $sub_evalelement->elementTitle1; ?></td>
                                    <td width='35%' dir="ltr">  
                                        <?php
                                        if ($sub_evalelement->editValue == 'yes') {
                                            if (preg_match_all('[((\[)(\w+)(\]))]', $sub_evalelement->elementEquation, $matchs)) {
                                                echo '<div class="row">';
                                                foreach ($matchs[3] as $match) {
                                                    echo '<div class="col-md-' . (12 / count($matchs[3])) . ' pull-center">';
                                                    echo '<div class = "form-group has-feedback">';
                                                    echo form_label($match);
                                                    echo form_input($sub_evalelement->elementID . '_' . $match, setInputElementValue($sub_evalelement->degValue, $match), ' class="form-control text-center"');
//                                                    echo '<span class = "form-control-feedback">' . $match . '</span>';
                                                    echo '</div>';
                                                    echo '</div>';
                                                }
                                                echo '</div>';
                                            } else {
                                                echo form_label($sub_evalelement->elementSymbol);
                                                echo form_input($sub_evalelement->elementID . '_' . $sub_evalelement->elementSymbol, setInputElementValue($sub_evalelement->degValue, $sub_evalelement->elementSymbol), ' class="form-control text-center"');
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td colspan="4">
                                        <div class="panel panel-success">
                                            <!-- Default panel contents -->
                                            <div class="panel-heading"><?php echo $i . ' - ' . ++$j . '- ' . $sub_evalelement->elementTitle1; ?></div>
                                            <table class="table table-striped table-bordered">
                                                <?php echo form_hidden($sub_evalelement->elementID . '_' . $sub_evalelement->elementSymbol, 0); ?>
                                                <?php $k = 1; ?>
                                                <?php foreach ($sub_sub_evalelements as $sub_sub_evalelement): ?>
                                                    <?php if ($sub_sub_evalelement->elementParent == $sub_evalelement->elementID): ?>
                                                        <tr>
                                                            <td width="5%"><?php echo $k++ . '-' . $j . '-' . $i ;?></td>
                                                            <td><?php echo $sub_sub_evalelement->elementTitle1; ?></td>
                                                            <td width='35%' dir="ltr">

                                                                <?php
                                                                if ($sub_sub_evalelement->editValue == 'yes') {
                                                                    if (preg_match_all('[((\[)(\w+)(\]))]', $sub_sub_evalelement->elementEquation, $matchs)) {
                                                                        echo '<div class="row">';
                                                                        foreach ($matchs[3] as $match) {
                                                                            echo '<div class="col-md-' . (12 / count($matchs[3])) . ' pull-center">';
                                                                            echo '<div class = "form-group has-feedback">';
                                                                            echo form_label($match);
                                                                            echo form_input($sub_sub_evalelement->elementID . '_' . $match, setInputElementValue($sub_sub_evalelement->degValue, $match), ' class="form-control text-center"');
//                                                                            echo '<span class = "form-control-feedback">' . $match . '</span>';
                                                                            echo '</div>';
                                                                            echo '</div>';
                                                                        }
                                                                        echo '</div>';
                                                                    } else {
                                                                        echo form_label($sub_sub_evalelement->elementSymbol);
                                                                        echo form_input($sub_sub_evalelement->elementID . '_' . $sub_sub_evalelement->elementSymbol, setInputElementValue($sub_sub_evalelement->degValue, $sub_sub_evalelement->elementSymbol), ' class="form-control text-center"');
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>

                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </table>
            </div>
            <hr />
        <?php endforeach; ?>
        <div class="text-center"><?php echo form_submit('', 'حفظ قيم عناصر التقويم', 'class="btn btn-primary"') ?></div>
        <?php echo form_close(); ?>
    </div>
</section>