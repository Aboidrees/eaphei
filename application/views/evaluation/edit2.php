<ol class="breadcrumb">
    <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('mod/evaluation', 'التقويم'); ?></li>
    <li class="active"><?php echo empty($evaluation->evalID) ? 'إضافة تقويم' : "تعديل التقويم"; ?></li>

</ol>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo empty($evaluation->evalID) ? 'إضافة تقويم' : "تعديل التقويم"; ?></h3>
    </div>
    <div class="panel-body">
        <?php echo validation_errors('<div class="warning">', '</div>'); ?>
        <?php echo $this->session->flashdata('error'); ?>
        <?php echo form_open(); ?>
        <table class="table">
            <tr>
                <td>تاريخ إجراء التقويم</td>
                <td><?php echo form_input('evalDate', set_value('evalDate', date('Y-m-d', $evaluation->evalDate)), 'class="form-control right datepicker"'); ?></td>
            </tr>
            <tr>
                <td>تاريخ جمع البيانات</td>
                <td><?php echo form_input('dataGetheringDate', set_value('dataGetheringDate', date('Y-m-d', $evaluation->dataGetheringDate)), 'class="form-control right datepicker"'); ?></td>
            </tr>
            <tr>
                <td>البرنامج</td>
                <td><?php echo form_dropdown('progID', $program, set_value('progID', $evaluation->progID), 'class="form-control"'); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo form_submit('', 'Save', 'class="btn btn-primary"'); ?></td>
            </tr>
        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    $(function () {
        $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
    });
</script>
