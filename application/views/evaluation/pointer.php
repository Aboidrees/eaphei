<ol class="breadcrumb">
    <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('mod/evaluation', 'التقويمات'); ?></li>
    <li class = "active">تعبئة قيم التقويم</li>
</ol>
<section>
    <nav>
        <ul class="pager info">
            <li class="next"><?php echo anchor("", 'التالي<span aria-hidden="true">&Gg;</span>', 'class="btn btn-success"'); ?></li>
            <li class="previous"><?php echo anchor("mod/evaluation/evaluat/$evalID", '<span aria-hidden="true">&Ll;</span> السابق', 'class="btn btn-success"'); ?></li>
        </ul>
    </nav>

    <div class="panel panel-primary">

        <div class="panel-heading">
            <h1 class="panel-title">مؤشرات الأداء</h1>
        </div>
        <div class="list-group">
            <?php foreach ($pointers as $pointer): ?>
                <div class="list-group-item row">
                    <div class=" col-lg-5"><?php echo 'نسبة ' . $student->varName . ' إلى ' . $pointer->elementTitle2, '' ?></div>
                    <div class=" col-lg-7"><?php echo round($student->varValue / $pointer->degValue, 2) .' طالب/أستاذ'; ?></div>
                </div>

            <?php endforeach; ?>
        </div>
    </div>
</section>