<ol class="breadcrumb">
    <li><?php echo anchor('mod/home', 'الرئيسية'); ?></li>
    <li><?php echo anchor('mod/evaluation', 'التقويمات'); ?></li>
    <li class = "active">إعداد المتغيرات</li>
</ol>
    <nav>
        <ul class="pager info">
            <li class="next"><?php echo anchor("mod/evaluation/elementValues/$evalID", 'التالي<span aria-hidden="true">&Gg;</span>', 'class="btn btn-success"'); ?></li>
            <li class="previous"><?php echo anchor('mod/evaluation', '<span aria-hidden="true">&Ll;</span> السابق', 'class="btn btn-success"'); ?></li>
        </ul>
    </nav>
   