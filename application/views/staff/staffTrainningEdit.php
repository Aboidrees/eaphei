<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Add staffTrainning</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('Code', 'cCode'); ?>
                    <?php echo form_input('cCode', set_value('cCode', $staffTrainning->cCode), 'class="form-control" id="cCode"'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo form_label('Contact Hours', 'cContactHours'); ?>
                    <?php echo form_input('cContactHours', set_value('cContactHours', $staffTrainning->cContactHours), 'class="form-control" id="cContactHours"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_label('Title', 'cTitle'); ?>
                    <?php echo form_input('cTitle', set_value('cTitle', $staffTrainning->cTitle), 'class="form-control" id="cTitle"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Related Modules', 'cReletedModules'); ?>
                    <?php echo form_textarea('cReletedModules', set_value('cReletedModules', $staffTrainning->cReletedModules), 'class="form-control" id="cReletedModules"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Objectives', 'cObjectives'); ?>
                    <?php echo form_textarea('cObjectives', set_value('cObjectives', $staffTrainning->cObjectives), 'class="form-control" id="cObjectives"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Prerequisites', 'cPrerequisites'); ?>
                    <?php echo form_textarea('cPrerequisites', set_value('cPrerequisites', $staffTrainning->cReletedModules), 'class="form-control" id="cPrerequisites"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Outcomes', 'cOutcomes'); ?>
                    <?php echo form_textarea('cOutcomes', set_value('cOutcomes', $staffTrainning->cOutcomes), 'class="form-control" id="cOutcomes"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Outlines', 'cOutline'); ?>
                    <?php echo form_textarea('cOutline', set_value('cOutline', $staffTrainning->cOutline), 'class="form-control" id="cOutline"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Learninig Method', 'cLearninigMethod'); ?>
                    <?php echo form_textarea('cLearninigMethod', set_value('cLearninigMethod', $staffTrainning->cLearninigMethod), 'class="form-control" id="cLearninigMethod"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Software', 'cRecommSoftware'); ?>
                    <?php echo form_textarea('cRecommSoftware', set_value('cRecommSoftware', $staffTrainning->cRecommSoftware), 'class="form-control" id="cRecommSoftware"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Hardware', 'cRecommHardware'); ?>
                    <?php echo form_textarea('cRecommHardware', set_value('cRecommHardware', $staffTrainning->cRecommHardware), 'class="form-control" id="cRecommHardware"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Recommended Books', 'cRecommBooks'); ?>
                    <?php echo form_textarea('cRecommBooks', set_value('cRecommBooks', $staffTrainning->cRecommBooks), 'class="form-control" id="cRecommBooks"'); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_label('Assesstment', 'cAssesstment'); ?>
                    <?php echo form_textarea('cAssesstment', set_value('cAssesstment', $staffTrainning->cAssesstment), 'class="form-control" id="cAssesstment"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>

                <button type="submit" value="personal" name="save" class="btn btn-success">Save staffTrainning</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>