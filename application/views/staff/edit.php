<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li><?php echo anchor('staff', 'هيئة التدريس'); ?></li>
            <li class="active">إضافة عضو</li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php if (isset($staff->staffID)): ?>
            <div class="btn-group">
                <?php $segment = $this->uri->segment(2); ?>
                <?php echo anchor("staff/basicInfo/{$staff->staffID}", 'البيانات الأساسية', 'class="btn btn-primary ' . ($segment == 'basicInfo' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/qualifications/{$staff->staffID}", 'المؤهل', 'class="btn btn-primary ' . ($segment == 'qualifications' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/job/{$staff->staffID}", 'بيانات الوظيفة', 'class="btn btn-primary ' . ($segment == 'job' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/scientificParticipation/{$staff->staffID}", 'المشاركات العلمية', 'class="btn btn-primary ' . ($segment == 'publications' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/staffTrainning/{$staff->staffID}", 'الدورات التدريبية', 'class="btn btn-primary ' . ($segment == 'staffTrainning' ? 'active' : '') . '"'); ?>
            </div>		
        <?php endif; ?>
        <br />
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">البيانات الاساسية</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
                <div class="row">
                    <div class="col-md-8 col-xs-12 form-group">
                        <?php echo form_label('الاسم', 'staffName'); ?>
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <?php echo form_input('staffName0', set_value('staffName0', @explode('-', $staff->staffName)[0]), 'placeholder="الاول" class="form-control" id="staffName"'); ?>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <?php echo form_input('staffName1', set_value('staffName1', @explode('-', $staff->staffName)[1]), 'placeholder="الثاني" class="form-control" id="staffName"'); ?>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <?php echo form_input('staffName2', set_value('staffName2', @explode('-', $staff->staffName)[2]), 'placeholder="الثالث" class="form-control" id="staffName"'); ?>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <?php echo form_input('staffName3', set_value('staffName3', @explode('-', $staff->staffName)[3]), 'placeholder="الرابع" class="form-control" id="staffName"'); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <div class="form-group">
                            <?php echo form_label('النوع', 'staffGender'); ?>
                            <?php echo form_dropdown('staffGender', array('ذكر' => 'ذكر', 'أنثى' => 'أنثى'), $staff->staffGender, 'class="form-control" id="staffGender"'); ?>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <div class="form-group">
                            <?php echo form_label('تاريخ الميلاد', 'staffBirthDate'); ?>
                            <?php echo form_date('staffBirthDate', date('Y-m-d', $staff->staffBirthDate), 'class="form-control" id="staffBirthDate"'); ?>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 border-left">
                        <div class="form-group">
                            <?php echo form_label('الجنسية', 'staffNationality'); ?>
                            <?php echo form_input('staffNationality', set_value('staffNationality', $staff->staffNationality), 'class="form-control" id="staffNationality"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 border-left">
                        <div class="form-group">
                            <?php echo form_label('رقم الهوية', 'staffNationalID'); ?>
                            <?php echo form_input('staffNationalID', set_value('staffNationalID', $staff->staffNationalID), 'class="form-control" id="staffNationalID"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <?php echo form_label('العنوان الدائم', 'staffPermanentAddress'); ?>
                            <?php echo form_input('staffPermanentAddress', set_value('staffPermanentAddress', $staff->staffPermanentAddress), 'class="form-control" min="10" max="100" id="staffPermanentAddress"'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-12 border-left">
                        <div class="form-group">
                            <?php echo form_label('البريد الالكتروني', 'staffEmail'); ?>
                            <?php echo form_input('staffEmail', set_value('staffEmail', $staff->staffEmail), 'class="form-control" min="10" max="100" id="staffEmail"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 border-left">
                        <div class="form-group">
                            <?php echo form_label('هاتف', 'staffPhone'); ?>
                            <?php echo form_input('staffPhone', set_value('staffPhone', $staff->staffPhone), 'class="form-control" min="10" max="100" id="staffPhone"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <?php echo form_label('جوال', 'staffMobile'); ?>
                            <?php echo form_input('staffMobile', set_value('staffMobile', $staff->staffMobile), 'class="form-control" min="10" max="100" id="staffMobile"'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" value="personal" name="save" class="btn btn-primary">Save Basic Info</button>
                        <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>