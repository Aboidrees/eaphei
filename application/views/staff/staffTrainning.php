<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">الدورات التدريبية</h3>
        <div class="box-tools">
            <?php echo anchor("staff/staffTrainningEdit/{$staff->staffID}", '<i class="fa fa-plus text-primary"></i>', 'class="btn btn-default btn-sm"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="5%">كود</th>
                    <th>الدورة</th>
                    <th width="5%">تعديل</th>
                    <th width="5%">حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($staffTrainning)) {
                    foreach ($staffTrainning as $staffTrainning) {
                        ?>
                        <tr>
                            <td><?php echo $staffTrainning->cCode; ?></td>
                            <td><?php echo anchor("staff/staffTrainningEdit/{$staff->staffID}/$staffTrainning->cCode", $staffTrainning->cTitle); ?></td>
                            <td><?php echo btn_edit("staff/staffTrainningEdit/{$staff->staffID}/$staffTrainning->cCode") ?></td>
                            <td><?php echo btn_delete("staff/staffTrainningDelete/{$staff->staffID}/$staffTrainning->cCode") ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5"> لا توجد دورات</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>