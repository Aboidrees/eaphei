<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Books</h3>
        <div class="box-tools">
            <?php echo anchor("staff/bookEdit/{$staff->staffID}", '<i class="fa fa-plus text-success"></i>', 'class="btn btn-default btn-sm"') ?>
        </div>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Publishing Date</th>
                    <th>Publisher</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($books)) {
                    foreach ($books as $book) {
                        ?>
                        <tr>
                            <td><?php echo anchor("staff/bookEdit/{$staff->staffID}/{$book->bookID}", $book->bookTitle); ?></td>
                            <td><?php echo date("d / m / Y", $book->bookPubDate); ?></td>
                            <td><?php echo $book->bookPublisher; ?></td>
                            <td><?php echo btn_delete("staff/bookDelete/{$staff->staffID}/{$book->bookID}"); ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">No Books</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>