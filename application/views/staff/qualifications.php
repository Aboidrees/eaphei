<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">المؤهل العلمي</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-4 col-xs-6 border-left">
                <div class="form-group">
                    <?php echo form_label('المؤهل العلمي', 'staffHigherQualification'); ?>
                    <?php echo form_dropdown('staffHigherQualification', $qualifications, set_value('staffHigherQualification', $staff->staffHigherQualification), 'class="form-control" id="staffHigherQualification"'); ?>
                </div>
            </div>

            <div class="col-md-4 col-xs-6">
                <div class="form-group">
                    <?php echo form_label('التاريخ', 'staffDegreeDate'); ?>
                    <?php echo form_date('staffDegreeDate', set_value('staffDegreeDate', $staff->staffDegreeDate), 'class="form-control" id="staffDegreeDate"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-6 border-left">
                <div class="form-group">
                    <?php echo form_label('التخصص العام', 'staffGenrSpec'); ?>
                    <?php echo form_input('staffGenrSpec', set_value('staffGenrSpec', $staff->staffGenrSpec), 'class="form-control" id="staffGenrSpec"'); ?>
                </div>
            </div>

            <div class="col-md-4 col-xs-6">
                <div class="form-group">
                    <?php echo form_label('التخصص الدقيق', 'staffSpec'); ?>
                    <?php echo form_input('staffSpec', set_value('staffSpec', $staff->staffSpec), 'class="form-control" id="staffSpec"'); ?>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <?php echo form_label('', 'staffResearchTitle'); ?>
                    <?php echo form_textarea('staffResearchTitle', set_value('staffResearchTitle', $staff->staffResearchTitle, TRUE), 'class="form-control" id="staffResearchTitle"', 40, 3); ?>
                </div>
            </div>
        </div>
        <hr >
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="qualification" name="save" class="btn btn-primary">حفظ</button>
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>