<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo isset($scientificParticipation->spID) ? 'تعديل' : 'إضافة' ?> مساهمة علمية</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-4 col-xs-12 border-left-md">
                <div class="form-group">
                    <?php echo form_label('العنوان', 'spTitle'); ?>
                    <?php echo form_input('spTitle', set_value('spTitle', $scientificParticipation->spTitle), 'class="form-control" id="spTitle"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('نوع المساهمة العلمية', 'spType'); ?>
                    <?php echo form_dropdown('spType', $spType, set_value('spType', $scientificParticipation->spType), 'class="form-control" id="spType"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('الناشر/المؤسسة المنظمة(إذا كان المشاركة هي حضور مؤتمر اة نحوه)', 'spPublisher'); ?>
                    <?php echo form_input('spPublisher', set_value('spPublisher', $scientificParticipation->spPublisher), 'class="form-control" id="spPublisher"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('التاريخ', 'spDate'); ?>
                    <?php echo form_date('spDate', set_value('spDate', $scientificParticipation->spDate), 'class="form-control" id="spDate"'); ?>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="form-group">
                    <?php echo form_label('التفاصيل', 'spDetails'); ?>
                    <?php echo form_textarea('spDetails', set_value('spDetails', $scientificParticipation->spDetails), 'class="form-control" id="spDetails"', 40, 8); ?>
                </div>
                <div class="form-group">
                    <?php echo form_label('رابط المشاركة العلمية / ارشيف المؤتمر', 'spLink'); ?>
                    <?php echo form_input('spLink', set_value('spLink', $scientificParticipation->spLink), 'class="form-control" id="spLink"'); ?>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="personal" name="save" class="btn btn-primary">حفظ</button>
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>