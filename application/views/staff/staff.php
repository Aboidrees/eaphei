<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li><?php echo anchor('staff', 'هيئة التدريس'); ?></li>
            <li class="active"><?php echo $staff->staffName; ?></li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php if (isset($staff->staffID)): ?>
            <div class="btn-group">
                <?php $segment = $this->uri->segment(2); ?>
                <?php echo anchor("staff/basicInfo/{$staff->staffID}", 'البيانات الأساسية', 'class="btn btn-primary ' . ($segment == 'basicInfo' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/qualifications/{$staff->staffID}", 'المؤهل', 'class="btn btn-primary ' . ($segment == 'qualifications' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/job/{$staff->staffID}", 'بيانات الوظيفة', 'class="btn btn-primary ' . ($segment == 'job' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/scientificParticipation/{$staff->staffID}", 'المساهمات العلمية', 'class="btn btn-primary ' . ($segment == 'scientificParticipation'||$segment == 'scientificParticipationEdit' ? 'active' : '') . '"'); ?>
                <?php echo anchor("staff/staffTrainning/{$staff->staffID}", 'الدورات التدريبية', 'class="btn btn-primary ' . ($segment == 'staffTrainning' ? 'active' : '') . '"'); ?>
            </div>		
        <?php endif; ?>
        <br />
        <?php $this->load->view($subsubview); ?>
    </div>
</section>

