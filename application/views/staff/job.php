<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">بيانات الوظيفة</h3>
        <hr style="margin: 5px 0;">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open('', 'role="form" class="container-fluid"'); ?>
        <div class="row">
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('المؤسسة', 'instID'); ?>
                    <?php echo form_dropdown('instID', $institutions, set_value('instID', $staff->instID), 'class="form-control" id="instID"  onchange="form.submit()"'); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('الكلية', 'facID'); ?>
                    <?php echo form_dropdown('facID', $faculties, set_value('facID', $staff->facID), 'class="form-control" id="facID"  onchange="form.submit()"'); ?>
                </div>
            </div>

            <div class="col-md-3 col-xs-4">
                <div class="form-group">
                    <?php echo form_label('القسم', 'depID'); ?>
                    <?php echo form_dropdown('depID', $departments, set_value('depID', $staff->depID), 'class="form-control" id="depID"'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('الرتبة العلمية', 'staffPosition'); ?>
                    <?php echo form_dropdown('staffPosition', $staffPosition, set_value('staffPosition', $staff->staffPosition), 'class="form-control" id="staffPosition"'); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('تاريخ التعيين', 'staffHireDate'); ?>
                    <?php echo form_date('staffHireDate', set_value('staffHireDate', $staff->staffHireDate), 'class="form-control" id="staffHireDate"'); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-4">
                <div class="form-group">
                    <?php echo form_label('عنوان العمل', 'staffCurrentAddress'); ?>
                    <?php echo form_input('staffCurrentAddress', set_value('staffCurrentAddress', $staff->staffCurrentAddress), 'class="form-control" min="10" max="100" id="staffCurrentAddress"'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('الحالة الوظيفية', 'staffStatus'); ?>
                    <?php echo form_dropdown('staffStatus', $staffStatus, set_value('staffStatus', $staff->staffStatus), 'class="form-control" id="staffStatus"'); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-4 border-left">
                <div class="form-group">
                    <?php echo form_label('تاريخ بداية', 'staffStatusStartDate'); ?>
                    <?php echo form_date('staffStatusStartDate', set_value('staffStatusStartDate', $staff->staffStatusStartDate), 'class="form-control" id="staffStatusStartDate"'); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-4">
                <div class="form-group">
                    <?php echo form_label('تاريخ نهاية', 'staffStatusEndDate'); ?>
                    <?php echo form_date('staffStatusEndDate', set_value('staffStatusEndDate', $staff->staffStatusEndDate), 'class="form-control" id="staffStatusEndDate"'); ?>
                </div>
            </div>
        </div>
        <hr >
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" value="job" name="save" class="btn btn-primary">حفظ</button>
                <?php echo validation_errors('<div class="alert alert-danger alert-with-icon" data-notify="container"><button type="button" aria-hidden="true" class="close">×</button> <span data-notify="icon" class="fa fa-bell"></span><span data-notify="message">', '</span></div>'); ?>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box-body -->
</div>