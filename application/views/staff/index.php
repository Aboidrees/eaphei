<section class="content-header ">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li class = "active">هيئة التدريس ومعاونيهم</li>
        </ol> 
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor("staff/edit", '<i class="fa fa-plus"></i> إضافة عضو هيئة تدريس', 'class="btn btn-primary"'); ?>
        <hr style="margin: 2px 0">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">هيئة التدريس ومعاونيهم</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('staffName', '', 'placeholder="Search Staff ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th class="col-xs-1 text-center" ></th>
                            <th>الاسم</th>
                            <th>تاريخ التعيين</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($staff)) {
                            foreach ($staff as $staff) {
                                ?>
                                <tr>
                                    <td><?php echo btn_delete("staff/delete/$staff->staffID") ?></td>
                                    <td><?php echo anchor("staff/basicInfo/$staff->staffID", $staff->staffName); ?></td>
                                    <td><?php echo date('Y-m-d', $staff->staffHireDate || now()); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">لا يوجد أعضاء</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>