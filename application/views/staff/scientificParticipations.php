<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">المساهمات العلمية</h3>
        <div class="box-tools">
            <?php echo anchor("staff/scientificParticipationEdit/{$staff->staffID}", '<i class="fa fa-plus"></i> إضافة مساهمة علمية', 'class="btn btn-primary"') ?>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>العنوان</th>
                    <th>نوع المشاركة</th>
                    <th>التاريخ</th>
                    <th>الناشر/المؤسسة</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($scientificParticipations)) {
                    foreach ($scientificParticipations as $scientificParticipation) {
                        ?>
                        <tr>
                            <td width="5%" class="border-left text-center"><?php echo btn_delete("staff/scientificParticipationDelete/{$staff->staffID}/$scientificParticipation->spID"); ?></td>
                            <td><?php echo anchor("staff/scientificParticipationEdit/{$staff->staffID}/{$scientificParticipation->spID}", $scientificParticipation->spTitle); ?></td>
                            <td><?php echo $scientificParticipation->spType; ?></td>
                            <td><?php echo $scientificParticipation->spDate; ?></td>
                            <td><?php echo $scientificParticipation->spPublisher; ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="text-center" colspan="5">لا توجد مساهمات علمية</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>