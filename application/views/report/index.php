<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية '); ?></li>
            <li class="active">التقارير</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">تقرير</h3>
                <div class="box-tools"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="container-fluid">
                    <?php echo form_open('', array('class' => 'row')); ?>

                    <div class="col-sm-3">
                        <?php echo form_dropdown('instID', $instList, set_value('instID'), 'class="form-control" onChange="form.submit()"'); ?>
                    </div>

                    <?php if (intval($instID) > 0): ?>
                        <div class="col-sm-3">
                            <?php echo form_dropdown('facID', $facList, set_value('facID'), 'class="form-control" onChange="form.submit()"'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (intval($facID) > 0): ?>
                        <div class="col-sm-3">
                            <?php echo form_dropdown('depID', $depList, set_value('depID'), 'class="form-control" onChange="form.submit()"'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (intval($depID) > 0): ?>
                        <div class="col-sm-3">
                            <?php echo form_dropdown('progID', $progList, set_value('progID'), 'class="form-control" onChange="form.submit()"'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (intval($progID) > 0): ?>
                        <div class="col-sm-3">
                            <?php echo form_dropdown('evalID', $evalList, set_value('evalID'), 'class="form-control" onChange="form.submit()"'); ?>
                        </div>
                    <?php endif; ?>


                    <?php echo form_close(); ?>

                </div>
                <div class="container-fluid">
                    <?php if (intval($this->input->post()) == 0): ?>
                        <div class="row">
                            <table class="table table-striped table-bordered">
                                <caption class="text-right">المؤسسات</caption>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>المؤسسة</th>
                                        <th>تااريخ الانشاء</th>
                                        <th>العنوان</th>
                                        <th>عدد الكليات</th>
                                    </tr>
                                </thead>
                                <?php $i = 0; ?>
                                <?php foreach ($institutions as $institution): ?>
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td><?php echo $institution->instName; ?></td>
                                        <td><?php echo $institution->instFoundationDate; ?></td>
                                        <th><?php echo $institution->instAddress; ?></th>
                                        <th><?php echo $institution->facNum; ?></th>
                                    </tr>
                                <?php endforeach;
                                ?>
                            </table>
                        </div>
                    <?php endif; ?>
                    <?php if (intval($this->input->post('evalID', TRUE)) > 0): ?>
                        <div class="row">
                            <table class="table table-striped">
                                <caption class="text-right"></caption>
                                <thead>
                                    <tr>
                                        <th>1</th>
                                        <th><?php echo($cra->elementTitle1); ?></th>
                                        <th><?php echo($cra->degValue); ?></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>2</th>
                                        <th><?php echo($prwa->elementTitle1); ?></th>
                                        <th><?php echo($prwa->degValue); ?></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>3</th>
                                        <th><?php echo($ha->elementTitle1); ?></th>
                                        <th><?php echo($ha->degValue); ?></th>
                                        <th><?php echo(round($ha->degValue / $cra->degValue * 100, 1)); ?>%</th>
                                    </tr>
                                    <tr>
                                        <th>4</th>
                                        <th><?php echo($sa->elementTitle1); ?></th>
                                        <th><?php echo($sa->degValue); ?></th>
                                        <th><?php echo(round($sa->degValue / $cra->degValue * 100, 1)); ?>%</th>
                                    </tr>
                                    <tr>
                                        <th>5</th>
                                        <th><?php echo($esa->elementTitle1); ?></th>
                                        <th><?php echo($esa->degValue); ?></th>
                                        <th><?php echo(round($esa->degValue / $cra->degValue * 100, 1)); ?>%</th>
                                    </tr>
                                    <tr>
                                        <th>6</th>
                                        <th><?php echo($espa->elementTitle1); ?></th>
                                        <th><?php echo($espa->degValue); ?></th>
                                        <th><?php echo(round($espa->degValue / $cra->degValue * 100, 1)); ?>%</th>
                                    </tr>
                                    <tr>
                                        <th>7</th>
                                        <th>عدد الطلاب</th>
                                        <th><?php echo($S->degValue); ?></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>8</th>
                                        <th><?php echo($L3->elementTitle1); ?></th>
                                        <th><?php echo($L3->degValue); ?></th>
                                        <th><?php echo(round($S->degValue / $L3->degValue)); ?> طالب لكل بروفيسور</th>
                                    </tr>
                                    <tr>
                                        <th>9</th>
                                        <th><?php echo($L4->elementTitle1); ?></th>
                                        <th><?php echo($L4->degValue); ?></th>
                                        <th><?php echo(round($S->degValue / $L4->degValue)); ?> طالب لكل  استاذ مشارك</th>
                                    </tr>
                                    <tr>
                                        <th>10</th>
                                        <th><?php echo($L5->elementTitle1); ?></th>
                                        <th><?php echo($L5->degValue); ?></th>
                                        <th><?php echo(round($S->degValue / $L5->degValue)); ?> طالب لكل استاذ مساعد</th>
                                    </tr>
                                    <tr>
                                        <th>11</th>
                                        <th><?php echo($L6->elementTitle1); ?></th>
                                        <th><?php echo($L6->degValue); ?></th>
                                        <th><?php echo(round($S->degValue / $L6->degValue)); ?>طالب لكل محاضر </th>
                                    </tr>
                                    <tr>
                                        <th>12</th>
                                        <th>المساحة الكلية</th>
                                        <th><?php echo($ar4); ?> متر مربع</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
