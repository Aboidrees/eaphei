<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Staff</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">هيئة التدريس ومعاونيهم حسب الرتبة العلمية</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">الكلية</th>
                            <th class="text-center" colspan="2">أستاذ</th>
                            <th class="text-center" colspan="2">أستاذ مشارك</th>
                            <th class="text-center" colspan="2">أستاذ مساعد</th>
                            <th class="text-center" colspan="2">محاضر</th>
                            <th class="text-center" colspan="2">مساعد تدريس</th>
                            <th class="text-center" colspan="2">المجموع</th>
                        </tr>
                        <tr>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $temp0 = 0;
                        $temp1 = 0;
                        $temp2 = 0;
                        $temp3 = 0;
                        $temp4 = 0;
                        $temp5 = 0;
                        $temp6 = 0;
                        $temp7 = 0;
                        $temp8 = 0;
                        $temp9 = 0;
                        ?>
                        <?php foreach ($staff_array['staffByPosition'] as $facName => $staff) : ?>		
                            <tr class="text-center">
                                <?php echo '<th>' . $facName . '</th>'; ?>
                                <?php echo '<td>' . $staff[0] . '</td>'; ?>
                                <?php echo '<td>' . $staff[1] . '</td>'; ?>
                                <?php echo '<td>' . $staff[2] . '</td>'; ?>
                                <?php echo '<td>' . $staff[3] . '</td>'; ?>
                                <?php echo '<td>' . $staff[4] . '</td>'; ?>
                                <?php echo '<td>' . $staff[5] . '</td>'; ?>
                                <?php echo '<td>' . $staff[6] . '</td>'; ?>
                                <?php echo '<td>' . $staff[7] . '</td>'; ?>
                                <?php echo '<td>' . $staff[8] . '</td>'; ?>
                                <?php echo '<td>' . $staff[9] . '</td>'; ?>

                                <?php $temp0 += $staff[0]; ?>
                                <?php $temp1 += $staff[1]; ?>
                                <?php $temp2 += $staff[2]; ?>
                                <?php $temp3 += $staff[3]; ?>
                                <?php $temp4 += $staff[4]; ?>
                                <?php $temp5 += $staff[5]; ?>
                                <?php $temp6 += $staff[6]; ?>
                                <?php $temp7 += $staff[7]; ?>
                                <?php $temp8 += $staff[8]; ?>
                                <?php $temp9 += $staff[9]; ?>
                                <td><?php echo ($staff[0] + $staff[2] + $staff[4] + $staff[6] + $staff[8]); ?></td>
                                <td><?php echo ($staff[1] + $staff[3] + $staff[5] + $staff[7] + $staff[9]); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="text-center">
                            <th>المجموع</th>
                            <?php echo '<td>' . $temp0 . '</td>'; ?>	
                            <?php echo '<td>' . $temp1 . '</td>'; ?>	
                            <?php echo '<td>' . $temp2 . '</td>'; ?>	
                            <?php echo '<td>' . $temp3 . '</td>'; ?>	
                            <?php echo '<td>' . $temp4 . '</td>'; ?>	
                            <?php echo '<td>' . $temp5 . '</td>'; ?>	
                            <?php echo '<td>' . $temp6 . '</td>'; ?>	
                            <?php echo '<td>' . $temp7 . '</td>'; ?>	
                            <?php echo '<td>' . $temp8 . '</td>'; ?>	
                            <?php echo '<td>' . $temp9 . '</td>'; ?>	
                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>		
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">هيئة التدريس ومعاونيهم حسب المؤهل</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">الكلية</th>
                            <th class="text-center" colspan="2">دكتوراة الفلسفة</th>
                            <th class="text-center" colspan="2">ماجستير العلوم</th>
                            <th class="text-center" colspan="2">الدبلوم العالي</th>
                            <th class="text-center" colspan="2">بكالريوس العلوم</th>
                            <th class="text-center" colspan="2">المجموع</th>
                        </tr>
                        <tr>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $temp0 = 0;
                        $temp1 = 0;
                        $temp2 = 0;
                        $temp3 = 0;
                        $temp4 = 0;
                        $temp5 = 0;
                        $temp6 = 0;
                        $temp7 = 0;
                        ?>
                        <?php foreach ($staff_array['staffByQualification'] as $facName => $staff) : ?>		
                            <tr class="text-center">
                                <?php echo '<th>' . $facName . '</th>'; ?>
                                <?php echo '<td>' . $staff[0] . '</td>'; ?>
                                <?php echo '<td>' . $staff[1] . '</td>'; ?>
                                <?php echo '<td>' . $staff[2] . '</td>'; ?>
                                <?php echo '<td>' . $staff[3] . '</td>'; ?>
                                <?php echo '<td>' . $staff[4] . '</td>'; ?>
                                <?php echo '<td>' . $staff[5] . '</td>'; ?>
                                <?php echo '<td>' . $staff[6] . '</td>'; ?>
                                <?php echo '<td>' . $staff[7] . '</td>'; ?>

                                <?php $temp0 += $staff[0]; ?>
                                <?php $temp1 += $staff[1]; ?>
                                <?php $temp2 += $staff[2]; ?>
                                <?php $temp3 += $staff[3]; ?>
                                <?php $temp4 += $staff[4]; ?>
                                <?php $temp5 += $staff[5]; ?>
                                <?php $temp6 += $staff[6]; ?>
                                <?php $temp7 += $staff[7]; ?>

                                <td><?php echo ($staff[0] + $staff[2] + $staff[4] + $staff[6]); ?></td>
                                <td><?php echo ($staff[1] + $staff[3] + $staff[5] + $staff[7]); ?></td>

                            </tr>
                        <?php endforeach; ?>
                        <tr class="text-center">
                            <th>المجموع</th>
                            <?php echo '<td>' . $temp0 . '</td>'; ?>	
                            <?php echo '<td>' . $temp1 . '</td>'; ?>	
                            <?php echo '<td>' . $temp2 . '</td>'; ?>	
                            <?php echo '<td>' . $temp3 . '</td>'; ?>	
                            <?php echo '<td>' . $temp4 . '</td>'; ?>	
                            <?php echo '<td>' . $temp5 . '</td>'; ?>	
                            <?php echo '<td>' . $temp6 . '</td>'; ?>	
                            <?php echo '<td>' . $temp7 . '</td>'; ?>	
                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7); ?></td>	
                        </tr>     
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">التسرب</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">الكلية</th>
                            <th class="text-center" colspan="2">أستاذ</th>
                            <th class="text-center" colspan="2">أستاذ مشارك</th>
                            <th class="text-center" colspan="2">أستاذ مساعد</th>
                            <th class="text-center" colspan="2">محاضر</th>
                            <th class="text-center" colspan="2">مساعد تدريس</th>
                            <th class="text-center" colspan="2">المجموع</th>
                        </tr>
                        <tr>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $temp0 = 0;
                        $temp1 = 0;
                        $temp2 = 0;
                        $temp3 = 0;
                        $temp4 = 0;
                        $temp5 = 0;
                        $temp6 = 0;
                        $temp7 = 0;
                        $temp8 = 0;
                        $temp9 = 0;
                        ?>
                        <?php foreach ($staff_array['leak'] as $facName => $staff) : ?>		
                            <tr class="text-center">
                                <?php echo '<th>' . $facName . '</th>'; ?>
                                <?php echo '<td>' . $staff[0] . '</td>'; ?>
                                <?php echo '<td>' . $staff[1] . '</td>'; ?>
                                <?php echo '<td>' . $staff[2] . '</td>'; ?>
                                <?php echo '<td>' . $staff[3] . '</td>'; ?>
                                <?php echo '<td>' . $staff[4] . '</td>'; ?>
                                <?php echo '<td>' . $staff[5] . '</td>'; ?>
                                <?php echo '<td>' . $staff[6] . '</td>'; ?>
                                <?php echo '<td>' . $staff[7] . '</td>'; ?>
                                <?php echo '<td>' . $staff[8] . '</td>'; ?>
                                <?php echo '<td>' . $staff[9] . '</td>'; ?>

                                <?php $temp0 += $staff[0]; ?>
                                <?php $temp1 += $staff[1]; ?>
                                <?php $temp2 += $staff[2]; ?>
                                <?php $temp3 += $staff[3]; ?>
                                <?php $temp4 += $staff[4]; ?>
                                <?php $temp5 += $staff[5]; ?>
                                <?php $temp6 += $staff[6]; ?>
                                <?php $temp7 += $staff[7]; ?>
                                <?php $temp8 += $staff[8]; ?>
                                <?php $temp9 += $staff[9]; ?>

                                <td><?php echo ($staff[0] + $staff[2] + $staff[4] + $staff[6] + $staff[8]); ?></td>
                                <td><?php echo ($staff[1] + $staff[3] + $staff[5] + $staff[7] + $staff[9]); ?></td>
                            </tr>
                        <?php endforeach; ?>					
                        <tr class="text-center">
                            <th>المجموع</th>
                            <?php echo '<td>' . $temp0 . '</td>'; ?>	
                            <?php echo '<td>' . $temp1 . '</td>'; ?>	
                            <?php echo '<td>' . $temp2 . '</td>'; ?>	
                            <?php echo '<td>' . $temp3 . '</td>'; ?>	
                            <?php echo '<td>' . $temp4 . '</td>'; ?>	
                            <?php echo '<td>' . $temp5 . '</td>'; ?>	
                            <?php echo '<td>' . $temp6 . '</td>'; ?>	
                            <?php echo '<td>' . $temp7 . '</td>'; ?>	
                            <?php echo '<td>' . $temp8 . '</td>'; ?>	
                            <?php echo '<td>' . $temp9 . '</td>'; ?>	
                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>		
                        </tr>
                </table>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">هيئة التدريس ومعاونيهم من الأجانب</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">الكلية</th>
                            <th class="text-center" colspan="2">أستاذ</th>
                            <th class="text-center" colspan="2">أستاذ مشارك</th>
                            <th class="text-center" colspan="2">أستاذ مساعد</th>
                            <th class="text-center" colspan="2">محاضر</th>
                            <th class="text-center" colspan="2">مساعد تدريس</th>
                            <th class="text-center" colspan="2">المجموع</th>
                        </tr>
                        <tr>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $temp0 = 0;
                        $temp1 = 0;
                        $temp2 = 0;
                        $temp3 = 0;
                        $temp4 = 0;
                        $temp5 = 0;
                        $temp6 = 0;
                        $temp7 = 0;
                        $temp8 = 0;
                        $temp9 = 0;
                        ?>
                        <?php foreach ($staff_array['foreigners'] as $facName => $staff) : ?>		
                            <tr class="text-center">

                                <?php echo '<th>' . $facName . '</th>'; ?>
                                <?php echo '<td>' . $staff[0] . '</td>'; ?>
                                <?php echo '<td>' . $staff[1] . '</td>'; ?>
                                <?php echo '<td>' . $staff[2] . '</td>'; ?>
                                <?php echo '<td>' . $staff[3] . '</td>'; ?>
                                <?php echo '<td>' . $staff[4] . '</td>'; ?>
                                <?php echo '<td>' . $staff[5] . '</td>'; ?>
                                <?php echo '<td>' . $staff[6] . '</td>'; ?>
                                <?php echo '<td>' . $staff[7] . '</td>'; ?>
                                <?php echo '<td>' . $staff[8] . '</td>'; ?>
                                <?php echo '<td>' . $staff[9] . '</td>'; ?>

                                <?php $temp0 += $staff[0]; ?>
                                <?php $temp1 += $staff[1]; ?>
                                <?php $temp2 += $staff[2]; ?>
                                <?php $temp3 += $staff[3]; ?>
                                <?php $temp4 += $staff[4]; ?>
                                <?php $temp5 += $staff[5]; ?>
                                <?php $temp6 += $staff[6]; ?>
                                <?php $temp7 += $staff[7]; ?>
                                <?php $temp8 += $staff[8]; ?>
                                <?php $temp9 += $staff[9]; ?>

                                <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                                <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>
                            </tr>
                        <?php endforeach; ?>					


                        <tr class="text-center">
                            <th>المجموع</th>
                            <?php echo '<td>' . $temp0 . '</td>'; ?>	
                            <?php echo '<td>' . $temp1 . '</td>'; ?>	
                            <?php echo '<td>' . $temp2 . '</td>'; ?>	
                            <?php echo '<td>' . $temp3 . '</td>'; ?>	
                            <?php echo '<td>' . $temp4 . '</td>'; ?>	
                            <?php echo '<td>' . $temp5 . '</td>'; ?>	
                            <?php echo '<td>' . $temp6 . '</td>'; ?>	
                            <?php echo '<td>' . $temp7 . '</td>'; ?>	
                            <?php echo '<td>' . $temp8 . '</td>'; ?>	
                            <?php echo '<td>' . $temp9 . '</td>'; ?>	
                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>	
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">هيئة التدريس ومعاونيهم - المتعاونون</h3>
                <hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">الكلية</th>
                            <th class="text-center" colspan="2">أستاذ</th>
                            <th class="text-center" colspan="2">أستاذ مشارك</th>
                            <th class="text-center" colspan="2">أستاذ مساعد</th>
                            <th class="text-center" colspan="2">محاضر</th>
                            <th class="text-center" colspan="2">مساعد تدريس</th>
                            <th class="text-center" colspan="2">المجموع</th>
                        </tr>
                        <tr>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                            <th class="text-center" width="6%">ذ</th>
                            <th class="text-center" width="6%">ث</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $temp0 = 0;
                        $temp1 = 0;
                        $temp2 = 0;
                        $temp3 = 0;
                        $temp4 = 0;
                        $temp5 = 0;
                        $temp6 = 0;
                        $temp7 = 0;
                        $temp8 = 0;
                        $temp9 = 0;
                        ?>
                        <?php foreach ($staff_array['duty'] as $facName => $staff) : ?>		
                            <tr class="text-center">
                                <?php echo '<th>' . $facName . '</th>'; ?>
                                <?php echo '<td>' . $staff[0] . '</td>'; ?>
                                <?php echo '<td>' . $staff[1] . '</td>'; ?>
                                <?php echo '<td>' . $staff[2] . '</td>'; ?>
                                <?php echo '<td>' . $staff[3] . '</td>'; ?>
                                <?php echo '<td>' . $staff[4] . '</td>'; ?>
                                <?php echo '<td>' . $staff[5] . '</td>'; ?>
                                <?php echo '<td>' . $staff[6] . '</td>'; ?>
                                <?php echo '<td>' . $staff[7] . '</td>'; ?>
                                <?php echo '<td>' . $staff[8] . '</td>'; ?>
                                <?php echo '<td>' . $staff[9] . '</td>'; ?>

                                <?php $temp0 += $staff[0]; ?>
                                <?php $temp1 += $staff[1]; ?>
                                <?php $temp2 += $staff[2]; ?>
                                <?php $temp3 += $staff[3]; ?>
                                <?php $temp4 += $staff[4]; ?>
                                <?php $temp5 += $staff[5]; ?>
                                <?php $temp6 += $staff[6]; ?>
                                <?php $temp7 += $staff[7]; ?>
                                <?php $temp8 += $staff[8]; ?>
                                <?php $temp9 += $staff[9]; ?>
                                <td><?php echo ($staff[0] + $staff[2] + $staff[4] + $staff[6] + $staff[8]); ?></td>
                                <td><?php echo ($staff[1] + $staff[3] + $staff[5] + $staff[7] + $staff[9]); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="text-center">
                            <th>المجموع</th>
                            <?php echo '<td>' . $temp0 . '</td>'; ?>	
                            <?php echo '<td>' . $temp1 . '</td>'; ?>	
                            <?php echo '<td>' . $temp2 . '</td>'; ?>	
                            <?php echo '<td>' . $temp3 . '</td>'; ?>	
                            <?php echo '<td>' . $temp4 . '</td>'; ?>	
                            <?php echo '<td>' . $temp5 . '</td>'; ?>	
                            <?php echo '<td>' . $temp6 . '</td>'; ?>	
                            <?php echo '<td>' . $temp7 . '</td>'; ?>	
                            <?php echo '<td>' . $temp8 . '</td>'; ?>	
                            <?php echo '<td>' . $temp9 . '</td>'; ?>	
                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>		
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>	
</section>
