<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Faculties</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Faculties</h3><hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                if (count($faculties)) {
                    foreach ($faculties as $faculty) {
                        ?>
                        <table class="table table-striped">
                            <tr>
                                <th>Faculty Name</th>
                                <th>Telephone</th>
                                <th>Fax</th>
                                <th>Foundation Date</th>

                            </tr>
                            <tr>
                                <td><?php echo $faculty->facName; ?></td>
                                <td><?php echo $faculty->facPhone; ?></td>
                                <td><?php echo $faculty->facFax; ?></td>
                                <td><?php echo date("d-m-Y", $faculty->facFoundationDate); ?></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td colspan="3"><?php echo $faculty->facAddress; ?></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="container">
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 12em;">Departments</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->DepNum; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 12em;">Programs</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->ProgNum; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 12em;">Infrastructures</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->infrastructureNum; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 12em;">Books</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->StaffBookNum; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 12em;">Publications</td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo $faculty->PublicationNum; ?></span></td>
                                                </tr>
                                        </table>
                                    </div>
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 12em;">Staff Members</td>
                                                    <td><?php echo $faculty->StaffNum; ?></td>
                                                    <td style="width: 3em;"></td>
                                                </tr>
                                                <tr>
                                                    <td>Available Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->AvailableStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->AvailableStaffNum; ?>
                                                            </div>

                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->AvailableStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Not Available Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @(($faculty->StaffNum - $faculty->AvailableStaffNum) / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->StaffNum - $faculty->AvailableStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @(($faculty->StaffNum - $faculty->AvailableStaffNum) / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Loaned Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->LoanedStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->LoanedStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->LoanedStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Scholarship Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->ScholarshipStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->ScholarshipStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->ScholarshipStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Contract Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->ContractStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->ContractStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->ContractStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Vacation Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->VacationStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->VacationStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->VacationStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Pension Staff</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->PensionStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->PensionStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->PensionStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Other</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->OtherStaffNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->OtherStaffNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->OtherStaffNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-4">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 12em;">Professors</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->ProfessorNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->ProfessorNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 3em;"><span class="badge bg-green"><?php echo @($faculty->ProfessorNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Associate Professors</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @(($faculty->AssociateProfessorNum) / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->AssociateProfessorNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @(($faculty->AssociateProfessorNum) / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Associate Professors</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->AssociateProfessorNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->AssociateProfessorNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->AssociateProfessorNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>

                                                <tr>
                                                    <td>Assistant Professors</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->AssistantProfessorNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->AssistantProfessorNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->AssistantProfessorNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Lecturers</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->LecturerNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->LecturerNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->LecturerNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Teaching Assistants</td>
                                                    <td>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" style="width: <?php echo @($faculty->TeachingAssistantNum / $faculty->StaffNum) * 100; ?>%">
                                                                <?php echo $faculty->TeachingAssistantNum; ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge bg-green"><?php echo @($faculty->TeachingAssistantNum / $faculty->StaffNum) * 100; ?>%</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php
                    }
                }
                ?>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <?php echo anchor_popup('report/faculties_print', '<i class="fa fa-print"></i> Print', 'class = "btn btn-default"'); ?>
            </div>
        </div>
    </div>
</section>