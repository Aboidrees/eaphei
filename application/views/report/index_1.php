<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li class="active">Report</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">

            <div class="box-header">
                <h3 class="box-title">Reports</h3><hr style="margin: 5px 0;">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <?php echo anchor("report/faculties", "Faculties",'class="btn btn-success"');?>
				<?php echo anchor("report/infrastructures", "Infrastructures",'class="btn btn-success"');?>
				<?php echo anchor("report/staff", "Staff",'class="btn btn-success"');?>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>