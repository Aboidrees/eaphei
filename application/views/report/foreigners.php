<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'Home'); ?></li>
            <li><?php echo anchor('report', 'Report'); ?></li>
            <li class="active">Staff</li>
        </ol>          
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Staff</h3>
                <hr style="margin: 5px 0;">
                <?php //echo '<pre>'.var_dump($staff_array).'</pre>';?>
                <table class="table table-bordered">
                    <tr>
                        <th rowspan="2">Faculty</th>
                        <th colspan="2">Professor</th>
                        <th colspan="2">Associate Professor</th>
                        <th colspan="2">Assistant Professor</th>
                        <th colspan="2">Lectuerer</th>
                        <th colspan="2">Teaching Assistant</th>
                        <th colspan="2">Total</th>
                    </tr>
                    <tr>
                        <th>M</th>
                        <th>F</th>
                        <th>M</th>
                        <th>F</th>						
                        <th>M</th>
                        <th>F</th>						
                        <th>M</th>
                        <th>F</th>						
                        <th>M</th>
                        <th>F</th>
                        <th>M</th>
                        <th>F</th>
                    </tr>
                    <?php
                    $temp0 = 0;
                    $temp1 = 0;
                    $temp2 = 0;
                    $temp3 = 0;
                    $temp4 = 0;
                    $temp5 = 0;
                    $temp6 = 0;
                    $temp7 = 0;
                    $temp8 = 0;
                    $temp9 = 0;
                    ?>
                        <?php foreach ($staff_array as $facName => $staff) : ?>		
                        <tr>

                            <?php echo '<td>' . $facName . '</td>'; ?>
                            <?php echo '<td>' . $staff[0] . '</td>'; ?>
                            <?php echo '<td>' . $staff[1] . '</td>'; ?>
                            <?php echo '<td>' . $staff[2] . '</td>'; ?>
                            <?php echo '<td>' . $staff[3] . '</td>'; ?>
                            <?php echo '<td>' . $staff[4] . '</td>'; ?>
                            <?php echo '<td>' . $staff[5] . '</td>'; ?>
                            <?php echo '<td>' . $staff[6] . '</td>'; ?>
                            <?php echo '<td>' . $staff[7] . '</td>'; ?>
                            <?php echo '<td>' . $staff[8] . '</td>'; ?>
                            <?php echo '<td>' . $staff[9] . '</td>'; ?>

                            <?php $temp0 += $staff[0]; ?>
                            <?php $temp1 += $staff[1]; ?>
                            <?php $temp2 += $staff[2]; ?>
                            <?php $temp3 += $staff[3]; ?>
                            <?php $temp4 += $staff[4]; ?>
                            <?php $temp5 += $staff[5]; ?>
                            <?php $temp6 += $staff[6]; ?>
                            <?php $temp7 += $staff[7]; ?>
    <?php $temp8 += $staff[8]; ?>
    <?php $temp9 += $staff[9]; ?>

                            <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                            <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>
<?php endforeach; ?>					
                    </tr>

                    <tr>
                        <td>Total</td>
                        <?php echo '<td>' . $temp0 . '</td>'; ?>	
                        <?php echo '<td>' . $temp1 . '</td>'; ?>	
                        <?php echo '<td>' . $temp2 . '</td>'; ?>	
                        <?php echo '<td>' . $temp3 . '</td>'; ?>	
                        <?php echo '<td>' . $temp4 . '</td>'; ?>	
                        <?php echo '<td>' . $temp5 . '</td>'; ?>	
                        <?php echo '<td>' . $temp6 . '</td>'; ?>	
                        <?php echo '<td>' . $temp7 . '</td>'; ?>	
<?php echo '<td>' . $temp8 . '</td>'; ?>	
<?php echo '<td>' . $temp9 . '</td>'; ?>	
                        <td><?php echo ($temp0 + $temp2 + $temp4 + $temp6 + $temp8); ?></td>
                        <td><?php echo ($temp1 + $temp3 + $temp5 + $temp7 + $temp9); ?></td>	
                    </tr>

                </table>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
        </div>
    </div>	
</section>
