<section class="content-header">
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li><?php echo anchor('home', 'الرئيسية'); ?></li>
            <li class = "active">الدورات</li>
        </ol>          
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?php echo anchor('course/edit', '<i class="ti-plus"></i> إضافة دورة', 'class="btn btn-primary"'); ?>
        <br>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">الدورات التدريبية</h3>
                <div class="box-tools">
                    <?php echo form_open(); ?>
                    <div class="input-group input-group-sm" style="width: 200px;">
                        <?php echo form_input('instName', '', 'placeholder="ابحث عن دورة ...." class="form-control pull-right"'); ?> 
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?> 
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>اسم الدورة</th>
                            <th>تعديل</th>
                            <th>حذف</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($course)) {
                            foreach ($course as $course) {
                                ?>
                                <tr>
                                    <td><?php echo anchor("course/edit/$course->cCode", $course->cTitle); ?></td>
                                    <td><?php echo btn_edit("course/edit/$course->cCode") ?></td>
                                    <td><?php echo btn_delete("course/delete/$course->cCode") ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">ﻻ يوجد دورات</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
