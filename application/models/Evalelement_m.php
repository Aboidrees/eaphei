<?php

class evalelement_m extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    protected $_primary_key = 'elementID';
    public $_table_name = 'evalelement';
    public $rules = array(
        'elementTitle1' => array(
            'field' => 'elementTitle1',
            'label' => 'Element Title',
            'rules' => 'trim|max_length[256]|xss_clean',
        ),
        'elementTitle2' => array(
            'field' => 'elementTitle2',
            'label' => 'Element Title',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'elementParent' => array(
            'field' => 'elementParent',
            'label' => 'Element Parent',
            'rules' => 'trim|required|intval',
        ),
        'elementEquation' => array(
            'field' => 'elementEquation',
            'label' => 'Element Equation',
            'rules' => 'trim|max_length[512]|xss_clean',
        ),
        'elementSymbol' => array(
            'field' => 'elementSymbol',
            'label' => 'Element Symbol',
            'rules' => 'trim|max_length[4]|xss_clean',
        ),
        'elementWeight' => array(
            'field' => 'elementWeight',
            'label' => 'Element Weight',
            'rules' => 'trim|required|doubleval',
        ),
        'elementTypicalDgree' => array(
            'field' => 'elementTypicalDgree',
            'label' => 'Element Typical Dgree',
            'rules' => 'trim|doubleval',
        ),
        'editValue' => array(
            'field' => 'editValue',
            'label' => 'Editing Element Value',
            'rules' => 'trim|max_length[4]|xss_clean',
        ),
        'efID' => array(
            'field' => 'efID',
            'label' => 'Evaluation Form',
            'rules' => 'trim|max_length[4]|xss_clean',
        ),
    );

    public function get_new() {
        $evalelement = new stdClass();
        $evalelement->elementTitle1 = '';
        $evalelement->elementTitle2 = '';
        $evalelement->elementParent = '';
        $evalelement->elementEquation = '';
        $evalelement->elementSymbol = '';
        $evalelement->elementWeight = 1;
        $evalelement->elementTypicalDgree = '';
        $evalelement->editValue = '';
        $evalelement->efID = 0;
        return $evalelement;
    }

    public function getEvelElements($data) {
        $array = array();
        $evalelements = $this->get_by(array('elementParent' => $data['elementParent'],'efID' => $data['efID']));
        foreach ($evalelements as $index => $evalelement) {
            $this->db->group_by('elementID, evalID, varName');
            $degreeValue[$index] = $this->getValues("evalID = {$data['evalID']} and elementID = $evalelement->elementID", TRUE);
            $evalelement->evalID = $data['evalID'];
            $evalelement->degValue = $degreeValue[$index];
            $array[$index] = $evalelement;

            //$array['elementIDs'][] = $evalelement->elementID;
        }
        return $array;
    }

    public function get_by_join($data) {
        $this->db->join('degree', 'degree.elementID = evalelement.elementID'); //degID 	degValue    evalID	 	 
        return $this->get_by($data);
    }

    public function equationProcessing(array $array, $parent_symbol = NULL, $sub_arrays_result = array()) {
        $results = array();
        foreach ($array as $sub_array) {
            $equation = $sub_array->elementEquation; // equation copy

            if($sub_array->elementTypicalDgree !== '0'){
                $equation = str_replace('Typical', $sub_array->elementTypicalDgree, $equation);
            }
            
            if (preg_match_all('[((\[)(\w+)(\]))]', $equation)) {
                foreach ($sub_array->degValue as $degree) {
                    $equation = str_replace('[' . $degree->varName . ']', $degree->degValue, $equation);
                }
            }

            // contain [Variables]
            if (strstr($equation, 'sum(' . strtoupper($sub_array->elementSymbol) . ')')) {

                $search = array('sum(' . strtoupper($sub_array->elementSymbol) . ')');
                $replace = array(array_sum(vectorMultiply($sub_arrays_result[$sub_array->elementID][$sub_array->elementSymbol], $sub_arrays_result[$sub_array->elementID]['weights'])));
                $equation = str_replace("sum(", "array_sum(", str_replace($search, $replace, $equation));
            }
            if (strstr($equation, 'sum') and strstr($equation, 'weight')) {
                $search = array('sum(' . strtoupper($sub_array->elementSymbol) . '*weight)', 'sum(weight)');
                $replace = array(
                    array_sum(vectorMultiply($sub_arrays_result[$sub_array->elementID][$sub_array->elementSymbol], $sub_arrays_result[$sub_array->elementID]['weights'])),
                    array_sum($sub_arrays_result[$sub_array->elementID]['weights'])
                );
                $equation = str_replace("sum(", "array_sum(", str_replace($search, $replace, $equation));
            }
//            if (!empty(array_keys($sub_arrays_result[$sub_array->elementID])[0])) {
//                $symbol = array_keys($sub_arrays_result[$sub_array->elementID])[0];
//            }
            if (stripos($equation, '+')) {
                $equation = str_replace($sub_arrays_result[$sub_array->elementID]['symbol'], $sub_arrays_result[$sub_array->elementID][$sub_array->elementSymbol], $equation);
            }

            if (empty($equation)) {
                $equation = $sub_array->degValue[0]->degValue;
            }
            if (stripos($equation, '0/0')) {
                $equation = 0;
            }
            if ($equation != 'empty' and ! stripos($equation, 'sum')) {
                @eval('$sub_array->degValue   = floatval(' . $equation . ');');
            }

            $sub_array->degValue <= 100 || $sub_array->degValue = 100.0;

            if ($parent_symbol !== NULL) {
                $results[$parent_symbol][] = $sub_array->degValue;
                $results["weights"][] = floatval($sub_array->elementWeight);
                $results['symbol'][] = $sub_array->elementSymbol;
            }
            $results['arrays'][] = $sub_array;
        }
        return $results;
    }

    public function getValues($data) {
        $this->_primary_key = 'elementID';
        $this->_table_name = 'degree';
        $array = $this->get_by($data);
        $this->_table_name = 'evalelement';
        return $array;
    }

    public function saveValues($data, $id, $manual_primary_key_insert) {
        $this->_primary_key = 'elementID';
        $this->_table_name = 'degree';
        $theid = $this->save($data, $id, $manual_primary_key_insert);
        $this->_table_name = 'evalelement';
        return $theid;
    }

}
