<?php

class User_M extends MY_Model {

    protected $_table_name = 'user';
    protected $_order_by = 'userName';
    protected $_primary_key = 'userID';
    public $rules = array(
        'userMail' => array(
            'field' => 'userMail',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|xss_clean'
        ),
        'userPass' => array(
            'field' => 'userPass',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    );
    public $rules_admin = array(
        'userName' => array(
            'field' => 'userName',
            'label' => 'Name',
            'rules' => 'trim|required|max[64]|xss_clean'
        ),
        'userMail' => array(
            'field' => 'userMail',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|max[64]|xss_clean'
        ),
        'userPass' => array(
            'field' => 'userPass',
            'label' => 'Password',
            'rules' => 'trim|callback__hash'
        ),
        'userPass_confirm' => array(
            'field' => 'userPass_confirm',
            'label' => 'Confirm password',
            'rules' => 'trim|callback__hash|matches[userPass]'
        ),
        'userGroup' => array(
            'field' => 'userGroup',
            'label' => 'User Group',
            'rules' => 'trim|max[4]|xss_clean'
        ),
        'userStatus' => array(
            'field' => 'userStatus',
            'label' => 'User Status',
            'rules' => 'trim|max[16]|xss_clean'
        ),
        'userPhone' => array(
            'field' => 'userPhone',
            'label' => 'User Phone',
            'rules' => 'trim|required|max[16]|xss_clean'
        ),
        'instID' => array(
            'field' => 'instID',
            'label' => 'Institution',
            'rules' => 'trim|intval'
        ),
        'facID' => array(
            'field' => 'facID',
            'label' => 'faculty',
            'rules' => 'trim|intval'
        ),
        'depID' => array(
            'field' => 'depID',
            'label' => 'department',
            'rules' => 'trim|intval'
        ),
        'progID' => array(
            'field' => 'progID',
            'label' => 'Program',
            'rules' => 'trim|intval'
        ),
    );

    function __construct() {
        parent::__construct();
    }

    public function login() {
        // data from the form
        $user_login_info['userMail'] = $this->input->post('userMail');
        $user_login_info['userPass'] = $this->hash($this->input->post('userPass'));
        $user = $this->get_by($user_login_info, TRUE);

        if (count($user)) {
            // Log in user
            $data['userID'] = intval($user->userID);
            $data['loggedIn'] = TRUE;
            $data['userGroup'] = $user->userGroup;
            $this->session->set_userdata($data);
            return TRUE;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function adminLogin() {
        if ($this->session->userdata('userGroup') == 'admin') {
            return (bool) $this->session->userdata('loggedIn');
        }
    }

    public function userLogin() {
        if ($this->session->userdata('userGroup') == 'user') {
            return (bool) $this->session->userdata('loggedIn');
        }
    }

    // TODO : Remove this function
    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }

    public function mod_login() {
        if ($this->session->userdata('userGroup') == 2) {
            return TRUE;
        }
    }

    public function get_new() {
        $user = new stdClass();
        $user->userName = '';
        $user->userMail = '';
        $user->userPass = '';
        $user->userPhone = '';
        $user->userStatus = 'active';
        $user->userGroup = '';
        $user->instID = 0;
        $user->facID = 0;
        $user->depID = 0;
        $user->progID = 0;
        return $user;
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

}
