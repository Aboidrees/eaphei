<?php
class Course_m extends MY_Model {

    protected $_primary_key = 'cCode';
    protected $_table_name = 'course';
    protected $_order_by = 'cTitle';
    public $rules = array(
        'cCode' => array(
            'field' => 'cCode',
            'label' => 'Course Code',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cTitle' => array(
            'field' => 'cTitle',
            'label' => 'Course Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'cContactHours' => array(
            'field' => 'cContactHours',
            'label' => 'Course Contact Hours',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cReletedModules' => array(
            'field' => 'cReletedModules',
            'label' => 'Course Releted Modules',
            'rules' => 'trim|required|xss_clean',
        ),
        'cObjectives' => array(
            'field' => 'cObjectives',
            'label' => 'Course Objectives',
            'rules' => 'trim|required|xss_clean',
        ),
        'cPrerequisites' => array(
            'field' => 'cPrerequisites',
            'label' => 'Course Prerequisites',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutcomes' => array(
            'field' => 'cOutcomes',
            'label' => 'Course Outcomes',
            'rules' => 'trim|required|xss_clean',
        ), 
        'cLearninigMethod' => array(
            'field' => 'cLearninigMethod',
            'label' => 'Course Code',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutline' => array(
            'field' => 'cOutline',
            'label' => 'Course Outline',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommSoftware' => array(
            'field' => 'cRecommSoftware',
            'label' => 'Course Recommended Software',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommHardware' => array(
            'field' => 'cRecommHardware',
            'label' => 'Course Recommended Hardware',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommBooks' => array(
            'field' => 'cRecommBooks',
            'label' => 'Course Recommended Books',
            'rules' => 'trim|required|xss_clean',
        ),
        'cAssesstment' => array(
            'field' => 'cAssesstment',
            'label' => 'Course Assesstment and Evaluation',
            'rules' => 'trim|required|xss_clean',
        ),
        'cAttendence' => array(
            'field' => 'cAttendence',
            'label' => 'Course Attendence',
            'rules' => 'trim|required|xss_clean',
        ),
    );

    public function get_new() {
        $course = new stdClass();
        $course->cCode = '';
        $course->cTitle = '';
        $course->cContactHours = '';
        $course->cReletedModules = '';
        $course->cObjectives = '';
        $course->cPrerequisites = '';
        $course->cOutcomes = '';
        $course->cOutline = '';
        $course->cLearninigMethod = '';
        $course->cRecommSoftware = '';
        $course->cRecommHardware = '';
        $course->cRecommBooks = '';
        $course->cAssesstment = '';
        $course->cAttendence = '';
        return $course;
    }

}
