<?php

class StaffTrainning_m extends MY_Model {

    protected $_primary_key = 'tCode';
    protected $_table_name = 'staffTrainning';
    protected $_order_by = 'tTitle';
    public $rules = array(
        'tCode' => array(
            'field' => 'tCode',
            'label' => 'staffTrainning Code',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'tTitle' => array(
            'field' => 'tTitle',
            'label' => 'staffTrainning Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'tContactHours' => array(
            'field' => 'tContactHours',
            'label' => 'staffTrainning Contact Hours',
            'rules' => 'trim|required|max_length[4]|xss_clean',
        ),
        'tObjectives' => array(
            'field' => 'tObjectives',
            'label' => 'staffTrainning Objectives',
            'rules' => 'trim|required|xss_clean',
        ),
        'tPrerequisites' => array(
            'field' => 'tPrerequisites',
            'label' => 'staffTrainning Prerequisites',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'tAttendence' => array(
            'field' => 'tAttendence',
            'label' => 'staffTrainning Attendence',
            'rules' => 'trim|required|xss_clean',
        ),
        'tOutline' => array(
            'field' => 'tOutline',
            'label' => 'staffTrainning Outline',
            'rules' => 'trim|required|xss_clean',
        ),
        'tLearninigMethod' => array(
            'field' => 'tLearninigMethod',
            'label' => 'staffTrainning Learninig Method',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
        'tRecommResource' => array(
            'field' => 'tRecommResource',
            'label' => 'staffTrainning Recommended Resource',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'tAssesstment' => array(
            'field' => 'tAssesstment',
            'label' => 'staffTrainning Assesstment',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'tValidation' => array(
            'field' => 'tValidation',
            'label' => 'staffTrainning Validation',
            'rules' => 'trim|required|max_length[16]|xss_clean|toDate',
        ),            
    );

    public function get_new() {
        $staffTrainning = new stdClass();
        $staffTrainning->tCode = '';
        $staffTrainning->tTitle = '';
        $staffTrainning->tContactHours = '';
        $staffTrainning->tObjectives = '';
        $staffTrainning->tPrerequisites = '';
        $staffTrainning->tAttendence = '';
        $staffTrainning->tOutline = '';
        $staffTrainning->tLearninigMethod = '';
        $staffTrainning->tRecommResource = '';
        $staffTrainning->tAssesstment = '';
        $staffTrainning->tValidation = '';
        return $staffTrainning;
    }

}
