<?php

class Program_m extends MY_Model {

    protected $_primary_key = 'progID';
    protected $_table_name = 'program';
    protected $_order_by = 'progName';
    public $rules = array(
        'progName' => array(
            'field' => 'progName',
            'label' => 'Faculty Name',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'progStartDate' => array(
            'field' => 'progStartDate',
            'label' => 'Program Start Date',
            'rules' => 'trim|required|toDate',
        ),
        'progLastReviewDate' => array(
            'field' => 'progLastReviewDate',
            'label' => 'Program Last Review Date',
            'rules' => 'trim|required|toDate',
        ),
        'progType' => array(
            'field' => 'progType',
            'label' => 'Program Type',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'depID' => array(
            'field' => 'depID',
            'label' => 'Department',
            'rules' => 'trim|required|intval',
        ),
        'progPref' => array(
            'field' => 'progPref',
            'label' => 'Program Pref',
            'rules' => 'trim|xss_clean',
        ),
    );

    public function get_new() {
        $program = new stdClass();
        $program->progName = '';
        $program->progStartDate = now();
        $program->progLastReviewDate = now();
        $program->progType = '';
        $program->depID = 0;
        $program->progPref = '';
        return $program;
    }

}
