<?php
class staffTrainning_m extends MY_Model {

    protected $_primary_key = 'cCode';
    protected $_table_name = 'staffTrainning';
    protected $_order_by = 'cTitle';
    public $rules = array(
        'cCode' => array(
            'field' => 'cCode',
            'label' => 'staffTrainning Code',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cTitle' => array(
            'field' => 'cTitle',
            'label' => 'staffTrainning Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'cContactHours' => array(
            'field' => 'cContactHours',
            'label' => 'staffTrainning Contact Hours',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'cReletedModules' => array(
            'field' => 'cReletedModules',
            'label' => 'staffTrainning Releted Modules',
            'rules' => 'trim|required|xss_clean',
        ),
        'cObjectives' => array(
            'field' => 'cObjectives',
            'label' => 'staffTrainning Objectives',
            'rules' => 'trim|required|xss_clean',
        ),
        'cPrerequisites' => array(
            'field' => 'cPrerequisites',
            'label' => 'staffTrainning Prerequisites',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutcomes' => array(
            'field' => 'cOutcomes',
            'label' => 'staffTrainning Outcomes',
            'rules' => 'trim|required|xss_clean',
        ), 
        'cLearninigMethod' => array(
            'field' => 'cLearninigMethod',
            'label' => 'staffTrainning Code',
            'rules' => 'trim|required|xss_clean',
        ),
        'cOutline' => array(
            'field' => 'cOutline',
            'label' => 'staffTrainning Outline',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommSoftware' => array(
            'field' => 'cRecommSoftware',
            'label' => 'staffTrainning Recommended Software',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommHardware' => array(
            'field' => 'cRecommHardware',
            'label' => 'staffTrainning Recommended Hardware',
            'rules' => 'trim|required|xss_clean',
        ),
        'cRecommBooks' => array(
            'field' => 'cRecommBooks',
            'label' => 'staffTrainning Recommended Books',
            'rules' => 'trim|required|xss_clean',
        ),
        'cAssesstment' => array(
            'field' => 'cAssesstment',
            'label' => 'staffTrainning Assesstment and Evaluation',
            'rules' => 'trim|required|xss_clean',
        ),
        'cAttendence' => array(
            'field' => 'cAttendence',
            'label' => 'staffTrainning Attendence',
            'rules' => 'trim|required|xss_clean',
        ),
    );

    public function get_new() {
        $staffTrainning = new stdClass();
        $staffTrainning->cCode = '';
        $staffTrainning->cTitle = '';
        $staffTrainning->cContactHours = '';
        $staffTrainning->cReletedModules = '';
        $staffTrainning->cObjectives = '';
        $staffTrainning->cPrerequisites = '';
        $staffTrainning->cOutcomes = '';
        $staffTrainning->cOutline = '';
        $staffTrainning->cLearninigMethod = '';
        $staffTrainning->cRecommSoftware = '';
        $staffTrainning->cRecommHardware = '';
        $staffTrainning->cRecommBooks = '';
        $staffTrainning->cAssesstment = '';
        $staffTrainning->cAttendence = '';
        return $staffTrainning;
    }

}
