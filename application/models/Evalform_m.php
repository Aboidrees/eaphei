<?php

class Evalform_m extends MY_Model {

    protected $_primary_key = 'efID';
    protected $_table_name = 'evalform';
    protected $_order_by = 'efName';
    public $rules = array(
        'efName' => array(
            'field' => 'efName',
            'label' => 'Evaluation Form Name',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'efDesc' => array(
            'field' => 'efDesc',
            'label' => 'Evaluation Form Description',
            'rules' => 'trim|xss_clean',
        ),
        'efSectro' => array(
            'field' => 'efSector',
            'label' => 'Evaluation Form Description',
            'rules' => 'trim|xss_clean',
        ),        
    );

    public function get_new() {
        $evalform = new stdClass();
        $evalform->efName = '';
        $evalform->efDesc = '';
        $evalform->efSector = '';
        return $evalform;
    }

}
