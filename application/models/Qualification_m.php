<?php

class Qualification_m extends MY_Model {

    protected $_primary_key = 'qID';
    protected $_table_name = 'qualification';
    protected $_order_by = '';
    public $rules = array(
        'qName' => array(
            'field' => 'qName',
            'label' => 'qualification Name',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
    );

    public function get_new() {
        $qualification = new stdClass();
        $qualification->qName = '';
        return $qualification;
    }

}
