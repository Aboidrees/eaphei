<?php

class Specification_m extends MY_Model {

    protected $_primary_key = 'sID';
    protected $_table_name = 'specification';
    protected $_order_by = '';
    public $rules = array(
        'sName' => array(
            'field' => 'sName',
            'label' => 'specification Name',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
    );

    public function get_new() {
        $specification = new stdClass();
        $specification->sName = '';
        return $specification;
    }

}
