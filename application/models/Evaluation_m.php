<?php

class Evaluation_m extends MY_Model {

    protected $_primary_key = 'evalID';
    public $_table_name = 'evaluation';
    public $rules = array(
        'progID' => array(
            'field' => 'progID',
            'label' => 'Program',
            'rules' => 'trim|required|intval',
        ),
        'efID' => array(
            'field' => 'efID',
            'label' => 'Evaluation Form',
            'rules' => 'trim|required|intval',
        ), 'evalDate' => array(
            'field' => 'evalDate',
            'label' => 'Evaluation Date',
            'rules' => 'trim|required|xss_clean|callback__strtotime',
        ),
        'dataGetheringDate' => array(
            'field' => 'dataGetheringDate',
            'label' => 'Data Gethering Date',
            'rules' => 'trim|required|xss_clean|callback__strtotime',
        ),
    );

    public function get_new() {
        $evaluation = new stdClass();
        $evaluation->evalDate = now();
        $evaluation->dataGetheringDate = now();
        $evaluation->efID = 0;
        $evaluation->progID = '';
        return $evaluation;
    }

    public function getStartEvaluationVars($evalID) {
        $this->db->join('program', 'program.progID = evaluation.progID');
        $this->db->join('faculty', 'faculty.facID = program.facID');
        $this->db->join('institution', 'institution.instID = faculty.instID');
        $this->db->where('evalID', $evalID);
        $array['general'] = $this->get(NULL, TRUE);

        $this->_primary_key = 'progID';
        $this->_table_name = 'program';
        $this->db->select('progType')->group_by('progType');
        $progTypes = $this->get();


        $i = 0;
        foreach ($progTypes as $progTypes) {

            $this->db->select("count(progType) as progNum");
            $this->db->where(array('progType' => $progTypes->progType, 'facID' => $array['general']->facID));
            $array['progType'][$progTypes->progType] = $this->get(NULL, TRUE)->progNum;
        }

        $this->_table_name = 'stdnumbers';
        $this->db->where(array('evalID' => $evalID, 'progID' => $array['general']->progID));
        $array['stdnumbers'] = $this->get();

        return $array;
    }

}
