<?php

class Publication_m extends MY_Model {
//'pubID', 'pubTitle', 'pubType', 'pubDate', 'pubAbstract', 'pubJournal', 'pubLink'
    protected $_primary_key = 'pubID';
    protected $_table_name = 'publication';
    protected $_order_by = '';
    public $rules = array(
        'pubTitle' => array(
            'field' => 'pubTitle',
            'label' => 'Publication Title',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'pubType' => array(
            'field' => 'pubType',
            'label' => 'Publication Type',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'pubDate' => array(
            'field' => 'pubDate',
            'label' => 'Publication Publishing Date',
            'rules' => 'trim|required|max_length[32]|xss_clean|toDate',
        ),
        'pubAbstract' => array(
            'field' => 'pubAbstract',
            'label' => 'Publication Abstract',
            'rules' => 'trim|required|xss_clean',
        ),
         'pubJournal' => array(
            'field' => 'pubJournal',
            'label' => 'Publishing Journal',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'pubLink' => array(
            'field' => 'pubLink',
            'label' => 'Publication Link',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),       
        
    );

    public function get_new() {
        $publication = new stdClass();
        $publication->pubTitle = '';
        $publication->pubDate = now();
        $publication->pubTitle = '';
        $publication->pubAbstract = '';
        $publication->pubType = '';
        $publication->pubJournal = '';
        $publication->pubLink = '';
        $publication->staffID = 0;
        return $publication;
    }

}
