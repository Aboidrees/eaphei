<?php

class Institution_m extends MY_Model {

    protected $_primary_key = 'instID';
    protected $_table_name = 'institution';
    protected $_order_by = 'instName';
    public $rules = array(
        'instName' => array(
            'field' => 'instName',
            'label' => 'Institution Name',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'instEmail' => array(
            'field' => 'instEmail',
            'label' => 'Institution Mail',
            'rules' => 'trim|required|max_length[128]|xss_clean|valid_email',
        ),
        'instPhone' => array(
            'field' => 'instPhone',
            'label' => 'Institution Phone',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'instFax' => array(
            'field' => 'instFax',
            'label' => 'Institution Fax',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
        'instFoundationDate' => array(
            'field' => 'instFoundationDate',
            'label' => 'Institution Foundation Date',
            'rules' => 'trim|required|max_length[32]',
        ),
        'instAddress' => array(
            'field' => 'instAddress',
            'label' => 'Institution Address',
            'rules' => 'trim|required|max_length[256]|xss_clean',
        ),
        'instLogo' => array(
            'field' => 'instLogo',
            'label' => 'Institution logo',
            'rules' => 'trim|max_length[32]|xss_clean',
        ),
    );

    public function get_new() {
        $institution = new stdClass();
        $institution->instName = '';
        $institution->instEmail = '';
        $institution->instPhone = '';
        $institution->instFax = '';
        $institution->instFoundationDate = '';
        $institution->instAddress = '';
        $institution->instLogo = '';
        $institution->parentID = 0;
        $institution->type = '';
        return $institution;
    }

}
