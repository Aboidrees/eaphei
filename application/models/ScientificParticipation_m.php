<?php

class ScientificParticipation_m extends MY_Model {
    protected $_primary_key = 'spID';
    protected $_table_name = 'scientificparticipation';
    protected $_order_by = '';
    public $rules = array(
        'spTitle' => array(
            'field' => 'spTitle',
            'label' => 'Scientific Participation Title',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'spType' => array(
            'field' => 'spType',
            'label' => 'Scientific Participation Type',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
        'spPublisher' => array(
            'field' => 'spPublisher',
            'label' => 'Scientific Participation Publisher',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'spDate' => array(
            'field' => 'spDate',
            'label' => 'Scientific Participation Date',
            'rules' => 'trim|required|max_length[16]|xss_clean',
        ),
         'spLink' => array(
            'field' => 'spLink',
            'label' => 'Scientific Participation Link',
            'rules' => 'trim|required|max_length[128]|xss_clean',
        ),
        'spDetails' => array(
            'field' => 'spDetails',
            'label' => 'Scientific Participation Details',
            'rules' => 'trim|required|xss_clean',
        ),       
        
    );

    public function get_new() {
        $pcientificParticipation = new stdClass();
        $pcientificParticipation->spTitle = '';
        $pcientificParticipation->spType = '';
        $pcientificParticipation->spPublisher = '';
        $pcientificParticipation->spDate = now();
        $pcientificParticipation->spLink = '';        
        $pcientificParticipation->spDetails = '';
        $pcientificParticipation->staffID = 0;
        return $pcientificParticipation;
    }

}
