<?php

class Assistancestaff_m extends MY_Model {

    protected $_primary_key = 'sID';
    protected $_table_name = 'assistancestaff';
    protected $_order_by = '';
    public $rules = array(
        'aStaffJopTitle' => array(
            'field' => 'aStaffJopTitle',
            'label' => 'assistance staff Name',
            'rules' => 'trim|required|max_length[32]|xss_clean',
        ),
    );

    public function get_new() {
        $assistancestaff = new stdClass();
        $assistancestaff->aStaffJopTitle = '';
        return $assistancestaff;
    }

}
