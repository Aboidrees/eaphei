<?php

class Group_m extends MY_Model {

    protected $_primary_key = 'groupID';
    protected $_table_name = 'group';
    protected $_order_by = 'groupName';
    public $rules = array(
        'groupName' => array(
            'field' => 'groupName',
            'label' => 'Group Name',
            'rules' => 'trim|required|max_length[64]|xss_clean',
        ),
    );

    public function get_new() {
        $group = new stdClass();
        $group->groupName = '';
        
        return $group;
    }

}
