<?php

class Admin_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'وزارة التعليم العالي | تقويم المناهج';
        $this->load->model('user_m');
        //$this->db->select('instID, facID, depID ,progID');

        if ($this->session->userdata('instID')) {
            $this->db->join('institution', 'institution.instID = user.instID');
        }

        $this->user = $this->user_m->get($this->session->userdata('userID'));
        // Login check
        $exception_uris = array('user/login', 'user/logout');
        if (in_array(uri_string(), $exception_uris) == FALSE) {
            if ($this->user_m->adminLogin() == FALSE) {
                redirect('user/login');
            }
        }
    }

}
