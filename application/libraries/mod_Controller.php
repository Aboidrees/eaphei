<?php

class Mod_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        // initialize page title
        $this->data['meta_title'] = 'تقويم برامج الكليات - ';

        // Handling user datat
        $this->_user_login_process($this->session->userdata());

        !empty($this->user) || show_error("ليس لديك الصلاحيات للدخول لهذه الصفحة");

        // Login check
        $exception_uris = array('login', 'logout');
        if (in_array(uri_string(), $exception_uris) == FALSE) {
//            $this->user_m->loggedin() || redirect('user/login');
            if ($this->user_m->userLogin() == FALSE) {
                redirect('user/login');
            }
        }
    }

    function _user_login_process($user_Session) {
        // get user info by userID stored In the session
        $this->db->select('user.userID, user.userName, user.userGroup, user.userStatus,institution.*,faculty.facID, faculty.facName,program.progID, program.progName');

        $this->db->join('program', 'program.progID = user.progID', 'left');
        $this->db->join('faculty', 'faculty.facID = user.facID', 'left');
        $this->db->join('institution', 'institution.instID = user.instID', 'left');

        $this->user = $this->user_m->get($user_Session['userID']);

        // if there is a user add user institution name to the title
        if ($this->user->instName !== NULL) {
            $this->data['meta_title'] .= $this->user->instName;
        }
        // else set general title
        else {
            $this->data['meta_title'] .= ' لوحة المستخدمين';
        }
    }

}
