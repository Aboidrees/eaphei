-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2017 at 01:09 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eaphei`
--

-- --------------------------------------------------------

--
-- Table structure for table `assistancestaff`
--

CREATE TABLE `assistancestaff` (
  `aStaffID` tinyint(3) UNSIGNED NOT NULL,
  `aStaffJopTitle` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assistancestaff`
--

INSERT INTO `assistancestaff` (`aStaffID`, `aStaffJopTitle`) VALUES
(1, 'Professor'),
(2, 'Associate Professor'),
(3, 'Assistant Professor'),
(4, 'Lecturer'),
(5, 'Teaching Assistant'),
(6, 'Instructor'),
(7, 'Administrative'),
(8, 'Technician'),
(9, 'Employee'),
(11, 'worker');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `cCode` varchar(32) NOT NULL,
  `cTitle` varchar(128) NOT NULL,
  `cContactHours` varchar(32) NOT NULL,
  `cReletedModules` text NOT NULL,
  `cObjectives` text NOT NULL,
  `cPrerequisites` text NOT NULL,
  `cOutcomes` text NOT NULL,
  `cOutline` text NOT NULL,
  `cLearninigMethod` text NOT NULL,
  `cRecommSoftware` text NOT NULL,
  `cRecommHardware` text NOT NULL,
  `cRecommBooks` text NOT NULL,
  `cAssesstment` text NOT NULL,
  `cAttendence` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`cCode`, `cTitle`, `cContactHours`, `cReletedModules`, `cObjectives`, `cPrerequisites`, `cOutcomes`, `cOutline`, `cLearninigMethod`, `cRecommSoftware`, `cRecommHardware`, `cRecommBooks`, `cAssesstment`, `cAttendence`) VALUES
('MATH001', 'اساسيات الماتلاب', '36', 'خوارزميات الحاسوب', 'تدريب الدارس على استخدام الماتلاب', 'معرفة اساسيات البرمجة', '...', '1.\r\n2.\r\n3.', '1.\r\n2.\r\n3.', '1.\r\n2.\r\n3.', '1.\r\n2.\r\n3.', '1.\r\n2.\r\n3.', '1.\r\n2.\r\n3.', '');

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE `degree` (
  `evalID` bigint(20) NOT NULL,
  `elementID` bigint(20) NOT NULL,
  `varName` varchar(32) NOT NULL,
  `degValue` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`evalID`, `elementID`, `varName`, `degValue`) VALUES
(1, 1, 'P', 0),
(1, 2, 'L', 0),
(1, 3, 'cr', 190),
(1, 3, 'cra', 192),
(1, 4, 'h', 20),
(1, 4, 'ha', 25),
(1, 5, 's', 57),
(1, 5, 'sa', 48),
(1, 6, 'es', 67),
(1, 6, 'esa', 56),
(1, 7, 'esp', 66),
(1, 7, 'espa', 63),
(1, 8, 'prw', 2200),
(1, 8, 'prwa', 2385),
(1, 9, 'proj', 6),
(1, 9, 'proja', 6),
(1, 10, 'tr', 10),
(1, 10, 'tra', 6),
(1, 11, 'M', 0),
(1, 12, 'M1', 100),
(1, 13, 'M2', 100),
(1, 14, 'M3', 100),
(1, 15, 'M4', 100),
(1, 16, 'M5', 100),
(1, 17, 'M6', 100),
(1, 18, 'N', 0),
(1, 19, 'N1', 100),
(1, 20, 'av', 82),
(1, 21, 'T', 0),
(1, 22, 'L1', 20),
(1, 22, 'S', 263),
(1, 23, 'L2', 17),
(1, 23, 'S', 263),
(1, 24, 'L3', 2),
(1, 24, 'S', 263),
(1, 25, 'L4', 2),
(1, 25, 'S', 263),
(1, 26, 'L5', 1),
(1, 26, 'S', 263),
(1, 27, 'L6', 6),
(1, 27, 'S', 263),
(1, 28, 'L7', 8),
(1, 28, 'L7a', 9),
(1, 29, 'A', 0),
(1, 30, 'A1', 70),
(1, 31, 'A2', 60),
(1, 32, 'A3', 65),
(1, 33, 'A4', 100),
(1, 34, 'A5', 0),
(1, 35, 'librarybooks', 600),
(1, 35, 'noofcourses', 70),
(1, 35, 'noofstudents', 263),
(1, 36, 'journalsAvailable', 1),
(1, 36, 'numberofprogram', 1),
(1, 37, 'A53', 15),
(1, 38, 'A6', 0),
(1, 39, 'numberavailable', 1),
(1, 39, 'numberofprograms', 1),
(1, 40, 'A62', 18),
(1, 41, 'pc', 30),
(1, 41, 'S', 263),
(1, 42, 'ch', 16),
(1, 42, 'S', 263),
(1, 43, 'F', 0),
(1, 44, 'ar41', 1005),
(1, 44, 'S', 263),
(1, 45, 'ar42', 10119),
(1, 45, 'S', 263),
(1, 46, 'ar43', 700),
(1, 46, 'S', 263),
(1, 47, 'ar44', 1284),
(1, 47, 'S', 263),
(1, 48, 'ar45', 15000),
(1, 48, 'S', 263),
(1, 49, 'ar46', 350),
(1, 49, 'S', 263),
(1, 50, 'ar47', 270),
(1, 50, 'S', 263),
(1, 51, 'F8', 0),
(1, 52, 'F81', 40),
(1, 53, 'F82', 40),
(1, 54, 'F83', 20),
(1, 55, 'F9', 0),
(1, 56, 'F91', 30),
(1, 57, 'F92', 20),
(1, 58, 'F93', 0),
(1, 59, 'E', 0),
(1, 60, 'E1', 0),
(1, 61, 'E11', 20),
(1, 62, 'E12', 30),
(1, 63, 'E13', 30),
(1, 64, 'E14', 20),
(1, 65, 'E2', 0),
(1, 66, 'E21', 20),
(1, 67, 'E22', 30),
(1, 68, 'E23', 20),
(1, 69, 'E24', 20),
(1, 70, 'E25', 10),
(1, 71, 'E3', 0),
(1, 72, 'E31', 27),
(1, 73, 'E32', 27),
(1, 74, 'E33', 18),
(1, 75, 'E34', 18),
(1, 76, 'E4', 0),
(1, 77, 'E41', 20),
(1, 78, 'E42', 18),
(1, 79, 'E43', 20),
(1, 80, 'E44', 20),
(1, 81, 'E45', 20),
(1, 82, 'E5', 0),
(1, 83, 'E51', 20),
(1, 84, 'E52', 20),
(1, 85, 'E53', 60),
(1, 86, 'E6', 0),
(1, 87, 'E61', 20),
(1, 88, 'E62', 0),
(1, 89, 'E63', 0),
(1, 90, 'E7', 0),
(1, 91, 'E71', 31),
(1, 92, 'E72', 27),
(1, 93, 'E73', 18),
(1, 94, 'E74', 0),
(1, 95, 'E8', 0),
(1, 96, 'E81', 20),
(1, 97, 'E82', 30),
(1, 98, 'E83', 50),
(1, 99, 'L8', 6),
(1, 99, 'S', 263),
(1, 100, 'G', 0),
(1, 101, 'G1', 0),
(1, 102, 'G11', 18),
(1, 103, 'G12', 17),
(1, 104, 'G13', 18),
(1, 105, 'G14', 17),
(1, 106, 'x', 103),
(1, 106, 'y', 173),
(1, 107, 'G3', 85),
(1, 108, 'G4', 90),
(1, 109, 'G5', 0),
(1, 110, 'G51', 18),
(1, 111, 'G52', 20),
(1, 112, 'G3', 0),
(1, 113, 'G54', 15),
(1, 114, 'R', 0),
(1, 115, 'R1', 0),
(1, 116, 'zeta1', 1),
(1, 117, 'zeta2', 3),
(1, 118, 'zeta3', 53),
(1, 119, 'R2', 0),
(1, 120, 'R21', 10),
(1, 121, 'R22', 0),
(1, 122, 'R23', 32),
(1, 123, 'R3', 0),
(1, 124, 'R31', 50),
(1, 125, 'R32', 50),
(1, 126, 'R4', 0),
(1, 127, 'R41', 150),
(1, 128, 'R42', 120),
(1, 129, 'R43', 120),
(1, 130, 'R5', 0),
(1, 131, 'R51', 15),
(1, 132, 'R52', 25),
(1, 133, 'R53', 25),
(1, 134, 'R54', 20),
(1, 135, 'R55', 15),
(1, 136, 'R6', 0),
(1, 137, 'R61', 40),
(1, 138, 'R62', 30),
(1, 139, 'R63', 0),
(1, 140, 'C', 0),
(1, 141, 'y', 4),
(1, 142, 'z0', 240),
(1, 142, 'zi', 240),
(1, 143, 'h', 20),
(1, 144, 'q0', 120),
(1, 144, 'qi', 200),
(1, 145, 'r', 3),
(1, 146, 'z', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `depName` varchar(256) NOT NULL,
  `depEmail` varchar(128) NOT NULL,
  `depPhone` varchar(16) NOT NULL,
  `depFax` varchar(16) NOT NULL,
  `depFoundationDate` varchar(32) NOT NULL,
  `depAddress` varchar(256) NOT NULL,
  `depLogo` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`facID`, `depID`, `depName`, `depEmail`, `depPhone`, `depFax`, `depFoundationDate`, `depAddress`, `depLogo`) VALUES
(1, 1, 'قسم هندسة الكيمياء', 'uofg@uofg.edu.sd', '00249000000000', '00249000000000', '157766400', 'السودان، مدني، النشيشيبة ص.ب. 20', NULL),
(1, 2, 'قسم هندسة الحاسوب', 'uofk@uofk.edu.sd', '00249000000000', '00249000000000', '-2145916800', 'السودان، الخرطوم', NULL),
(1, 3, 'قسم هندسة الالكترونيات', 'oiu@oiu.edu.sd', '00249000000000', '00249000000000', '1478044800', 'السودان، ,ودمدني', NULL),
(1, 4, 'قسم هندسة الأغذية', 'n@neelain.edu.sd', '+249 92 931 5732', '+249 92 931 5732', '725846400', '52nd St, Khartoum, Sudan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `evalelement`
--

CREATE TABLE `evalelement` (
  `efID` bigint(20) NOT NULL,
  `elementID` bigint(20) NOT NULL,
  `elementEquation` varchar(512) NOT NULL,
  `elementSymbol` varchar(4) NOT NULL,
  `elementTitle1` varchar(256) NOT NULL,
  `elementTitle2` varchar(256) NOT NULL,
  `elementTypicalDgree` int(11) NOT NULL,
  `elementWeight` double NOT NULL,
  `editValue` varchar(4) NOT NULL,
  `elementParent` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evalelement`
--

INSERT INTO `evalelement` (`efID`, `elementID`, `elementEquation`, `elementSymbol`, `elementTitle1`, `elementTitle2`, `elementTypicalDgree`, `elementWeight`, `editValue`, `elementParent`) VALUES
(1, 1, 'L+M+N', 'P', 'تقويم البرنامج الدراسي', 'تقويم البرنامج الدراسي', 100, 0.1, 'no', 0),
(1, 2, '0.4 * sum(L*weight) / sum(weight)', 'L', 'تقويم المنهج', 'تقويم المنهج', 40, 1, 'no', 1),
(1, 3, '([cra]/Typical)*100', 'L1', 'عدد الساعات المعتمدة للبرنامج', 'درجات الساعات المعتمدة للبرنامج (cr0) مقارنة مع البرنامج الانموذج (cr)', 190, 2, 'yes', 2),
(1, 4, '([ha]/Typical)*100', 'L2', 'عدد الساعات المعتمدة للمقررات الانسانية', 'درجات الساعات المعتمدة للمقررات الانسانية(h0) مقارنة مع البرنامج الانموذج (h)', 29, 1, 'yes', 2),
(1, 5, '([sa]/Typical)*100', 'L3', 'عدد الساعات المعتمدة للعلوم البحتة الاساسية', 'درجات الساعات المعتمدة للعلوم البحتة الاساسية(s0) مقارنة مع البرنامج الانموذج (s)', 57, 1, 'yes', 2),
(1, 6, '([esa]/Typical)*100', 'L4', 'عدد الساعات المعتمدة لمقررات العلوم الهندسية الأساسية', 'درجات الساعات المعتمدة للعلوم الهندسية الاساسية(es0) مقارنة مع البرنامج الانموذج (es)', 67, 1, 'yes', 2),
(1, 7, '([espa]/Typical)*100', 'L5', 'عدد الساعات المعتمدة لمقررات العلوم الهندسية التطبيقية', 'درجات الساعات المعتمدة للعلوم التخصصية التطبيقية(esp0) مقارنة مع البرنامج الانموذج (esp)', 66, 2, 'yes', 2),
(1, 8, '([prwa]/Typical)*100', 'L6', 'عدد ساعات الاتصال العملي (الورش والمختبرات والمتابعة)', 'درجات ساعات الاتصال العملي (الورش والمختبرات والمتابعة) (prw0) مقارنة مع البرنامج الانموذج (prw)', 2200, 2, 'yes', 2),
(1, 9, '([proja]/Typical)*100', 'L7', 'الساعات المعتمدة لمشروع التخرج', 'درجات الساعات المعتمدة لمشروع التخرج (proja) مقارنة مع البرنامج الانموذج (proj)', 6, 2, 'yes', 2),
(1, 10, '([tra]/Typical)*100', 'L8', 'عدد ساعات التدريب الميداني', 'درجات الساعات المعتمدة للتدريب الميداني (tr0) مقارنة مع البرنامج الانموذج (tr)', 10, 2, 'yes', 2),
(1, 11, '0.3 * sum(M*weight) / sum(weight)', 'M', 'تقويم الخطة الدراسية', 'تقويم الخطة الدراسية', 30, 1, 'no', 1),
(1, 12, '', 'M1', 'توصيف المقررات واشتمالها الأهداف والمخرجات', 'درجات توصيف المقررات بالمقارنة مع الانموذج', 100, 2, 'yes', 11),
(1, 13, '', 'M2', 'ترتيب طرح المقررات في الفصول الدراسية', 'درجات ترتيب طرح المقررات بالمقارنة مع الانموذج', 100, 1, 'yes', 11),
(1, 14, '', 'M3', 'توزيع المقررات على الفصول الدراسية', 'درجات توزيع المقررات على الفصول الدراسية مقارنة بالانموذج', 100, 1, 'yes', 11),
(1, 15, '', 'M4', 'مطابقة الجدول الدراسي للخطة الدراسية', 'درجات مطابقة الجدول الدراسي للخطة الدراسية', 100, 2, 'yes', 11),
(1, 16, '', 'M5', 'اشتمال الخطة لحلقات النقاش (السمنارات)', 'درجات حلقات النقاش (السمنارات)', 100, 1, 'yes', 11),
(1, 17, '', 'M6', 'مراجعة وتطوير المنهج دورياً', 'درجات المراجعة الدورية للمنهج', 100, 3, 'yes', 11),
(1, 18, '0.3 * sum(N*weight) / sum(weight)', 'N', 'تقويم شروط القبول للبرنامج', 'تقويم شروط القبول للبرنامج', 30, 1, 'no', 1),
(1, 19, '', 'N1', 'المساق في الشهادة الثانوية', 'درجات المساق في الشهادة الثانوية(n1). يساوي 100 إذا كان المساق علمي أو صناعي، صفر إذا كان غير ذلك.', 100, 3, 'yes', 18),
(1, 20, '([av]/75)*100', 'N2', 'أدنى درجة نجاح في الشهادة الثانوية للمقبولين للدفعات الخمسة الأخيرة', 'درجات النسبة المئوية في الشهادة للمقبولين في البرنامج (n2). حيث يمثل متوسط أدنى نسبة شهادة للمقبولين للخمس دفعات الأخيرة.', 100, 2, 'yes', 18),
(1, 21, 'sum(T*weight) / sum(weight)', 'T', 'تقويم هيئة التدريس ومعاونيهم', 'تقويم هيئة التدريس ومعاونيهم', 100, 0.2, 'no', 0),
(1, 22, '(20*[L1]/[S])*100', 'T1', 'العدد الكلي لأعضاء هيئة التدريس (L1) = (L1 نصف المتعاونين من داخل المؤسسة + ثلث المتعاونين من خاج المؤسسة)', 'درجات العدد الكلي لهيئة التدريس', 100, 3, 'yes', 21),
(1, 23, '(20*[L2])/(0.85*[S])*100', 'T2', 'عدد أعضاء هيئة التدريس المتفرغين', 'درجات أعضاء هيئة التدريس الدائمين. (عدد أعضاء هيئة التدريس لكل الوقت من درجة محاضر فما فوق L2)', 100, 5, 'yes', 21),
(1, 24, '(200*[L3])/([S])*100', 'T3', 'عدد أعضاء هيئة التدريس المتفرغين في درجة أستاذ', 'درجات الأساتذة (البروفيسور)', 100, 2, 'yes', 21),
(1, 25, '(150*[L4])/([S])*100', 'T4', 'عدد أعضاء هيئة التدريس المتفرغين في درجة أستاذ مشارك', 'درجات الأساتذة المشاركين', 100, 2, 'yes', 21),
(1, 26, '(100 *[L5])/([S])*100', 'T5', 'عدد أعضاء هيئة التدريس المتفرغين في درجة أستاذ مساعد', 'درجات الأساتذة المساعدين', 100, 2, 'yes', 21),
(1, 27, '(75*[L6])/([S])*100', 'T6', 'عدد أعضاء هيئة التدريس المتفرغين في درجة محاضر', 'درجات الأساتذة المحاضرين', 100, 1, 'yes', 21),
(1, 28, '[L7]/[L7a]*100', 'T7', 'عدد التقنيين في المختبرات والورش', 'درجات التقنيين بالمختبرات والورش', 100, 2, 'yes', 21),
(1, 29, 'sum(A*weight) / sum(weight)', 'A', 'تقويم وسائل ومعينات التدريس', 'تقويم وسائل ومعينات التدريس', 100, 0.15, 'no', 0),
(1, 30, '', 'A1', 'تجهيز القاعات', 'درجات تجهيز القاعات', 100, 2, 'yes', 29),
(1, 31, '', 'A2', 'تجهيز المراسم', 'درجات تجهيز المراسم', 100, 2, 'yes', 29),
(1, 32, '', 'A3', 'تجهيزات المختبرات', 'درجات تجهيزات المختبرات', 100, 3, 'yes', 29),
(1, 33, '', 'A4', 'تجهيزات المشاغل (الورش)', 'درجات تجهيزات الورش', 100, 3, 'yes', 29),
(1, 34, 'sum(A5)', 'A5', 'تجهيز المكتبات', 'درجات تجهيز المكتبات', 100, 4, 'no', 29),
(1, 35, '(10*[librarybooks]*50)/([noofstudents]*[noofcourses])', 'A51', 'عدد الكتب الدراسية والمراجع', 'الكتب الدراسية والمراجع', 50, 1, 'yes', 34),
(1, 36, '[journalsAvailable]/[numberofprogram]*20', 'A52', 'عدد الدوريات التخصصية', 'الدوريات التخصصية', 20, 1, 'yes', 34),
(1, 37, '', 'A53', 'سعة المكتبة الإلكترونية  (تقدير من 30 درجة)', 'توفر مكتبة الكترونية ووسائل تقنية', 30, 1, 'yes', 34),
(1, 38, 'sum(A6)', 'A6', 'توفر الوسائل السمعية والبصرية', 'درجاات توفر الوسائل السمعية والبصرية', 100, 2, 'no', 29),
(1, 39, '[numberavailable]/[numberofprograms]*80', 'A61', 'توفر جهاز عرض (projector) لكل شعبة', 'توفر جهاز عرض لكل شعبة', 80, 1, 'yes', 38),
(1, 40, '', 'A62', 'توفر شاشة (Screen) عرض (تقدير من 20 درجة)', 'توفر شاشة (Screen) عرض', 20, 1, 'yes', 38),
(1, 41, '15*[pc]*100/[S]', 'A7', 'عدد أجهزة الحاسوب منسوباً لعدد الطلاب المستخدمين لها', 'درجات تجهيزات الحاسوب', 100, 2, 'yes', 29),
(1, 42, '15*20*[ch]*100/[S]', 'A8', 'توفر شبكة للمعلومات منسوباً لعدد الطلاب المستخدمين لها', 'درجات توفر شبكة للمعلومات', 100, 1, 'yes', 29),
(1, 43, 'sum(F*weight) / sum(weight)', 'F', 'تقويم البنية التحتية', 'تقويم البنية التحتية', 100, 0.15, 'no', 0),
(1, 44, '(100*[ar41])/(1.25*[S])', 'F1', 'عدد ومساحة مكاتب هيئة التدريس', 'درجات مباني الادارة وهيئة التدريس', 100, 3, 'yes', 43),
(1, 45, '([ar42]*100)/(6*[S])', 'F2', 'أعداد وسعات ومساحات القاعات والمراسم والمختبرات والورش', 'درجات القاعات والمراسم والمختبرات والورش', 100, 6, 'yes', 43),
(1, 46, '([ar43]*100)/(0.4*[S])', 'F3', 'مساحة مبنى المكتبة وسعة الإجلاس فيها', 'درجات مبنى المكتبة', 100, 3, 'yes', 43),
(1, 47, '[ar44]*100/[S]', 'F4', 'مساحات مقاصف الأساتذة والطلاب والعاملين', 'درجات مقاصف الأساتذة والطلاب والعاملين', 100, 2, 'yes', 43),
(1, 48, '([ar45]*100)/(4*[S])', 'F5', 'مساحات الميادين والممرات', 'درجات الميادين والممرات', 100, 2, 'yes', 43),
(1, 49, '([ar46]*100)/(0.3*[S])', 'F6', 'مساحة المصلى', 'درجات المصلى', 100, 3, 'yes', 43),
(1, 50, '([ar47]*100)*(0.125*[S])', 'F7', 'مساحات دورات المياه', 'درجات دورات المياه', 100, 2, 'yes', 43),
(1, 51, 'sum(F8)', 'F8', 'الموقع والبيئة المحيطة', 'درجات الموقع والبيئة المحيطة', 100, 2, 'no', 43),
(1, 52, '', 'F81', 'البعد عن الضوضاء', 'البعد عن الضوضاء', 40, 1, 'yes', 51),
(1, 53, '', 'F82', 'سلامة البيئة المحيطة', 'سلامة البيئة المحيطة', 40, 1, 'yes', 51),
(1, 54, '', 'F83', 'سهولة المواصلات', 'سهولة المواصلات', 100, 1, 'yes', 51),
(1, 55, 'sum(F9)', 'F9', 'مقومات الأنشطة اللاصفية', 'درجات مقومات الأنشطة اللاصفة', 100, 2, 'no', 43),
(1, 56, '', 'F91', 'توفر ملاعب قدم وسلة. (تقدير من 40 درجة)', 'توفر ملاعب قدم وسلة', 40, 1, 'yes', 55),
(1, 57, '', 'F92', 'توفر مقومات الأنشطة الرياضية الأخرى. (تقدير من 30 درجة)', 'توفر مقومات الأنشطة الرياضية الأخرى', 30, 1, 'yes', 55),
(1, 58, '', 'F93', 'توفر مسرح ومقومات أنشطة ثقافية. (تقدير من 30 درجة)', 'توفر مسرح ومقومات أنشطة ثقافية', 30, 1, 'yes', 55),
(1, 59, 'sum(E*weight) / sum(weight)', 'E', 'تقويم أداء الطلاب', 'تقويم أداء الطلاب', 100, 0.15, 'no', 0),
(1, 60, 'sum(E1)', 'E1', 'اللائحة العامة للبرنامج (شاملة القبول والتسجيل)', 'درجات لائحة تنفيذ البرامج', 100, 2, 'no', 59),
(1, 61, '', 'E11', 'توفر لائحة مفصلة ومكتوبة', 'توفر لائحة مفصلة ومكتوبة', 20, 1, 'yes', 60),
(1, 62, '', 'E12', 'شمولية اللائحة لكافة جوانب تنفيذ البرنامج', 'شمولية اللائحة لكافة جوانب تنفيذ البرنامج', 30, 1, 'yes', 60),
(1, 63, '', 'E13', 'تطبيقها بواسطة جميع أعضاء هيئة التدريس', 'تطبيقها بواسطة جميع أعضاء هيئة التدريس', 30, 1, 'yes', 60),
(1, 64, '', 'E14', 'تطبيقها بواسطة مجلس القسم والكلية', 'تقويم تطبيقها بواسطة مجلس القسم والكلية', 20, 1, 'yes', 60),
(1, 65, 'sum(E2)', 'E2', 'درجات لائحة الامتحانات', 'درجات لائحة الامتحانات', 100, 2, 'no', 59),
(1, 66, '', 'E21', 'توفر لائحة لضبط الامتحانات واعداد الناتئج', 'توفر لائحة لضبط الامتحانات واعداد الناتئج', 20, 1, 'yes', 65),
(1, 67, '', 'E22', 'شمولية اللائحة لكافة جوانب اجراءات الامتحانات', 'شمولية اللائحة لكافة جوانب اجراءات الامتحانات', 30, 1, 'yes', 65),
(1, 68, '', 'E23', 'الالتزام بتطبيق كافة الإجراءات', 'الالتزام بتطبيق كافة الإجراءات', 20, 1, 'yes', 65),
(1, 69, '', 'E24', 'الالتزام بتطبييق قواعد اعداد النتائج', 'الالتزام بتطبييق قواعد اعداد النتائج', 20, 1, 'yes', 65),
(1, 70, '', 'E25', 'تقويم تطبيقها بواسطة مجلس القسم والكلية', 'تقويم تطبيقها بواسطة مجلس القسم والكلية', 10, 1, 'yes', 65),
(1, 71, 'sum(E3)', 'E3', 'التقويم المستمر خلال الفصل الدراسي', 'التقويم المستمر خلال الفصل الدراسي', 100, 1, 'no', 59),
(1, 72, '', 'E31', 'توفير نماذج من التمارين العملية', 'تقويم نماذج من التمارين الفصلية', 30, 1, 'yes', 71),
(1, 73, '', 'E32', 'توفير نماذج من تقارير المختبرات للطلاب', 'تقويم نماذج من تقارير المختبرات للطلاب', 30, 1, 'yes', 71),
(1, 74, '', 'E33', 'توفير نماذج من الاختبارات والواجبات', 'تقويم نماذج من الاختبارات المرحلية والواجبات', 20, 1, 'yes', 71),
(1, 75, '', 'E34', 'تقويم تقدير الدرجات للنماذج أعلاه', 'تقويم تقدير الدرجات للنماذج أعلاه', 20, 1, 'yes', 71),
(1, 76, 'sum(E4)', 'E4', 'لائحة منح الإجازة الجامعية', 'درجات لائحة منح الاجازة العلمية', 100, 2, 'yes', 59),
(1, 77, '', 'E41', 'توفر لائحة تحدد تقدير الأداء وأسس منح الإجازة', 'توفر لائحة تحدد تقدير الأداء وأسس منح الدرجة', 20, 1, 'yes', 76),
(1, 78, '', 'E42', 'مدى الالتزام بتطبيق قواعد تقدير أداء الطلاب', 'الالتزام بتطبيق قواعد تقدير أداء الطلاب', 20, 1, 'yes', 76),
(1, 79, '', 'E43', 'مدى الالتزام بتطبيق أسس منح الإجازة', 'الالتزام بتطبيق اسس منح الدرجة', 20, 1, 'yes', 76),
(1, 80, '', 'E44', 'تقويم مستوى الأداء العام للطلاب في الامتحانات', 'تقويم مستوى الأداء العام للطلاب في الامتحانات', 20, 1, 'yes', 76),
(1, 81, '', 'E45', 'مدى تطبيق القواعد والأسس بواسطة المجالس', 'تقويم تطبيق القواعد والأسس بواسطة المجالس', 20, 1, 'yes', 76),
(1, 82, 'sum(E5)', 'E5', 'محاضر مجالس الممتحنين', 'درجات محاضر مجالس الممتحنين', 100, 1, 'no', 59),
(1, 83, '', 'E51', 'توفير نماذج لمحاضر موثقة لمجالس الممتحنين', 'الالتزام باعداد محاضر موثقة لمجالس الممتحنين', 20, 1, 'yes', 82),
(1, 84, '', 'E52', 'اطلاع مجلس الكلية على هذه المحاضر', 'اطلاع مجلس الكلية على هذه المحاضر', 20, 1, 'yes', 82),
(1, 85, '', 'E53', 'الاطلاع على نماذج من المحضر بواسطة لجنة التقويم', 'تقويم نماذج من المحضر بواسطة لجنة التقويم', 60, 1, 'yes', 82),
(1, 86, 'sum(E6)', 'E6', 'تقارير الممتحنين الخارجيين', 'درجات تقارير الممتحنين الخارجيين', 100, 1, 'no', 59),
(1, 87, '', 'E61', 'الالتزام بتعيين ممتحنين خارجيين وتوفر تقارير منهم', 'الالتزام بتعيين ممتحنين خارجيين وتوفر تقارير منهم', 20, 1, 'yes', 86),
(1, 88, '', 'E62', 'اطلاع مجلس الكلية ومجالس الأقسام على التقارير', 'تقويم مجلس الكلية ومجالس الأقسام لهذه التقارير', 30, 1, 'yes', 86),
(1, 89, '', 'E63', 'الاطلاع على نماذج من المحضر بواسطة لجنة التقويم', 'تقويم نماذج من المحضر بواسطة لجنة التقويم', 50, 1, 'yes', 86),
(1, 90, 'sum(E7)', 'E7', 'تطبيق مواصفات الامتحان الجيد', 'درجات تطبيق مواصفات الامتحان الجيد', 100, 4, 'no', 59),
(1, 91, '', 'E71', 'شمولية الأسئلة للمقرر واستيفاء مخرجات التعلم', 'شمولية الأسئلة للمقرر واستيفاء مخرجات التعلم', 35, 1, 'yes', 90),
(1, 92, '', 'E72', 'تفاوت الأسئلة في درجات  الصعوبة لتمييز القدرات', 'تفاوت الأسئلة في درجات  الصعوبة لتمييز القدرات', 30, 1, 'yes', 90),
(1, 93, '', 'E73', 'مراجعة الامتحان بواسطة أستاذ ثان', 'مراجعة الامتحان بواسطة أستاذ ثان', 20, 1, 'yes', 90),
(1, 94, '', 'E74', 'اطلاع الممتحن الخارجي على الامتحان قبل انعقاده', 'اطلاع الممتحن الخارجي على الامتحان قبل انعقاده', 15, 1, 'yes', 90),
(1, 95, 'sum(E8)', 'E8', 'تقارير التدريب العملي', 'درجات تقارير التدريب العملي', 100, 2, 'no', 59),
(1, 96, '', 'E81', 'توفر تقارير من مواقع العمل عن التدريب الميداني', 'توفر تقارير من مواقع العمل عن التدريب الميداني', 20, 1, 'yes', 95),
(1, 97, '', 'E82', 'اطلاع مجلس الكلية ومجالس الأقسام لهذه التقارير', 'اطلاع مجلس الكلية ومجالس الأقسام لهذه التقارير', 30, 1, 'yes', 95),
(1, 98, '', 'E83', 'تقويم نماذج من التقارير بواسطة لجنة التقويم', 'تقويم نماذج من التقارير بواسطة لجنة التقويم', 50, 1, 'yes', 95),
(1, 99, '(25*[L8]*100)/[S]', 'T8', 'عدد القوة المساعدة', 'درجات القوة المساعدة', 100, 1, 'yes', 21),
(1, 100, 'sum(G*weight) / sum(weight)', 'G', 'تقويم المخرجات بكالريوس الهندسة', 'تقويم المخرجات بكالريوس الهندسة', 0, 0.1, 'no', 0),
(1, 101, 'sum(G1)', 'G1', 'تطوير الخريج لنفسه', 'درجات تطوير الخريج لنفسه', 100, 5, 'no', 100),
(1, 102, '', 'G11', 'مدى التدرج الوظيفي للخريج', 'مدى التدرج الوظيفي للخريج', 25, 1, 'yes', 101),
(1, 103, '', 'G12', 'مدى التأهيل الاضافي للخريج في مجاله', 'مدى التأهيل الاضافي للخريج في مجاله', 25, 1, 'yes', 101),
(1, 104, '', 'G13', 'مدى التدرج المهني للخريج', 'مدى التدرج المهني للخريج', 25, 1, 'yes', 101),
(1, 105, '', 'G14', 'مدى حصول الخريج على شهادات تقديرية وإشادات بالإنجازات التي حققها في مجال عمله', 'مدى حصول الخريج على شهادات تقديرية وإشادات بالإنجازات التي حققها في مجال عمله', 25, 1, 'yes', 101),
(1, 106, '[x]*100/[y]', 'G2', 'درجات انخراط الخريج في مجال تخصصه', 'درجات انخراط الخريج في مجال تخصصه', 100, 3, 'yes', 100),
(1, 107, '', 'G3', 'رضى الخريج عن نفسه في مجال عمله', 'درجات رضى الخريج عن نفسه في مجاله', 100, 2, 'yes', 100),
(1, 108, '', 'G4', 'رضى المخدم عن الخريج', 'درجات رضى المخدم عن الخريج', 100, 3, 'yes', 100),
(1, 109, 'sum(G5)', 'G5', 'مدى ارتباط الكلية بالخريج', 'درجات مدى ارتباط الكلية بالخريج', 100, 3, 'no', 100),
(1, 110, '', 'G51', 'توفر مكتب للخريجين  (تقدير من 20 درجة)', 'توفر مكتب للخريجين', 20, 1, 'yes', 109),
(1, 111, '', 'G52', 'وجود سجلات بعناوين الخريجين (تقدير من 30 درجة)', 'وجود سجلات بعناوين الخريجين', 30, 1, 'yes', 109),
(1, 112, '', 'G3', 'وجود نشرة دورية توزع على الخريجين  (تقدير من 30 درجة)', 'وجود نشرة دورية توزع على الخريجين', 30, 1, 'yes', 109),
(1, 113, '', 'G54', 'تمثيل الخريجين في مجلس الادارة (تقدير من 20 درجة)', 'تمثيل الخريجين في مجلس الادارة', 20, 1, 'yes', 109),
(1, 114, 'sum(R*weight) / sum(weight)', 'R', 'الدراسات العليا والمشاركات العلمية والمهنية', 'تقويم الدراسات العليا والمشاركات العلمية والمهنية', 100, 0.1, 'yes', 0),
(1, 115, 'sum(R1)', 'R1', 'الدراسات العليا', 'درجات الدراسات العليا', 100, 2, 'no', 114),
(1, 116, '20*[zeta1]', 'R11', 'وجود برامج ماجستير', 'وجود برامج ماجستير', 20, 1, 'yes', 115),
(1, 117, '([zeta2]<=3) ?(40*[zeta2]/3):40*(2-[zeta2]/3)', 'R12', 'عدد دارسي الدكتوراة / عدد الأساتذة فوق درجة أستاذ مساعد', 'عدد دارسي الدكتوراة / عدد الأساتذة فوق درجة أستاذ مساعد', 40, 1, 'yes', 115),
(1, 118, '([zeta3]<=5) ?(8*[zeta3]):40', 'R13', 'عدد البحوث الجارية لحل مشكلات واقعية', 'البحوث الجارية لحل مشكلات واقعية', 40, 1, 'yes', 115),
(1, 119, 'sum(R2)', 'R2', 'النشر العلمي للأساتذة', 'درجات النشر العلمي للأساتذة', 100, 2, 'yes', 114),
(1, 120, '', 'R21', 'تأليف كتب منهجية', 'درجات تأليف كتب منهجية', 25, 1, 'yes', 119),
(1, 121, '', 'R22', 'ترجمة كتب منهجية', 'درجات ترجمة كتب منهجية', 25, 1, 'yes', 119),
(1, 122, '', 'R23', 'نشر أوراق علمية في دوريات أو مؤتمرات محكمة', 'درجات نشر أوراق علمية في دوريات أو مؤتمرات محكمة', 50, 1, 'yes', 119),
(1, 123, 'sum(R3)', 'R3', 'المؤتمرات العلمية', 'درجات المؤتمرات العلمية', 100, 1, 'yes', 114),
(1, 124, '', 'R31', 'انعقاد مؤتمرات علمية محكمة في مجال البرنامج', 'درجات انعقاد مؤتمرات علمية محكمة في مجال البرنامج', 50, 1, 'yes', 123),
(1, 125, '', 'R32', 'مشاركة الاساتذة في مؤتمرات علمية محكمة', 'درجات مشاركة الاساتذة في مؤتمرات علمية محكمة', 50, 1, 'yes', 123),
(1, 126, 'sum(R4)', 'R4', 'توطيد العلاقة بالمؤسسات العلمية', 'درجات توطيد العلاقة بالمؤسسات العلمية', 100, 1, 'no', 114),
(1, 127, '', 'R41', 'اتفاقيات تبادل خبرة مع جامعات عالمية', 'درجات اتفاقيات تبادل خبرة مع جامعات عالمية', 30, 1, 'yes', 126),
(1, 128, '', 'R42', 'تبادل زيارات علمية لطلاب الدراسات العليا', 'درجات تبادل زيارات علمية لطلاب الدراسات العليا', 30, 1, 'yes', 126),
(1, 129, '', 'R43', 'تبادل أساتذة للمشاركة في البحث العلمي أو التدريس', 'درجات تبادل أساتذة للمشاركة في البحث العلمي أو التدريس', 40, 1, 'yes', 126),
(1, 130, 'sum(R5)', 'R5', 'توطيد العلاقة بالمؤسسات الصناعية', 'درجات توطيد العلاقة بالمؤسسات الصناعية', 100, 2, 'no', 114),
(1, 131, '', 'R51', 'قيام اتفاقيات مع مؤسسات صناعية', 'قيام اتفاقيات مع مؤسسات صناعية', 15, 1, 'yes', 130),
(1, 132, '', 'R52', 'مشركة الأساتذة في تقديم استشارات فنية للمؤسسات الصناعية', 'مشركة الأساتذة في تقديم استشارات فنية للمؤسسات الصناعية', 25, 1, 'yes', 130),
(1, 133, '', 'R53', 'مشاركة العاملين في الصناعة في التدريس وتقويم المنهج', 'مشاركة العاملين في الصناعة في التدريس وتقويم المنهج', 25, 1, 'yes', 130),
(1, 134, '', 'R54', 'تمثيل أهل الصناعة في مجلس الادارة', 'تمثيل أهل الصناعة في مجلس الادارة', 20, 1, 'yes', 130),
(1, 135, '', 'R55', 'توفير فرص التدريس والزيارات العلمية', 'توفير فرص التدريس والزيارات العلمية', 15, 1, 'yes', 130),
(1, 136, 'sum(R6)', 'R6', 'أنشطة الطلاب العلمية اللاصفية', 'أنشطة الطلاب العلمية اللاصفية', 100, 1, 'no', 114),
(1, 137, '', 'R61', 'قيام جمعيات علمية في مجال البرنامج', 'قيام جمعيات علمية في مجال البرنامج', 40, 1, 'yes', 136),
(1, 138, '', 'R62', 'المشاركة في مسابقات علمية', 'المشاركة في مسابقات علمية', 30, 1, 'yes', 136),
(1, 139, '', 'R63', 'الحصول على جوائز تقديرية لابداع علمي', 'الحصول على جوائز تقديرية لابداع علمي', 30, 1, 'yes', 136),
(1, 140, 'sum(C*weight) / sum(weight)', 'C', 'المساهمة في خدمة المجتمع', 'المساهمة في خدمة المجتمع', 100, 0.05, 'no', 0),
(1, 141, '[y]*100/5', 'C1', 'الدورات القصيرة للمجتمع المحيط بالكلية', 'درجات الدورات القصيرة للمجتمع المحيط بالكلية', 100, 1, 'yes', 140),
(1, 142, '[z0]*100/[zi]', 'C2', 'المستفيدين من الدورات القصيرة', 'درجات المستفيدين من الدورات القصيرة', 100, 1, 'yes', 140),
(1, 143, '[h]*100/5', 'C3', 'تقديم الحلول لمشكلات المجتمع', 'درجات تقديم الحلول لمشكلات المجتمع', 100, 2, 'yes', 140),
(1, 144, '(5*[q0]*100)/[qi]', 'C4', 'المشاركة في لجان خدمة المجتمع', 'درجات المشاركة في لجان خدمة المجتمع', 100, 2, 'yes', 140),
(1, 145, '[r]*100/10', 'C6', 'الندوات والمحاضرات والمقالات الصحفية', 'درجات الندوات والمحاضرات والمقالات الصحفية', 100, 1, 'yes', 140),
(1, 146, '[z]*100/2', 'C7', 'الحصول على جوائز تقديرية واشادات', 'درجات الحصول على جوائز تقديرية واشادات', 100, 1, 'yes', 140);

-- --------------------------------------------------------

--
-- Table structure for table `evalform`
--

CREATE TABLE `evalform` (
  `efID` bigint(20) NOT NULL,
  `efName` varchar(256) NOT NULL,
  `efDesc` text NOT NULL,
  `efSector` enum('Engineering','Medical','Agricultural','Economical','Industrial') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evalform`
--

INSERT INTO `evalform` (`efID`, `efName`, `efDesc`, `efSector`) VALUES
(1, 'استمارة تقويم كليات الهندسة', 'استمارة تقويم كليات الهندسة', 'Engineering'),
(2, 'استمارة تقويم كليات الاقتصاد', 'استمارة تقويم كليات الاقتصاد', 'Economical');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE `evaluation` (
  `progID` bigint(20) NOT NULL,
  `efID` bigint(20) NOT NULL,
  `evalID` bigint(20) NOT NULL,
  `evalDate` int(11) NOT NULL,
  `dataGetheringDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluation`
--

INSERT INTO `evaluation` (`progID`, `efID`, `evalID`, `evalDate`, `dataGetheringDate`) VALUES
(5, 1, 1, 1475280000, 1475280000),
(2, 1, 2, 1477267200, 1477267200),
(5, 1, 6, 1481500800, 1481500800),
(5, 1, 7, 1483484400, 1483484400);

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `instID` bigint(20) NOT NULL,
  `facID` bigint(20) NOT NULL,
  `facName` varchar(256) NOT NULL,
  `facEmail` varchar(128) NOT NULL,
  `facPhone` varchar(16) NOT NULL,
  `facFax` varchar(16) NOT NULL,
  `facFoundationDate` varchar(32) NOT NULL,
  `facAddress` varchar(256) NOT NULL,
  `facLogo` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`instID`, `facID`, `facName`, `facEmail`, `facPhone`, `facFax`, `facFoundationDate`, `facAddress`, `facLogo`) VALUES
(1, 1, 'كلية الهندسة والتكنولوجيا', 'eng@uofg.edu.sd', '00249000000000', '00249000000000', '252460800', 'السودان، ودمدني، النشيشيبة ص.ب. 20', NULL),
(1, 2, 'كلية الاقتصاد والتنمية الريفية', 'ferd@uofg.edu.sd', '00249000000000', '00249000000000', '1478217600', 'السودان، النشيشيبة', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `institution`
--

CREATE TABLE `institution` (
  `instID` bigint(20) NOT NULL,
  `instName` varchar(256) NOT NULL,
  `instEmail` varchar(128) NOT NULL,
  `instPhone` varchar(16) NOT NULL,
  `instFax` varchar(16) NOT NULL,
  `instFoundationDate` varchar(32) NOT NULL,
  `instAddress` varchar(256) NOT NULL,
  `instLogo` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institution`
--

INSERT INTO `institution` (`instID`, `instName`, `instEmail`, `instPhone`, `instFax`, `instFoundationDate`, `instAddress`, `instLogo`) VALUES
(1, 'جامعة الجزيرة', 'uofg@uofg.edu.sd', '00249000000000', '00249000000000', '157766400', 'السودان، مدني، النشيشيبة ص.ب. 20', NULL),
(2, 'جامعة الخرطوم', 'uofk@uofk.edu.sd', '00249000000000', '00249000000000', '-2145916800', 'السودان، الخرطوم', NULL),
(3, 'جامعة أمدرمان الاسلامية', 'oiu@oiu.edu.sd', '00249000000000', '00249000000000', '1478044800', 'السودان، الخرطوم', NULL),
(4, 'جامعة النيلين', 'n@neelain.edu.sd', '+249 92 931 5732', '+249 92 931 5732', '725846400', '52nd St, Khartoum, Sudan', NULL),
(11, 'جامعة القضارف', 'g@g.g', '0900000000000', '0900000000000', '1478908800', 'السودان، القضارف', NULL),
(12, 'جامعة كرري', 'yasir@karary.edu.sd', '00249921234732', '00249921234732', '1996-01-01', 'السودان الخرطوم امدرمان شارع الوادي\r\n    P.O.Box 12304', 'KararyLogo.png');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `depID` bigint(20) NOT NULL,
  `progID` bigint(20) NOT NULL,
  `progLastReviewDate` varchar(32) NOT NULL,
  `progName` varchar(256) NOT NULL,
  `progStartDate` varchar(32) NOT NULL,
  `progType` varchar(32) NOT NULL,
  `progPref` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`depID`, `progID`, `progLastReviewDate`, `progName`, `progStartDate`, `progType`, `progPref`) VALUES
(10, 2, '1477267200', 'بكلاريوس المحاسبة', '1477267200', 'بكلاريوس', ''),
(3, 3, '1478044800', 'بكلاريوس العلوم في هندسة الاتصالات', '1478044800', 'بكلاريوس', ''),
(3, 4, '1478044800', 'بكلاريوس العلوم في هندسة الاجهزة الطبية', '1478044800', 'بكلاريوس', ''),
(1, 5, '1420329600', 'بكلاريوس العلوم في هندسة الكيمياء', '252460800', 'بكلاريوس', '<p>نبذة عن البرنامج</p>');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `staffID` bigint(20) NOT NULL,
  `pubID` bigint(20) NOT NULL,
  `pubTitle` varchar(256) NOT NULL,
  `pubType` varchar(32) NOT NULL,
  `pubDate` varchar(32) NOT NULL,
  `pubAbstract` text NOT NULL,
  `pubJournal` varchar(256) NOT NULL,
  `pubLink` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`staffID`, `pubID`, `pubTitle`, `pubType`, `pubDate`, `pubAbstract`, `pubJournal`, `pubLink`) VALUES
(1, 1, '1 test', '1', '1482966000', 'test', 'uofg', 'journals.uofg.edu.sd'),
(1, 2, 'test 2', '0', '1482966000', 'lskdgf', 'dlfgh', 'journals.uofg.edu.sd'),
(3, 3, 'test', '0', '1483311600', 'abstract', 'karary', 'karary.edu.sd');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `qID` tinyint(4) NOT NULL,
  `qName` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`qID`, `qName`) VALUES
(1, 'Ph.D.'),
(2, 'M.Sc.'),
(3, 'B.Sc.');

-- --------------------------------------------------------

--
-- Table structure for table `scientificparticipation`
--

CREATE TABLE `scientificparticipation` (
  `spID` bigint(20) NOT NULL,
  `spTitle` varchar(128) NOT NULL,
  `spType` enum('PhD Proposal','Master Thesis','Scientific Participation','financed Research','Scientific Paper','Conferance','Book') NOT NULL,
  `spPublisher` varchar(128) NOT NULL,
  `spDate` varchar(16) NOT NULL,
  `spLink` varchar(128) NOT NULL,
  `spDetails` text NOT NULL,
  `staffID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scientificparticipation`
--

INSERT INTO `scientificparticipation` (`spID`, `spTitle`, `spType`, `spPublisher`, `spDate`, `spLink`, `spDetails`, `staffID`) VALUES
(1, 'test', 'PhD Proposal', 'test', '2017-12-21', 'link', 'test', 1),
(2, 'test', 'PhD Proposal', 'test', '2017-12-21', 'test', 'test', 1),
(3, 'test', 'PhD Proposal', 'test', '2017-12-21', 'test', 'test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `specification`
--

CREATE TABLE `specification` (
  `sID` tinyint(4) NOT NULL,
  `sName` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specification`
--

INSERT INTO `specification` (`sID`, `sName`) VALUES
(1, 'engineering');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `instID` bigint(20) NOT NULL,
  `facID` bigint(20) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `staffID` bigint(20) NOT NULL,
  `staffName` varchar(128) NOT NULL,
  `staffGender` enum('ذكر','أنثى','') DEFAULT NULL,
  `staffBirthDate` varchar(32) NOT NULL,
  `staffNationalID` char(11) NOT NULL,
  `staffNationality` varchar(16) DEFAULT NULL,
  `staffPermanentAddress` varchar(256) NOT NULL,
  `staffPhone` varchar(16) NOT NULL,
  `staffMobile` varchar(16) NOT NULL,
  `staffHigherQualification` enum('دكتوراة الفلسفة','ماجستير العلوم','الدبلوم العالي','بكلاريوس العلوم','الدبلوم التقني') NOT NULL,
  `staffDegreeDate` varchar(32) NOT NULL,
  `staffResearchTitle` varchar(256) NOT NULL,
  `staffSpec` varchar(64) NOT NULL,
  `staffGenrSpec` varchar(64) NOT NULL,
  `staffPosition` enum('أستاذ كرسي','أستاذ','أستاذ مشارك','أستاذ مساعد','محاضر','مساعد تدريس','كبير مدرسين','مدرس','مساعد مدرس') NOT NULL,
  `staffEmail` varchar(128) NOT NULL,
  `staffHireDate` varchar(32) NOT NULL,
  `staffStatus` enum('على رأس العمل','دوام جزئي','منتدب','معار','مبتعث','متعاقد بالخارج','أجازة بدون مرتب','معاش','اخرى') NOT NULL,
  `staffStatusStartDate` varchar(32) NOT NULL,
  `staffStatusEndDate` varchar(32) NOT NULL,
  `staffCurrentAddress` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`instID`, `facID`, `depID`, `staffID`, `staffName`, `staffGender`, `staffBirthDate`, `staffNationalID`, `staffNationality`, `staffPermanentAddress`, `staffPhone`, `staffMobile`, `staffHigherQualification`, `staffDegreeDate`, `staffResearchTitle`, `staffSpec`, `staffGenrSpec`, `staffPosition`, `staffEmail`, `staffHireDate`, `staffStatus`, `staffStatusStartDate`, `staffStatusEndDate`, `staffCurrentAddress`) VALUES
(1, 1, 1, 1, 'محمد-أبوإدريس-الطريفي-يوسف', 'ذكر', '1990-07-24', '1234567890', 'سوداني', 'السودان، الجزيرة، مهلة', '0114220702', '0114220702', 'دكتوراة الفلسفة', '2005-10-21', 'Securing Gas Payment using R.Q. Cod.', 'هندسة الحاسوب والشبكات', 'هندسة الحاسوب والشبكات', 'مساعد تدريس', 'aboidrees@uofg.edu.sd', '2016-07-14', 'على رأس العمل', '', '', 'السودان، الجزيرة، ودمدني'),
(0, 0, 1, 2, 'مجدي بكر محمود امين', NULL, '', '', NULL, '', '', '', '', '', '', '', '', 'أستاذ مشارك', '', '957139200', '', '', '', ''),
(0, 1, 1, 3, 'test', NULL, '1483398000', '', NULL, 'sdf', '0114220702', '0114220702', '', '1367618400', 'fgl;kbhdl', 'd;''lgk', 'dkfml', 'أستاذ', 'aboidrees2@uofg.edu.sd', '1483398000', '', '1483398000', '', 'xfmv');

-- --------------------------------------------------------

--
-- Table structure for table `staffbook`
--

CREATE TABLE `staffbook` (
  `staffID` bigint(20) NOT NULL,
  `bookID` bigint(20) NOT NULL,
  `bookTitle` varchar(128) NOT NULL,
  `bookPubDate` varchar(32) NOT NULL,
  `bookPublisher` varchar(64) NOT NULL,
  `bookOtherInfo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staffbook`
--

INSERT INTO `staffbook` (`staffID`, `bookID`, `bookTitle`, `bookPubDate`, `bookPublisher`, `bookOtherInfo`) VALUES
(1, 1, 'كتاب 1', '1483398000', 'كتاب', 'كتاب'),
(3, 2, 'XFK', '1483398000', 'dfkm', 'dfl;b');

-- --------------------------------------------------------

--
-- Table structure for table `stafftrainning`
--

CREATE TABLE `stafftrainning` (
  `staffID` bigint(20) NOT NULL,
  `tCode` varchar(32) NOT NULL,
  `tTitle` varchar(128) NOT NULL,
  `tContactHours` varchar(4) NOT NULL,
  `tObjectives` text NOT NULL,
  `tPrerequisites` varchar(128) NOT NULL,
  `tAttendence` text NOT NULL,
  `tOutline` text NOT NULL,
  `tLearninigMethod` varchar(64) NOT NULL,
  `tRecommResource` varchar(256) NOT NULL,
  `tAssesstment` varchar(128) NOT NULL,
  `tValidation` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `instID` int(11) NOT NULL,
  `facID` int(11) NOT NULL,
  `depID` bigint(20) NOT NULL,
  `progID` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `userMail` varchar(128) NOT NULL,
  `userName` varchar(128) NOT NULL,
  `userPass` varchar(512) NOT NULL,
  `userPhone` varchar(16) NOT NULL,
  `userStatus` enum('not active','active') NOT NULL DEFAULT 'not active',
  `userGroup` enum('admin','user') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`instID`, `facID`, `depID`, `progID`, `userID`, `userMail`, `userName`, `userPass`, `userPhone`, `userStatus`, `userGroup`) VALUES
(0, 0, 0, 0, 1, 'm.alshf3ee@gmail.com', 'Aboidrees', 'f54bede3510ee4cfb4266dbdedcb9292c400fcc65ce8d250bf6b4edaf4d313ea466c10413e5b0008040493d3bf836e0502e6edbfc5dfa4af5b8c0f3045713596', '0114220702', 'active', 'admin'),
(1, 1, 1, 5, 2, 'aboidrees@gmail.com', 'muhammad', 'f54bede3510ee4cfb4266dbdedcb9292c400fcc65ce8d250bf6b4edaf4d313ea466c10413e5b0008040493d3bf836e0502e6edbfc5dfa4af5b8c0f3045713596', '0923411746', 'not active', 'user'),
(1, 3, 0, 0, 3, 'm.alshf3ee2@gmail.com', 'uofg', 'f54bede3510ee4cfb4266dbdedcb9292c400fcc65ce8d250bf6b4edaf4d313ea466c10413e5b0008040493d3bf836e0502e6edbfc5dfa4af5b8c0f3045713596', '923411746', 'active', 'admin'),
(12, 0, 0, 0, 4, 'yasir@karary.edu.sd', 'Karary University', 'bc2437b0a2d54c5d8a433ef01ede4efb0069bcd2e97edc37098ff68add1f19aa33f06d7e53bf76a4dda6ac230b8ca19ed2d50ccd96903fef0c537e62e449050a', '0123644440', 'active', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE `user_session` (
  `ip_address` varchar(64) NOT NULL,
  `session_id` varchar(512) NOT NULL,
  `last_activity` varchar(512) NOT NULL,
  `user_agent` text NOT NULL,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`ip_address`, `session_id`, `last_activity`, `user_agent`, `user_data`) VALUES
('41.67.35.52', '69613aa662f90653aec63f39efae61c4', '1511359237', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.89 Chrome/62.0.3202.89', ''),
('41.67.35.66', 'c4dee17078f3ae250fd0a4b218458fc3', '1512325789', 'Mozilla/5.0', ''),
('41.67.35.66', 'e0d1009dd0d01464711655ec3f2a88bd', '1512325789', 'Mozilla/5.0', ''),
('154.98.54.88', '662f1cf694fa119403cf1ce6d6052c37', '1512347342', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:"userID";i:1;s:8:"loggedIn";b:1;s:9:"userGroup";s:5:"admin";}'),
('41.67.35.60', 'b4d14c8161ef11556366292decec8cea', '1513374473', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 'a:3:{s:6:"userID";i:1;s:8:"loggedIn";b:1;s:9:"userGroup";s:5:"admin";}'),
('127.0.0.1', '4c7f0a54294e7a55b42efe3515a4797e', '1513834808', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:"userID";i:1;s:8:"loggedIn";b:1;s:9:"userGroup";s:5:"admin";}'),
('127.0.0.1', '34fb376621b1b37ec9f7d4316d8f7291', '1513839067', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 'a:3:{s:6:"userID";i:1;s:8:"loggedIn";b:1;s:9:"userGroup";s:5:"admin";}'),
('127.0.0.1', 'b601e1f886e46d1607b58ae6c7860da1', '1513837890', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', ''),
('127.0.0.1', '7b298fb6121909c7150f3e85f0fc0e63', '1513837890', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assistancestaff`
--
ALTER TABLE `assistancestaff`
  ADD PRIMARY KEY (`aStaffID`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`cCode`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`evalID`,`elementID`,`varName`),
  ADD KEY `elementID` (`elementID`,`evalID`,`varName`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`depID`),
  ADD KEY `facID` (`facID`);

--
-- Indexes for table `evalelement`
--
ALTER TABLE `evalelement`
  ADD PRIMARY KEY (`elementID`),
  ADD KEY `efID` (`efID`);

--
-- Indexes for table `evalform`
--
ALTER TABLE `evalform`
  ADD PRIMARY KEY (`efID`);

--
-- Indexes for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`evalID`),
  ADD KEY `progID` (`progID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`facID`),
  ADD KEY `parentID` (`instID`);

--
-- Indexes for table `institution`
--
ALTER TABLE `institution`
  ADD PRIMARY KEY (`instID`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`qID`);

--
-- Indexes for table `specification`
--
ALTER TABLE `specification`
  ADD PRIMARY KEY (`sID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assistancestaff`
--
ALTER TABLE `assistancestaff`
  MODIFY `aStaffID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `qID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `specification`
--
ALTER TABLE `specification`
  MODIFY `sID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
